import * as path from 'path'
import * as webpack from 'webpack'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import HtmlWebpackHarddiskPlugin from 'html-webpack-harddisk-plugin'

const isProd = process.env.DYNA_SAUR_ENV == 'production'

const config: webpack.Configuration = {
  mode: isProd ? 'production' : 'development',
  entry: {
    login: path.join(__dirname, '../src/login.tsx'),
    main: path.join(__dirname, '../src/main.tsx')
  },
  output: {
    path: path.join(__dirname, '../dist/'),
    filename: '[name]-[contenthash].js',
    publicPath: isProd ? '/assets/' : 'http://localhost:8000/'
  },
  devtool: isProd ? 'source-map' : 'eval-cheap-module-source-map',
  devServer: {
    port: 8000,
    allowedHosts: 'all',
    headers: {"Access-Control-Allow-Origin": "*"},
  },
  plugins: [
    new HtmlWebpackPlugin({
      chunks: ['login'],
      filename: 'login.html',
      alwaysWriteToDisk: true
    }),
    new HtmlWebpackPlugin({
      chunks: ['main'],
      filename: 'main.html',
      alwaysWriteToDisk: true
    }),
    new HtmlWebpackHarddiskPlugin()
  ],
  resolve: {
    extensions: ['.js', '.json', '.ts', '.tsx']
  },
  module: {
    rules: [
        {
            test: /\.tsx?$/,
            exclude: /(node_modules|bower_components)/,
            use: {
                loader: "swc-loader",
                options: {
                    jsc: {
                        parser: {
                          syntax: "typescript",
                          tsx: true
                        },
                        target: "es2020",
                        transform: {
                          react: {
                            runtime: 'automatic',
                            // refresh: !isProd
                          }
                        },
                      },
                      module: {
                        type: "es6"
                      },
                }
            }
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
    ]
  }
}

export default config
