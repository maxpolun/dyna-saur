import { Dictionary } from '@reduxjs/toolkit';
import { Link } from 'react-router-dom';
import { BtnLink } from '../common/components/Button';
import { FieldDefinition } from '../common/ducks/fields.duck';
import { Scope } from '../common/ducks/scopes.duck';

interface FieldListProps {
  fieldsByName: Record<string, FieldDefinition[]>
  scopes: Dictionary<Scope>
}

export const FieldList = ({ fieldsByName, scopes }: FieldListProps) => {
  return <table className='field-list'>
    <thead>
      <tr>
      <th>Name</th><th>Tenants</th><th>&nbsp;</th>
      </tr>
    </thead>
    <tbody>
      {Object.keys(fieldsByName).map(name => (
        <tr key={name}>
          <td>{name}</td>
          <td><TenantList fields={fieldsByName[name]} scopes={scopes} /></td>
          <td>
            <BtnLink to={`/ui/fields/by-name/${name}`}>View details</BtnLink>
          </td>
        </tr>
      ))}
    </tbody>
  </table>
}

const TenantList = ({ fields, scopes }: {fields: FieldDefinition[], scopes: Dictionary<Scope>}) => {
  const tenants = [...fields.reduce((acc, field) => {
    const scope = scopes[field.scope_id]
    const tenant = scope?.descriptor.kind != 'global' ? scope?.descriptor.tenant_id : 'unknown'
    return tenant ? acc.add(tenant) : acc
  }, new Set<string>())]

  return <ul className='inline-list'>
    {tenants.map(t => (<li className='inline-list-item' key={t}>{t}</li>))}
  </ul>
}
