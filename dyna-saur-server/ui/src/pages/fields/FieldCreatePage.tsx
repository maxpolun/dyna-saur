import { decamelizeKeys } from 'humps'
import { useForm } from 'react-hook-form'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { Breadcrumbs } from '../../common/components/Breadcrumbs'
import { BtnContainer, BtnLink, Button } from '../../common/components/Button'
import { FormGroup, FormInput, Label } from '../../common/components/forms'
import { Form } from '../../common/components/forms/Form'
import { PageHeader } from '../../common/components/PageHeader'
import { Field, fieldActions } from '../../common/ducks/fields.duck'
import { scopeName, scopeSelectors } from '../../common/ducks/scopes.duck'


interface CreateFieldData {
  scopeId: string
  name: string
  description?: string
}

export const FieldCreatePage = () => {
  const scopes = useSelector(scopeSelectors.noGlobals)
  const { register, handleSubmit } = useForm<CreateFieldData>()
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const submit = async (formData: CreateFieldData) => {
    let response: Field = await fetch('/admin/fields', {
      method: 'POST',
      body: JSON.stringify({ definition: decamelizeKeys(formData) }),
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }
    })
      .then(r => r.json())
    dispatch(fieldActions.fieldCreated(response))
    navigate(`/ui/fields/by-name/${response.definition.name}`)
  }

  return <div>
    <Breadcrumbs
      crumbs={[{ path: '/ui/fields', label: 'All Fields' }]}
      current='Create Field'
    />
    <PageHeader title="Create Field" />

    <Form onSubmit={handleSubmit(submit)}>
      <FormGroup>
        <Label htmlFor='#newfield.name'>Name</Label>
        <FormInput {...register('name', {required: true})} id='newfield.name' type='text' />
      </FormGroup>

      <FormGroup>
        <Label htmlFor='#newfield.scope'>Scope</Label>
        <select {...register('scopeId', {required: true})} id='newfield.scope' className='form-input'>
          <option></option>
          {scopes.map(s => <option key={s.id} value={s.id}>{scopeName(s)}</option>)}
        </select>
      </FormGroup>

      <FormGroup>
        <Label htmlFor='#newfield.description'>Description</Label>
        <FormInput {...register('description')} id='newfield.description' type='textarea' />
      </FormGroup>

      <BtnContainer>
        <BtnLink btnStyle='default' to='/ui/fields'>Cancel</BtnLink>
        <Button type='submit'>Create</Button>
      </BtnContainer>
    </Form>
  </div>
}
