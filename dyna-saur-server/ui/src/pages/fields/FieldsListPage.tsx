import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { BtnLink } from '../../common/components/Button'
import { PageHeader } from '../../common/components/PageHeader'
import { fieldSelectors } from '../../common/ducks/fields.duck'
import { scopeSelectors } from '../../common/ducks/scopes.duck'
import { FieldList } from '../../fields/FieldList'

export const FieldListPage = () => {
  const fields = useSelector(fieldSelectors.fieldsByNameSelector)
  const scopes = useSelector(scopeSelectors.selectEntities)
  const anyFields = Object.keys(fields).length > 0
  return <div>
    <PageHeader title='Fields' actions={<BtnLink to="/ui/fields/new">New Field</BtnLink>} />
    {anyFields ?
      <FieldList fieldsByName={fields} scopes={scopes} /> :
      <div>No fields yet, why not <BtnLink to="/ui/fields/new">add one</BtnLink>?</div>
    }
  </div>
}
