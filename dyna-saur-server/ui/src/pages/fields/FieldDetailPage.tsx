import { useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'
import { Breadcrumbs } from '../../common/components/Breadcrumbs'
import { Loader } from '../../common/components/Loader'
import { PageHeader } from '../../common/components/PageHeader'
import { ScopeName } from '../../common/components/ScopeName'
import { fetchFullFieldsByName, fieldSelectors } from '../../common/ducks/fields.duck'
import { scopeSelectors } from '../../common/ducks/scopes.duck'
import { RootState } from '../../common/store'

interface FieldDetailsParams {
  fieldName: string
  [others: string]: string
}

export const FieldDetailLoader = () => {
  let { fieldName } = useParams<{ fieldName: string }>()
  const sel = (state: RootState) => {
    if (!fieldName) return 'unloaded'
    return fieldSelectors.fieldsSelector(state).nameStatus[fieldName] || 'unloaded'
  }
  return <Loader loadAction={() => fieldName && fetchFullFieldsByName(fieldName)} statusSelector={sel} />
}

export const FieldDetailPage = () => {
  const {fieldName} = useParams<FieldDetailsParams>()
  const fields = useSelector((state: RootState) => fieldSelectors.allFieldsWithNameSelector(state, fieldName))
  const scopes = useSelector(scopeSelectors.selectEntities)

  return <div>
    <Breadcrumbs
      crumbs={[{ path: '/ui/fields', label: 'All Fields' }]}
      current={fieldName}
    />
    <PageHeader title={`Field: ${fieldName}`} />
    {fields.map(f => {
      const scope = scopes[f.definition.scope_id]
      return <div key={f.definition.id}>
        id: {f.definition.id}
        scope: {scope && <ScopeName scope={scope} />}
      </div>
    })}
  </div>
}
