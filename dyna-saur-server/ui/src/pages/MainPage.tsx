import { Navigate, Route, Routes, useMatch, useParams } from "react-router-dom";
import App from '../common/components/App';
import { combineStatus, loadAll, Loader } from '../common/components/Loader';
import { NavFrame } from '../common/components/NavFrame';
import { fetchFieldDefs, fieldSelectors } from '../common/ducks/fields.duck';
import { fetchAllScopes, scopeSelectors } from '../common/ducks/scopes.duck';
import { ApiKeysPage } from './ApiKeysPage';
import { ConfigurationPage } from './ConfigurationPage';
import { FieldCreatePage } from './fields/FieldCreatePage';
import { FieldDetailLoader, FieldDetailPage } from './fields/FieldDetailPage';
import { FieldListPage } from './fields/FieldsListPage';
import { UsersPage } from './UsersPage';

export const MainPage = () => <App>
  <Routes>
    <Route path="/ui" element={<NavFrame />}>
      <Route path="fields" element={<Loader loadAction={
        loadAll(fetchAllScopes, fetchFieldDefs)}
        statusSelector={combineStatus(scopeSelectors.scopesLoadingStatus, fieldSelectors.fieldsLoadingStatusSelector)}
      />}>
        <Route index element={<FieldListPage />} />
        <Route path="new" element={<FieldCreatePage />} />
        <Route path="by-name/:fieldName" element={ <FieldDetailLoader /> }>
          <Route index element={<FieldDetailPage />} />
        </Route>
      </Route>
      <Route path='users' element={<UsersPage />} />
      <Route path='config/*' element={<ConfigurationPage />} />
      <Route path='api-keys' element={<ApiKeysPage />} />
    </Route>
    <Route path="/" element={
      // redirect to fields page by default
      <Navigate to={"/ui/fields"} />
    } />
  </Routes>
</App>
