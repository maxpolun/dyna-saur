import './LoginPage.css'

export function LoginPage() {

  return <div className='panel centered login-panel'>
    <form action='/login' method='POST'>
      <h1>Login</h1>

      <label htmlFor="#login--email">Email</label>
      <input id="login--email" name='email'defaultValue="" />

      <label htmlFor="#login--password">Password</label>
      <input id="login--password" name='password' type='password' defaultValue="" />

      <button>Submit</button>
    </form>
  </div>
}
