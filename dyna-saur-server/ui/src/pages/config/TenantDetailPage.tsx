import { useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'
import { Breadcrumbs } from '../../common/components/Breadcrumbs'
import { Loader } from '../../common/components/Loader'
import { PageHeader } from '../../common/components/PageHeader'
import { fetchOneTenant, TenantSelectors } from '../../common/ducks/tenants.duck'
import { RootState } from '../../common/store'
import { useTenantContext } from '../../tenants/TenantDetailFrame'

export const TenantDetailLoader = () => {
  const { tenant_id } = useParams<{ tenant_id: string }>()
  const sel = (state: RootState) => {
    if (!tenant_id) return 'unloaded'
    const loading = TenantSelectors.loading(state)
    if (loading != 'unloaded' && loading != 'loaded') return loading
    return TenantSelectors.selectById(state, tenant_id) ? 'loaded' : 'unloaded'
  }
  return <Loader loadAction={() => tenant_id && fetchOneTenant(tenant_id)} statusSelector={sel} />
}

export const TenantDetailPage = () => {
  const tenant = useTenantContext()
  return <div>
    <div>
      <dl>
        <dt>Identifier</dt>
        <dd>{tenant.id}</dd>

        <dt>Description</dt>
        <dd>{tenant.description}</dd>
      </dl>
    </div>
  </div>
}
