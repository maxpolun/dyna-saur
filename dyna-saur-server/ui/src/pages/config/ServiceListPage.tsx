import { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Button } from '../../common/components/Button'
import { Loader, RawLoader } from '../../common/components/Loader'
import { Modal } from '../../common/components/Modal'
import { createServiceCred, fetchCredsForService, ServiceCredentialActions, ServiceCredentialsSelectors } from '../../common/ducks/service-credentials'
import { fetchAllServicesForTenant, ServiceSelectors } from '../../common/ducks/services.duck'
import { RootState } from '../../common/store'
import { CredentialsList } from '../../services/CredentialsList'
import { useTenantContext } from '../../tenants/TenantDetailFrame'

export const ServicesLoader = () => {
  const tenant = useTenantContext()
  const sel = (state: RootState) => {
    return ServiceSelectors.loading(state, tenant.id)
  }
  return <Loader loadAction={() => fetchAllServicesForTenant(tenant.id)} statusSelector={sel} />
}

export const ServiceListPage = () => {
  const [openService, setOpenService] = useState<string | null>(null)
  const tenant = useTenantContext()
  const services = useSelector((state: RootState) => ServiceSelectors.byTenant(state, tenant.id))
  const dispatch = useDispatch()
  return <table>
    <thead>
      <tr>
        <th>Service Id</th>
        <th>&nbsp;</th>
      </tr>
    </thead>
    <tbody>
      {services.map(s => <tr key={s.id}>
        <td>{s.id}</td>
        <td>
          <Button onClick={() => setOpenService(s.id)}>Credentials</Button>
          <Modal
            title={`Credentials for ${s.id} in ${s.tenant_id}`}
            open={openService === s.id}
            onClosed={() => setOpenService(null)}
            actions={<Button onClick={() => createServiceCred(s).then(cred => dispatch(ServiceCredentialActions.credentialAdded(cred))) }>Generate new credential</Button>}
          >
            <RawLoader
              loadAction={() => fetchCredsForService(s)}
              statusSelector={(state: RootState) => ServiceCredentialsSelectors.loadingForService(state, s)}
            >
              <CredentialsList service={s} />
            </RawLoader>
          </Modal>
        </td>
      </tr>)}
    </tbody>
  </table>
}
