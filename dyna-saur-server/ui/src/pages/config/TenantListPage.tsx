import { useSelector } from 'react-redux'
import { BtnLink } from '../../common/components/Button'
import { PageHeader } from '../../common/components/PageHeader'
import { Tenant, TenantSelectors } from '../../common/ducks/tenants.duck'

const TenantList = ({ tenants }: { tenants: Tenant[] }) => {
  return <table>
    <thead>
      <tr>
        <th>Tenant id</th>
        <th>Description</th>
        <th>&nbsp;</th>
      </tr>
    </thead>
    <tbody>
      {tenants.map(t => <tr key={t.id}>
        <td>{t.id}</td>
        <td>{t.description}</td>
        <td><BtnLink to={`/ui/config/tenants/${t.id}`}>Configure</BtnLink></td>
      </tr>)}
    </tbody>
  </table>
}

export const TenantListPage = () => {
  const tenants = useSelector(TenantSelectors.selectAll)

  return <div>
    <PageHeader title='Tenants' actions={<BtnLink to="/ui/config/tenants/new">New Tenant</BtnLink>} />
    {tenants.length ?
      <TenantList tenants={tenants} /> :
      <div>No tenants yet, why not <BtnLink to="/ui/config/tenants/new">add one</BtnLink>?</div>
    }
  </div>
}
