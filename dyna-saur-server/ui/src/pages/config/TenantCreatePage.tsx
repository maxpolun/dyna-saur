import { useForm } from 'react-hook-form'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { Breadcrumbs } from '../../common/components/Breadcrumbs'
import { BtnContainer, BtnLink, Button } from '../../common/components/Button'
import { FormGroup, FormInput, Label } from '../../common/components/forms'
import { Form } from '../../common/components/forms/Form'
import { PageHeader } from '../../common/components/PageHeader'
import connect from '../../common/connection'
import { scopeActions } from '../../common/ducks/scopes.duck'
import { Tenant, tenantActions } from '../../common/ducks/tenants.duck'
import { validIdent } from '../../common/identifier'

interface CreateTenantFormData {
  id: string,
  description: string
}

class TenantIdConflictError extends Error { }

const submitCreateForm = async (formData: CreateTenantFormData): Promise<Tenant> => {
  const response = await connect('/admin/tenants', {
    method: 'POST',
    body: JSON.stringify(formData)
  })

  if (response.status == 409) {
    throw new TenantIdConflictError(`A tenant with id ${formData.id} already exists`)
  }

  return response.json()
}

export const TenantCreatePage = () => {
  const { register, handleSubmit, setError } = useForm<CreateTenantFormData>()
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const onSubmit =  async (formData: CreateTenantFormData) => {
    try {
      const response = await submitCreateForm(formData)

      dispatch(tenantActions.tenantCreated(response))
      dispatch(scopeActions.scopesCleared())

      navigate(`/ui/config/tenants/${response.id}`)
    } catch (e) {
      if (e instanceof TenantIdConflictError) {
        setError('id', {message: e.message, type: 'unique'})
      } else {
        throw e
      }
    }
  }

  return <div>
    <Breadcrumbs
      crumbs={[{ path: '/ui/config/tenants', label: 'All Tenants' }]}
      current='Create Tenant'
    />
    <PageHeader title="Create Tenant" />
    <Form onSubmit={handleSubmit(onSubmit)}>
      <FormGroup>
        <Label htmlFor='#newtenant.id'>Identifier <small>(1-255 ascii chars or _)</small></Label>
        <FormInput type='text' {...register('id', { required: true, validate: validIdent})} id='newtenant.id' />
      </FormGroup>

      <FormGroup>
        <Label htmlFor='#newtenant.description'>Description</Label>
        <FormInput type='textarea' {...register('description', { required: true})} id='newtenant.description' />
      </FormGroup>

      <BtnContainer>
        <BtnLink btnStyle='default' to='/ui/config/tenants'>Cancel</BtnLink>
        <Button btnStyle='primary' type='submit'>Create</Button>
      </BtnContainer>
    </Form>
  </div>
}
