import { Navigate, Route, Routes } from 'react-router-dom'
import { Loader } from '../common/components/Loader'
import { fetchAllTenants, TenantSelectors } from '../common/ducks/tenants.duck'
import { RootState } from '../common/store'
import { TenantDetailFrame } from '../tenants/TenantDetailFrame'
import { ServiceCreatePage } from './config/ServiceCreatePage'
import { ServiceDetailPage } from './config/ServiceDetailPage'
import { ServiceGroupCreatePage } from './config/ServiceGroupCreatePage'
import { ServiceGroupDetailPage } from './config/ServiceGroupDetailPage'
import { ServiceGroupListPage } from './config/ServiceGroupListPage'
import { ServiceListPage, ServicesLoader } from './config/ServiceListPage'
import { TenantCreatePage } from './config/TenantCreatePage'
import { TenantDetailLoader, TenantDetailPage } from './config/TenantDetailPage'
import { TenantListPage } from './config/TenantListPage'

export const ConfigurationPage = () => {
  return <Routes>
    <Route path='tenants'>
      <Route element={ <Loader loadAction={fetchAllTenants} statusSelector={TenantSelectors.loading} /> }>
        <Route index element={<TenantListPage />} />
      </Route>
      <Route path='new' element={<TenantCreatePage />} />
      <Route path=':tenant_id' element={<TenantDetailLoader />}>
        <Route element={<TenantDetailFrame />}>
          <Route index element={<TenantDetailPage />} />
          <Route path='services' element={ <ServicesLoader/> }>
            <Route index element={<ServiceListPage />} />
            <Route path='new' element={<ServiceCreatePage />} />
            <Route path=':service_id' element={<ServiceDetailPage />} />
          </Route>
          <Route path='service-groups'>
            <Route index element={<ServiceGroupListPage />} />
            <Route path='new' element={<ServiceGroupCreatePage />} />
            <Route path=':service_group_id' element={<ServiceGroupDetailPage />} />
          </Route>
        </Route>
      </Route>
    </Route>
    <Route index element={
      // redirect to tenants index page by default
      <Navigate to={"/ui/config/tenants"} />
    } />
  </Routes>
}
