import { useDispatch, useSelector } from 'react-redux'
import { Button } from '../common/components/Button'
import { deleteCredentialForService, ServiceCredentialsSelectors } from '../common/ducks/service-credentials'
import { Service } from '../common/ducks/services.duck'
import { RootState } from '../common/store'

export const CredentialsList = ({ service }: { service: Service }) => {
  const creds = useSelector((state: RootState) => ServiceCredentialsSelectors.forService(state, service))
  const dispatch = useDispatch()
  const deletingCreds = useSelector(ServiceCredentialsSelectors.beingDeleted)
  return <div>
    {
      creds.length === 0 ?
        <span>No credentials exist. Create one to use this service</span> :
        <table>
          <thead>
            <tr>
              <th>Secret</th>
              <th>Comment</th>
              <th>Algorithm</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {creds.map(c => <tr key={c.id}>
              <td>{c.secret}</td>
              <td>{c.comment}</td>
              <td>{c.algorithm}</td>
              <td>
                <Button disabled={deletingCreds[c.id]} onClick={ () => dispatch(deleteCredentialForService(c)) }>Delete</Button>
              </td>
            </tr>)}
          </tbody>
        </table>
    }
  </div>
}
