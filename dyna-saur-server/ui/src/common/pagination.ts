export type Paginated<T> = {
  cursor: string | undefined,
  items: T
}
