export const CSRF_HEADER = 'X-Csrf-Token'

let csrf_token: string | null = null

async function getCsrfToken() {
  csrf_token = await fetch('/internal/csrf_tokens').then(res => res.json())
}

export async function ensureCsrfToken(): Promise<string> {
  if (!csrf_token) {
    await getCsrfToken()
  }
  if (!csrf_token) {
    throw new Error('unable to fetch csrf token')
  }
  return csrf_token
}
