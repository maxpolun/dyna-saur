import { deepmerge } from 'deepmerge-ts'
import { CSRF_HEADER, ensureCsrfToken } from './csrf_token'

// global wrapper for fetch. We can add defaults here if needed too.
export default async function connect(url: RequestInfo, config?: RequestInit): Promise<Response> {
  let csrf = await ensureCsrfToken()
  const defaultConfig: RequestInit = {
    headers: {
      'Content-type': 'application/json; charset=utf-8',
      [CSRF_HEADER]: csrf
    }
  }
  return fetch(url, deepmerge(defaultConfig, config))
}
