import connect from '../connection'
import { AppDispatch, internalCreateStore, StoreType } from '../store'
import { fetchFieldDefs, fieldSelectors } from './fields.duck'

jest.mock('../connection')
const connectMock = connect as jest.Mock
let store: StoreType
beforeEach(() => {
  store = internalCreateStore()
  connectMock.mockReset()
})

describe('fields slice', () => {
  it('updates the state', async () => {
    connectMock.mockImplementationOnce(() => {
      expect(fieldSelectors.fieldsSelector(store.getState()).state).toBe('loading')
      return {
        ok: true,
        json: () => Promise.resolve({cursor: '', items: []})
      }
    })

    expect(fieldSelectors.fieldsSelector(store.getState()).state).toBe('unloaded')
    await (store.dispatch as AppDispatch)(fetchFieldDefs())
    expect(fieldSelectors.fieldsSelector(store.getState()).state).toBe('loaded')
    expect(fieldSelectors.fieldsByNameSelector(store.getState())).toEqual({})
  })
})
