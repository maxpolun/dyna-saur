import { createAsyncThunk, createEntityAdapter, createSelector, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { LoadingStatus } from '../components/Loader';
import connect from '../connection';
import { RootState } from '../store';

export interface Tenant {
  id: string
  description: string
  created_at: Date
  updated_at: Date
}

const TenantEntity = createEntityAdapter<Tenant>()

export const fetchAllTenants = createAsyncThunk('tenants/fetchAll', async () => {
  const res = await connect('/admin/tenants', {
    method: 'GET'
  })
  if (res.ok) {
    return res.json()
  }
  throw new Error('unable to fetch tenants')
})

export const fetchOneTenant = createAsyncThunk('tenants/fetchOne', async (tenantId: string) => {
  const res = await connect(`/admin/tenants/${tenantId}`, {
    method: 'GET'
  })
  if (res.ok) {
    return res.json()
  }
  throw new Error('unable to fetch tenants')
})

export const tenantSlice = createSlice({
  name: 'tenants',
  initialState: {
    tenants: TenantEntity.getInitialState(),
    loading: 'unloaded' as LoadingStatus
  },
  reducers: {
    tenantCreated: (state, action: PayloadAction<Tenant>) => {
      TenantEntity.addOne(state.tenants, action.payload)
    }
  },
  extraReducers: builder => {
    builder.addCase(fetchAllTenants.pending, (state, action) => {
      state.loading = 'loading'
    })
    builder.addCase(fetchAllTenants.fulfilled, (state, action) => {
      state.loading = 'loaded'
      TenantEntity.addMany(state.tenants, action.payload.items)
    })

    builder.addCase(fetchOneTenant.pending, (state, action) => {
      state.loading = 'loading'
    })
    builder.addCase(fetchOneTenant.fulfilled, (state, action) => {
      state.loading = 'loaded'
      TenantEntity.addOne(state.tenants, action.payload)
    })
  }
})

const getTenants = (state: RootState) => state[tenantSlice.name]
const tenantSelectors = TenantEntity.getSelectors((state: RootState) => getTenants(state).tenants)
const tenantsLoadingSelector = createSelector(getTenants, slice => slice.loading)

export const TenantSelectors = {
  getTenants,
  loading: tenantsLoadingSelector,
  ...tenantSelectors
}

export const tenantActions = {
  ...tenantSlice.actions
}
