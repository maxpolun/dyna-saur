import { createAsyncThunk, createEntityAdapter, createSelector, createSlice, PayloadAction, SerializedError } from '@reduxjs/toolkit'
import { error } from 'console'
import { LoadingStatus } from '../components/Loader'
import connect from '../connection'
import { RootState } from '../store'

export type FieldDefinition = {
  id: string
  scope_id: string
  name: string
  description?: string
  salt: number
  created_at: Date
  updated_at: Date
}

export type FieldValue = {
  id: string
  field_id: string
  payload: any
  ordinal: number
  created_at: Date
  updated_at: Date
}

export enum ConstraintOperation {
  Equals = 'EQUALS',
  NotEquals = 'NOT_EQUALS',
  InArray = 'IN_ARRAY'
}

export type FieldValueConstraint = {
  id: string
  field_value_id: string
  context_field: string,
  operation: ConstraintOperation,
  constrained_to_value: any
  ordinal: number
  created_at: Date
  updated_at: Date
}

export type Field = {
  definition: FieldDefinition,
  values: FieldValueDef[]
}

export type FieldValueDef = {
  definition: FieldValue,
  constraints: FieldValueConstraint[]
}

const FieldDefAdaptor = createEntityAdapter<FieldDefinition>({
  selectId: field => field.id,
  sortComparer: (a, b) => a.name.localeCompare(b.name)
})

const FieldAdaptor = createEntityAdapter<Field>({
  selectId: field => field.definition.id,
  sortComparer: (a, b) => a.definition.name.localeCompare(b.definition.name)
})

export const fetchFieldDefs = createAsyncThunk('fields/fetchdefs', async () => {
  let response = await connect('/admin/fields', {
    method: 'GET'
  })
  if (response.ok) {
    return (await response.json()).items
  }
  throw new Error(`error loading fields: ${response.statusText}}`)
})

export const fetchFullFieldsByName = createAsyncThunk('fields/fetchfull', async (name: string) => {
  const url = new URL('/admin/fields', window.location.origin)
  url.searchParams.append('name', name)
  url.searchParams.append('embed_children', "true")
  let response = await connect(url.toString(), {
    method: 'GET'
  })
  if (response.ok) {
    return (await response.json()).items
  }
  throw new Error(`error loading fields: ${response.statusText}}`)
})

export type FieldSliceState = {
  state: LoadingStatus,
  nameStatus: Record<string, LoadingStatus>,
  err?: SerializedError,
  defs: ReturnType<typeof FieldDefAdaptor.getInitialState>,
  fields: ReturnType<typeof FieldAdaptor.getInitialState>,
}

const initialState: FieldSliceState = {
  state: 'unloaded',
  nameStatus: {},
  defs: FieldDefAdaptor.getInitialState(),
  fields: FieldAdaptor.getInitialState()
}

export const fieldSlice = createSlice({
  name: 'fields',
  initialState,
  reducers: {
    fieldCreated: (state, action: PayloadAction<Field>) => {
      FieldDefAdaptor.addOne(state.defs, action.payload.definition)
      FieldAdaptor.addOne(state.fields, action.payload)
    }
  },
  extraReducers: builder => {
    builder.addCase(fetchFieldDefs.pending, (state, action) => {
      state.state = 'loading'
    })
    builder.addCase(fetchFullFieldsByName.pending, (state, action) => {
      state.nameStatus[action.meta.arg] = 'loading'
    })
    builder.addCase(fetchFieldDefs.rejected, (state, action) => {
      state.state = 'error',
      state.err = action.error
    })
    builder.addCase(fetchFullFieldsByName.rejected, (state, action) => {
      state.nameStatus[action.meta.arg] = 'error'
      state.err = action.error
    })
    builder.addCase(fetchFieldDefs.fulfilled, (state, action) => {
      let newState = {
        state: 'loaded' as const,
        nameStatus: {},
        defs: FieldDefAdaptor.getInitialState(),
        fields: FieldAdaptor.getInitialState()
      }
      newState.defs = FieldDefAdaptor.addMany(newState.defs, action.payload)
      return newState
    })

    builder.addCase(fetchFullFieldsByName.fulfilled, (state, action) => {
      state.nameStatus[action.meta.arg] = 'loaded'
      state.fields = FieldAdaptor.addMany(state.fields, action.payload)
    })
  }
})

const fieldsSelector = (state: RootState) => state.fields
const fieldDefsSelector = createSelector(fieldsSelector, state => state.defs)
const fullFieldsSelector = createSelector(fieldsSelector, state => state.fields)
const fieldDefEntitySelectors = FieldDefAdaptor.getSelectors(fieldDefsSelector)
const fieldEntitySelectors = FieldAdaptor.getSelectors(fullFieldsSelector)

const fieldsLoadingStatusSelector = createSelector(fieldsSelector, fields => fields.state)
const fieldsByNameSelector = createSelector(fieldDefsSelector, state => {
  return Object.values(state.entities).reduce((accum, fieldDef) => {
    if (typeof fieldDef === 'undefined') return accum
    accum[fieldDef.name] ??= []
    accum[fieldDef.name].push(fieldDef)
    return accum
  }, {} as Record<string, FieldDefinition[]>)
})
const allFieldsWithNameSelector = createSelector(
  [
    fieldEntitySelectors.selectEntities,
    (_state: RootState, fieldName?: string) => fieldName
  ],
  (fields, fieldName) => Object.values(fields).filter((field): field is Field => field?.definition.name === fieldName))

export const fieldSelectors = {
  fieldsSelector,
  fieldsLoadingStatusSelector,
  fieldsByNameSelector,
  allFieldsWithNameSelector,
  defs: fieldDefEntitySelectors,
  full: fieldEntitySelectors
}

export const fieldActions = fieldSlice.actions
