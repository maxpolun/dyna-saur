import { createAsyncThunk, createEntityAdapter, createSelector, createSlice } from '@reduxjs/toolkit';
import { LoadingStatus } from '../components/Loader';
import connect from '../connection';
import { PermissionError } from '../errors';
import { RootState } from '../store';

export interface Service {
  id: string
  tenant_id: string
  created_at: Date
}

const ServiceEntity = createEntityAdapter<Service>()

export const fetchAllServicesForTenant = createAsyncThunk('service/fetchByTenant', async (tenantId: string) => {
  let req = await connect(`/admin/tenants/${tenantId}/services`, {
    method: 'GET',
  })

  if (req.ok) {
    return (await req.json()).items
  } else if (req.status == 403) {
    throw new PermissionError('Unable to read services')
  }else {
    throw new Error(req.statusText)
  }
})

export const serviceSlice = createSlice({
  name: 'services',
  initialState: {
    services: ServiceEntity.getInitialState(),
    loadedByTenant: {} as Record<string, LoadingStatus>
  },
  reducers: {},
  extraReducers: builder => {
    builder.addCase(fetchAllServicesForTenant.pending, (state, action) => {
      state.loadedByTenant[action.meta.arg] = 'loading'
    })

    builder.addCase(fetchAllServicesForTenant.fulfilled, (state, action) => {
      state.loadedByTenant[action.meta.arg] = 'loaded'
      ServiceEntity.addMany(state.services, action.payload)
    })
  }
})

const servicesSelector = (state: RootState) => state.services
const selectors = ServiceEntity.getSelectors((s: RootState) => servicesSelector(s).services);
const tenantIdSelector = (_s: RootState, tenantId: string) => tenantId
const loadingSelector = createSelector(servicesSelector, tenantIdSelector, (state, tenantId) => state.loadedByTenant[tenantId] ?? 'unloaded')
const servicesByTenantSelector = createSelector(selectors.selectAll, tenantIdSelector, (services, tenantId) => {
  return services.filter(s => s.tenant_id === tenantId)
})

export const ServiceSelectors = {
  loading: loadingSelector,
  byTenant: servicesByTenantSelector,
  ...selectors
}
