import { createAsyncThunk, createEntityAdapter, createSelector, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { createSelectorCreator } from 'reselect'
import { LoadingStatus } from '../components/Loader'
import connect from '../connection'
import { RootState } from '../store'
import { Service } from './services.duck'

export interface ServiceCredential {
  id: string,
  service_id: string,
  tenant_id: string,
  comment?: string,
  algorithm: 'HS256',
  secret: string
}

const ServiceCredentialEntity = createEntityAdapter<ServiceCredential>()

export const fetchCredsForService = createAsyncThunk('services/creds/fetch', async (service: Service) => {
  let res = await connect(`/admin/tenants/${service.tenant_id}/services/${service.id}/credentials`, { method: 'GET' })
  if (res.ok) {
    return (await res.json()).items
  } else {
    throw new Error(res.statusText)
  }
})

export const deleteCredentialForService = createAsyncThunk('services/creds/delete', async (cred: ServiceCredential) => {
  let res = await connect(`/admin/tenants/${cred.tenant_id}/services/${cred.service_id}/credentials/${cred.id}`, { method: 'DELETE' })
  if (res.ok) {
    return
  } else {
    throw new Error(res.statusText)
  }
})

export const createServiceCred = async (service: Service, comment?: string): Promise<ServiceCredential> => {
  const res = await connect(`/admin/tenants/${service.tenant_id}/services/${service.id}/credentials`, {
    method: 'POST',
    body: JSON.stringify({ comment })
  })

  if (res.ok) {
    return (await res.json())
  }
  throw new Error(res.statusText)
}

export const credsSlice = createSlice({
  name: 'serviceCredentials',
  initialState: {
    creds: ServiceCredentialEntity.getInitialState(),
    loadedByService: {} as Record<string, LoadingStatus>,
    deletingIds: {} as Record<string, boolean>
  },
  reducers: {
    credentialAdded: (state, action: PayloadAction<ServiceCredential>) => {
      ServiceCredentialEntity.addOne(state.creds, action.payload)
    }
  },
  extraReducers: builder => {
    builder.addCase(fetchCredsForService.pending, (state, action) => {
      const { id, tenant_id } = action.meta.arg
      state.loadedByService[`${tenant_id}|${id}`] = 'loading'
    })

    builder.addCase(fetchCredsForService.fulfilled, (state, action) => {
      const { id, tenant_id } = action.meta.arg
      state.loadedByService[`${tenant_id}|${id}`] = 'loaded'
      ServiceCredentialEntity.addMany(state.creds, action.payload)
    })

    builder.addCase(deleteCredentialForService.fulfilled, (state, action) => {
      const { id } = action.meta.arg
      delete state.deletingIds[id]
      ServiceCredentialEntity.removeOne(state.creds, id)
    })

    builder.addCase(deleteCredentialForService.rejected, (state, action) => {
      const { id } = action.meta.arg
      delete state.deletingIds[id]
    })

    builder.addCase(deleteCredentialForService.pending, (state, action) => {
      const { id } = action.meta.arg
      state.deletingIds[id] = true
    })
  }
})

const serviceCredsSelector = (state: RootState) => state[credsSlice.name]
const entitiesSelector = createSelector(serviceCredsSelector, state => state.creds)
const selectors = ServiceCredentialEntity.getSelectors(entitiesSelector)
const serviceSelector = (_: RootState, s: Service) => s
const credsForServiceSelector = createSelector(selectors.selectAll, serviceSelector, (creds, service) => {
  return creds.filter(cred => cred && cred.service_id === service.id && cred.tenant_id === service.tenant_id)
})
const loadingForService = createSelector(serviceCredsSelector, serviceSelector,
  (creds, service) => creds.loadedByService[`${service.tenant_id}|${service.id}`] || 'unloaded')
const beingDeleted = createSelector(serviceCredsSelector, state => state.deletingIds)

export const ServiceCredentialsSelectors = {
  base: serviceCredsSelector,
  forService: credsForServiceSelector,
  loadingForService,
  beingDeleted,
  ...selectors
}

export const ServiceCredentialActions = credsSlice.actions
