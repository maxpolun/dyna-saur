import { createAsyncThunk, createEntityAdapter, createSelector, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { LoadingStatus } from '../components/Loader'
import connect from '../connection'
import { Paginated } from '../pagination'
import { RootState } from '../store'

export type Scope = {
  id: string,
  name: string,
  descriptor: ScopeDescriptor
}

export type ScopeDescriptor = { kind: 'global' } |
  { kind: 'tenant', tenant_id: string } |
  { kind: 'service', tenant_id: string, service_id: string } |
  { kind: 'service_group', tenant_id: string, service_group_id: string }

export function scopeName(scope: Scope) {
  switch (scope.descriptor.kind) {
    case 'global': return 'Global'
    case 'tenant': return `Tenant: ${scope.descriptor.tenant_id}`
    case 'service': return `Service: ${scope.descriptor.tenant_id}/${scope.descriptor.service_id}`
    case 'service_group': return `Service Group: ${scope.descriptor.tenant_id}/${scope.descriptor.service_group_id}`
  }
}

function scopeSort(scope: Scope) {
  switch (scope.descriptor.kind) {
    case 'global': return 'Global'
    case 'tenant': return scope.descriptor.tenant_id
    case 'service': return `${scope.descriptor.tenant_id}/s/${scope.descriptor.service_id}`
    case 'service_group': return `${scope.descriptor.tenant_id}/sg/${scope.descriptor.service_group_id}`
  }
}

const ScopeAdaptor = createEntityAdapter<Scope>({
  selectId: scope => scope.id,
  sortComparer: (a, b) => (
    scopeSort(a).localeCompare(scopeSort(b))
  )
})

export const fetchAllScopes = createAsyncThunk('scopes/fetch', async () => {
  let response = await connect('/admin/scopes', {
    method: 'GET'
  })
  if (response.ok) {
    return await response.json()
  }
  throw new Error(`error loading scopes: ${response.statusText}}`)
})

const initialState = {
  status: 'unloaded' as LoadingStatus,
  cursor: null as null | string,
  items: ScopeAdaptor.getInitialState()
}

export const scopeSlice = createSlice({
  name: 'scopes',
  initialState,
  reducers: {
    scopesCleared: (state) => {
      return initialState
    }
  },
  extraReducers: builder => {
    builder.addCase(fetchAllScopes.fulfilled, (state, action) => {
      state.status = 'loaded'
      state.cursor = action.payload.cursor
      ScopeAdaptor.setAll(state.items, action.payload.items)
    })
    builder.addCase(fetchAllScopes.pending, (state, action) => {
      state.status = 'loading'
    })
  }
})

const scopesSelector = (state: RootState) => state[scopeSlice.name]
const scopesItemsSelector = createSelector(scopesSelector, scopes => scopes.items)
const scopesCursorSelector = createSelector(scopesSelector, scopes => scopes.cursor)
const scopesLoadingStatus = createSelector(scopesSelector, scopes => scopes.status)
const entitiesSelectors = ScopeAdaptor.getSelectors(scopesItemsSelector)
const noGlobals = createSelector(entitiesSelectors.selectAll, scopes => scopes.filter(s => s.descriptor.kind !== 'global'))

export const scopeSelectors = {
  scopesSelector,
  scopesCursorSelector,
  scopesLoadingStatus,
  noGlobals,
  ...entitiesSelectors
}

export const scopeActions = {
  ...scopeSlice.actions
}
