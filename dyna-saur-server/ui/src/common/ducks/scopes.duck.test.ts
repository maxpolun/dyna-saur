import { configureStore, EnhancedStore } from '@reduxjs/toolkit'
import { createStore, Store } from 'redux'
import connect from '../connection'
import { Paginated } from '../pagination'
import { AppDispatch, internalCreateStore, RootState, StoreType } from '../store'
import { fetchAllScopes, Scope, scopeSelectors } from './scopes.duck'

jest.mock('../connection')
const connectMock = connect as jest.Mock
let store: StoreType
beforeEach(() => {
  store = internalCreateStore()
  connectMock.mockReset()
})

const successResponse: Paginated<Scope[]> = {
  cursor: '',
  items: [
    { id: 'g', name: 'global scope', descriptor: { kind: 'global' as const }},
    { id: 't', name: 'tenant scope', descriptor: { kind: 'tenant' as const, tenant_id: 't' }},
    { id: 's', name: 'service scope', descriptor: { kind: 'service' as const, tenant_id: 't', service_id: 's' }},
    { id: 'sg', name: 'service group scope', descriptor: { kind: 'service_group' as const, tenant_id: 't', service_group_id: 'sg' }},
  ]
}

describe('scopes', () => {
  it('can fetch', async () => {
    connectMock.mockResolvedValue({
      ok: true,
      json: () => Promise.resolve(successResponse)
    })

    await (store.dispatch as AppDispatch)(fetchAllScopes())
    expect(scopeSelectors.selectIds(store.getState() as RootState)).toEqual(expect.arrayContaining(successResponse.items.map(i => i.id)))
  })
})
