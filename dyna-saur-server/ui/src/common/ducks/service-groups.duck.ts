import { createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { LoadingStatus } from '../components/Loader';

interface ServiceGroup {
  id: string
  description: string
  created_at: Date
  updated_at: Date
}

const ServiceGroupEntity = createEntityAdapter<ServiceGroup>()

export const serviceGroupSlice = createSlice({
  name: 'serviceGroups',
  initialState: {
    serviceGroups: ServiceGroupEntity.getInitialState(),
    loadedByTenant: {} as Record<string, LoadingStatus>
  },
  reducers: {}
})
