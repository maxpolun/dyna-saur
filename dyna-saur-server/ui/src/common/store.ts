import { configureStore } from '@reduxjs/toolkit';
import { useDispatch } from 'react-redux';
import { fieldSlice } from './ducks/fields.duck';
import { scopeSlice } from './ducks/scopes.duck';
import { credsSlice } from './ducks/service-credentials';
import { serviceGroupSlice } from './ducks/service-groups.duck';
import { serviceSlice } from './ducks/services.duck';
import { tenantSlice } from './ducks/tenants.duck';

export const internalCreateStore = () => configureStore({
  reducer: {
    [scopeSlice.name]: scopeSlice.reducer,
    [fieldSlice.name]: fieldSlice.reducer,
    [tenantSlice.name]: tenantSlice.reducer,
    [serviceSlice.name]: serviceSlice.reducer,
    [serviceGroupSlice.name]: serviceGroupSlice.reducer,
    [credsSlice.name]: credsSlice.reducer
  }
})

export const store = internalCreateStore()
export type StoreType = typeof store

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
export const useAppDispatch = () => useDispatch<AppDispatch>()
