import { ReactNode } from 'react';
import './PageHeader.css'

interface PageHeaderProps {
  title: ReactNode
  actions?: ReactNode
}

export const PageHeader = ({title, actions}: PageHeaderProps) => {
  return <div className='page-header'>
    <h2>{title}</h2>
    <div className='actions'>
      {actions}
    </div>
  </div>
}
