import { ReactNode, useCallback, useEffect, useRef, useState } from 'react'
import { createPortal } from 'react-dom'
import './Modal.css'

export interface ModalWindowProps {
  children: ReactNode,
  open: boolean,
  onClosed: () => void
}

export const ModalWindow = ({ children, open, onClosed}: ModalWindowProps) => {
  const modalContainer = useRef<HTMLDivElement | undefined>(undefined)
  const [hasRef, setHasRef] = useState(false)
  const onKeyDown = useCallback((event: KeyboardEvent) => {
    if (event.key === 'Escape') {
      onClosed()
    }
  }, [onClosed])
  useEffect(() => {
    if (!modalContainer.current) {
      const elem = document.createElement('div')
      elem.className = 'modal-container'
      document.body.appendChild(elem)
      modalContainer.current = elem
      setHasRef(true)
    }
    return () => {
      if (modalContainer.current) {
        document.body.removeChild(modalContainer.current)
        modalContainer.current = undefined
      }
    }
  }, [])

  useEffect(() => {
    document.addEventListener('keydown', onKeyDown)
    return () => document.removeEventListener('keydown', onKeyDown)
  }, [onKeyDown])

  return (open && hasRef && modalContainer.current) ? createPortal(
    <div className='modal-overlay' onClick={onClosed}>
      <div className='modal' onClick={e => e.stopPropagation()}>
        {children}
      </div>
    </div>,
  modalContainer.current) : null
}

export interface ModalProps extends ModalWindowProps {
  title: ReactNode
  actions?: ReactNode
}

export const Modal = ({title, children, onClosed, actions, ...props}: ModalProps) => {
  return <ModalWindow onClosed={onClosed} {...props}>
    <div className='modal-header'>
      <h1>
        <div className='modal-title'>{title}</div>
        <div className='modal-actions-area'>{actions}</div>
      </h1>
      <button className='modal-close-btn' type='button' onClick={onClosed}>&times;</button>
    </div>
    <div className='modal-body'>{children}</div>
  </ModalWindow>
}
