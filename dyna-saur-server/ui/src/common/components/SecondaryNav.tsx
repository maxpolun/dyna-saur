import React, { FC, ReactNode } from 'react';
import './SecondaryNav.css'

export const SecondaryNav: FC<{main: ReactNode}> = ({ children, main }) => {
  return <div className='secondary-nav-container'>
    <nav className='secondary-nav'>{children}</nav>
    <div className='secondary-nav-main'>{main}</div>
  </div>
}
