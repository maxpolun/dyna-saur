import { useState } from 'react';
import { NavLink, Outlet } from 'react-router-dom';
import { Button } from './Button';
import './NavFrame.css'

export const NavFrame = () => {

  const [open, setOpen] = useState(false)
  const close = () => setOpen(false)

  return <div>
    <header className={ open ? 'menu open' : 'menu closed'}>
      <div className='burger-container'><Button className='hamburger-btn' onClick={() => setOpen(o => !o)}>🍔</Button></div>
      <h1>Dyna-saur</h1>

      <ul className='nav-tabs'>
        <div className='close-container'><Button className='close-btn' onClick={close}>
          <span className='chevron left'></span> Close
        </Button></div>
        <li><NavLink onClick={ close } className={({ isActive }) => isActive ? 'nav-tab active' : 'nav-tab'} to='/ui/fields' >
          Fields
        </NavLink></li>

        <li><NavLink onClick={ close } className={({ isActive }) => isActive ? 'nav-tab active' : 'nav-tab'} to='/ui/users' >
          Users
        </NavLink></li>

        <li><NavLink onClick={ close } className={({ isActive }) => isActive ? 'nav-tab active' : 'nav-tab'} to='/ui/config' >
          Configuration
        </NavLink></li>

        <li><NavLink onClick={ close } className={({ isActive }) => isActive ? 'nav-tab active' : 'nav-tab'} to='/ui/api-keys' >
          API Keys
        </NavLink></li>
      </ul>
    </header>
    <main>
      <Outlet />
    </main>
  </div>
}
