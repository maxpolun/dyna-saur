import { ButtonHTMLAttributes, ReactComponentElement, ReactNode } from 'react'
import { Link } from 'react-router-dom'
import './Button.css'

interface RawButtonProps {
  children: React.ReactNode,
  className?: string
  btnStyle?: 'default' | 'primary' | 'link'
  element?: string | React.ComponentType<any>
  [other: string]: any
}

export const RawButton = ({ children, className, btnStyle = 'default', element: Element = 'button', ...other}: RawButtonProps) => (
  <Element className={ `btn btn-${btnStyle} ${className}` } {...other}>{ children }</Element>
)

interface ButtonProps extends RawButtonProps {
  type?: 'button' | 'submit' | 'reset',
}

export const Button = ({type = 'button', ...props }: ButtonProps) => {
  return <RawButton {...props} type={type} element={'button'} />
}

interface BtnLinkProps extends RawButtonProps {
  to: string
}

export const BtnLink = ({ to, btnStyle = 'link', ...otherProps }: BtnLinkProps) => {
  return <RawButton {...otherProps} element={Link} to={to} btnStyle={btnStyle} />
}

export const BtnContainer = ({children}: {children: ReactNode}) => {
  return <div className='btn-container'>{children}</div>
}
