import { ReactNode } from 'react';
import { Link } from 'react-router-dom';
import './Breadcrumbs.css'
import { BtnLink } from './Button';

interface Breadcrumb {
  path: string
  label: ReactNode
}

interface BreadcrumbsProps {
  crumbs: Breadcrumb[],
  current: ReactNode
}

export const Breadcrumbs = ({ crumbs, current }: BreadcrumbsProps) => {
  return <div className='breadcrumbs'>
    {crumbs.map(c => {
      return <BtnLink key={c.path} to={c.path}>
        {c.label}
      </BtnLink>
    })}
    <span className='current'>{ current }</span>
  </div>
}
