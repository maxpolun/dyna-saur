import { Scope, scopeName } from '../ducks/scopes.duck';

export const ScopeName = ({ scope }: { scope: Scope }) => {
  const text = scopeName(scope)
  return <span className='scope-name' data-scopeid={scope.id} data-scopename={ scope.name }>{text}</span>
}
