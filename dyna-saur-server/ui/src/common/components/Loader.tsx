import { createSelector } from '@reduxjs/toolkit';
import { ReactElement, ReactNode, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Outlet } from 'react-router-dom';
import { AppDispatch, RootState, useAppDispatch } from '../store';

export type LoadingStatus = 'unloaded' | 'loading' | 'error' | 'loaded'
type loadFn = () => any
type StatusSelector = (state: RootState) => LoadingStatus

interface LoaderProps {
  loadAction: loadFn,
  statusSelector: StatusSelector
}

interface RawLoaderProps extends LoaderProps {
  children: ReactElement
}

export const RawLoader = ({ loadAction, statusSelector, children }: RawLoaderProps) => {
  const status = useSelector(statusSelector)
  const dispatch = useAppDispatch()
  useEffect(() => {
    if (status === 'unloaded') {
      dispatch(loadAction())
    }
  }, [loadAction, status])
  if (status === 'unloaded' || status === 'loading') {
    return <div>Loading</div>
  }

  return children
}

export const Loader = ({ loadAction, statusSelector }: LoaderProps) => {
  return <RawLoader loadAction={loadAction} statusSelector={statusSelector}><Outlet /></RawLoader>
}

export const loadAll = (...actions: loadFn[]) => () => (dispatch: AppDispatch) => {
  return Promise.all(actions.map(a => dispatch(a())))
}

export const combineStatus = (...selectors: StatusSelector[]) => createSelector(selectors, (...allStatuses): LoadingStatus => {
  if (allStatuses.every(s => s === 'unloaded')) return 'unloaded'
  if (allStatuses.some(s => s === 'loading')) return 'loading'
  if (allStatuses.some(s => s === 'error')) return 'error'
  return 'loaded'
})
