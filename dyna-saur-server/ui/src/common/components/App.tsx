import React from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'
import { store } from '../store'

const Frame: React.FC = ({ children }) => (
  <React.StrictMode>
    <BrowserRouter>
      <Provider store={store}>
        {children}
      </Provider>
    </BrowserRouter>
  </React.StrictMode>
)

export default Frame
