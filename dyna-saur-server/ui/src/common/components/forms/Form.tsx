import { FormHTMLAttributes, forwardRef } from 'react';

export const Form = forwardRef<HTMLFormElement, FormHTMLAttributes<any>>((props, ref) => (
  <form {...props} ref={ref} className={`form-panel ${props.className}`}/>
))
