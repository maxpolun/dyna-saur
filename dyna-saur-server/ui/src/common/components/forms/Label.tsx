import { forwardRef, LabelHTMLAttributes } from 'react';

export const Label = forwardRef < HTMLLabelElement, LabelHTMLAttributes<any>>((props, ref) => (
  <label {...props} ref={ref} className={`form-label ${props.className || ''}`}/>
))
