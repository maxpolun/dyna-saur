import { forwardRef, InputHTMLAttributes } from 'react';

interface FormInputProps extends InputHTMLAttributes<any>{
  type: string
  className?: string
}

export const FormInput = forwardRef<HTMLInputElement & HTMLTextAreaElement, FormInputProps>((props, ref) => {
  const className = `form-input ${props.className || ''}`

  return props.type === 'textarea' ?
    <textarea {...props} ref={ref} className={className} /> :
    <input {...props} ref={ref} className={className} />
})
