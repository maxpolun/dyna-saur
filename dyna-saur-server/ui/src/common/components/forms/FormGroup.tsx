import { ReactNode } from 'react';

interface FormGroupProps {
  children: ReactNode
  className?: string
  type?: 'normal' | 'inline'
}

export const FormGroup = ({ children, className, type = 'normal' }: FormGroupProps) => {
  const addedCN = type === 'inline' ? 'inline' : ''
  return <div className={`form-group ${addedCN} ${className || ''}`}>
    {children}
  </div>

}
