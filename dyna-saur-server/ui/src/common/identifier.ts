export const validIdent = (ident: string) => ident.length >= 1 && ident.length <= 255 && /^[a-zA-Z0-9_]+$/.test(ident)
