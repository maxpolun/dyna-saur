import ReactDOM from 'react-dom'
import 'modern-css-reset/dist/reset.min.css'
import './common/base.css'
import { MainPage } from './pages/MainPage'
import './common/csrf_token'

const elem = document.createElement('div')
document.body.appendChild(elem)

ReactDOM.render(<MainPage />, elem)
