import React, { createContext, FC, useContext } from 'react';
import { useSelector } from 'react-redux';
import { NavLink, Outlet, useOutletContext, useParams } from 'react-router-dom';
import { Breadcrumbs } from '../common/components/Breadcrumbs';
import { PageHeader } from '../common/components/PageHeader';
import { SecondaryNav } from '../common/components/SecondaryNav';
import { Tenant, TenantSelectors } from '../common/ducks/tenants.duck';
import { RootState } from '../common/store';

export interface TenantContext {
  tenant: Tenant
}

const currentTenantContext = createContext<Tenant>({id: 'error', description: '', created_at: new Date(0), updated_at: new Date(0)})

export const useTenantContext = () => useContext(currentTenantContext)

export const TenantDetailFrame= () => {
  let { tenant_id } = useParams<{ tenant_id: string }>()
  let tenant = useSelector((state: RootState) => tenant_id && TenantSelectors.selectById(state, tenant_id))
  if (!tenant) return <div>Error -- unable to load tenant</div>

  return <div>
    <Breadcrumbs
      crumbs={[{ path: '/ui/config/tenants', label: 'All Tenants' }]}
      current={tenant.id}
    />
    <PageHeader title={`Tenant: ${tenant.id}`} />
    <currentTenantContext.Provider value={tenant}><SecondaryNav main={<Outlet/>}>
      <NavLink to=''>Tenant</NavLink>
      <NavLink to='services'>Services</NavLink>
      <NavLink to='service-groups'>Service Groups</NavLink>
    </SecondaryNav></currentTenantContext.Provider>
  </div>
}
