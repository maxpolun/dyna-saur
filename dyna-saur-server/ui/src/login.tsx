import ReactDOM from 'react-dom'
import { LoginPage } from './pages/LoginPage'
import 'modern-css-reset/dist/reset.min.css'
import './common/base.css'

const elem = document.createElement('div')
document.body.appendChild(elem)

ReactDOM.render(<LoginPage />, elem)
