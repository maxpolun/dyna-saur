use actix_web::{
  web::{Data, Json, Path},
  HttpResponse, Result,
};
use serde::{Deserialize, Serialize};
use crate::{DbAddr, util::Identifier, problem_json::ProblemJsonExt};

use super::{messages::{AddServiceToGroup, CreateServiceGroup}, model::NewServiceGroup};

#[derive(Debug, Deserialize)]
pub struct ServiceGroupPath {
  tenant_id: Identifier,
  service_group_id: Identifier
}

#[derive(Debug, Serialize, Deserialize)]
pub struct NewServiceGroupRequest {
  id: Identifier,
  description: String
}

pub async fn create_service_group(db: Data<DbAddr>, tenant_id: Path<Identifier>, body: Json<NewServiceGroupRequest>) -> Result<HttpResponse> {
  let tenant_id = tenant_id.into_inner();
  let NewServiceGroupRequest {id, description} = body.into_inner();
  let sg = db.send(CreateServiceGroup(NewServiceGroup{ id, description, tenant_id })).await.err_problem_json()?.err_problem_json()?;
  Ok(HttpResponse::Ok().json(sg))
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AddServiceToGroupRequest {
  service_id: Identifier
}

pub async fn add_membership_to_service_group(db: Data<DbAddr>, path: Path<ServiceGroupPath>, body: Json<AddServiceToGroupRequest>) -> Result<HttpResponse> {
  let ServiceGroupPath {tenant_id, service_group_id} = path.into_inner();
  let AddServiceToGroupRequest { service_id} = body.into_inner();
  let membership = db.send(AddServiceToGroup{
    tenant_id,
    service_group_id,
    service_id,
  }).await.err_problem_json()?.err_problem_json()?;
  Ok(HttpResponse::Ok().json(membership))
}
