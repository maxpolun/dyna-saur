use crate::{problem_json::{ToProblemJson, internal_server_error, not_found}, query::SqlCols, shared::scope::model::{HasScope, Scope, ScopeDescriptor}, sql, util::{Identifier, Timestamp, db_error_constraint}};
use derive_pgmap::DbMap;
use serde::{Serialize, Deserialize};
use tokio_postgres::{GenericClient};
use thiserror::Error;
use dyna_db::DbMap;

#[derive(Debug, Serialize, Deserialize)]
pub struct NewServiceGroup {
  pub id: Identifier,
  pub description: String,
  pub tenant_id: Identifier
}

#[derive(Debug, Error)]
pub enum CreateServiceGroupError {
  #[error("tenant_id does not exist")]
  MissingTenant,
  #[error("database error: {}", .0)]
  DbError(#[from] tokio_postgres::Error)
}

impl ToProblemJson for CreateServiceGroupError {
    fn to_problem_json(self) -> crate::problem_json::ProblemJson {
        match self {
            CreateServiceGroupError::MissingTenant => not_found(),
            CreateServiceGroupError::DbError(_) => internal_server_error(),
        }
    }
}

#[derive(Debug, Serialize, Deserialize, DbMap)]
pub struct ServiceGroup {
  pub id: Identifier,
  pub description: String,
  pub tenant_id: Identifier,
  pub created_at: Timestamp,
  pub updated_at: Timestamp
}

#[derive(Debug, Serialize, Deserialize, DbMap)]
pub struct ServiceGroupMembership {
  tenant_id: Identifier,
  service_group_id: Identifier,
  service_id: Identifier,
  created_at: Timestamp
}

const TABLE_NAME: &str = "service_groups";
const ALL_COLUMNS: SqlCols<5> = SqlCols([
  "id",
  "description",
  "tenant_id",
  "created_at",
  "updated_at"
]);

const INSERT_COLUMNS: SqlCols<3> = SqlCols([
  "id",
  "description",
  "tenant_id",
]);

const MEMBERSHIP_TABLE: &str = "service_group_memberships";

impl ServiceGroup {
  pub async fn create(conn: &impl GenericClient, new_sg: &NewServiceGroup) -> Result<ServiceGroup, CreateServiceGroupError> {
    let NewServiceGroup{ id, description, tenant_id} = new_sg;
    let q = sql!("
      INSERT INTO {:table} ({:columns})
      VALUES ({id}, {description}, {tenant_id})
      RETURNING {:all_columns}",
        :table = TABLE_NAME, :columns = INSERT_COLUMNS, id, description, tenant_id, :all_columns = ALL_COLUMNS);
    let result = conn.query_one(q.query.as_str(), q.params.as_slice()).await;
    if let Some("service_groups_tenant_id_fkey") = result.as_ref().err().and_then(db_error_constraint) {
      return Err(CreateServiceGroupError::MissingTenant)
    }
    Ok(ServiceGroup::db_map(result?))
  }

  pub async fn add_membership(conn: &impl GenericClient, tenant_id: &str, service_group_id: &str, service_id: &str) -> Result<ServiceGroupMembership, tokio_postgres::Error> {
    let q = sql!("
      INSERT INTO {:table} (tenant_id, service_group_id, service_id) VALUES ({t}, {sg}, {s}) RETURNING tenant_id, service_group_id, service_id, created_at
    ", :table = MEMBERSHIP_TABLE, t = tenant_id, sg = service_group_id, s = service_id);
    Ok(ServiceGroupMembership::db_map(q.query_one(conn).await?))
  }

  pub async fn scope(&self, client: &impl GenericClient) -> Result<Scope, tokio_postgres::Error> {
    Scope::get(client, ScopeDescriptor::ServiceGroup {tenant_id: self.tenant_id.clone(), service_group_id: self.id.clone() }).await
  }
}

impl HasScope for ServiceGroup {
  fn scope_descriptor(&self) -> ScopeDescriptor {
    ScopeDescriptor::ServiceGroup {tenant_id: self.tenant_id.clone(), service_group_id: self.id.clone()}
  }
}

#[cfg(test)]
mod tests {
  use dyna_saur_core::id::ToIdentifierExt;
use pretty_assertions::assert_eq;

use crate::{admin::{service::model::{NewService, Service}, tenant::model::{NewTenant, Tenant}}, util::test_transaction};

use super::*;

  #[actix_rt::test]
  async fn can_create() {
    let tx = test_transaction().await;
    let t = Tenant::create(&tx, NewTenant{ id: "t".to_identifier(), description: "desc".into() }).await.unwrap();

    let sg = ServiceGroup::create(&tx, &NewServiceGroup {
      id: "sg".try_into().unwrap(),
      description: "sg".into(),
      tenant_id: t.id.clone()
    }).await.unwrap();
    assert_eq!(sg.id, Identifier::new("sg".into()).unwrap());
  }

  #[actix_rt::test]
  async fn create_requires_valid_tenant() {
    let tx = test_transaction().await;
    let _t = Tenant::create(&tx, NewTenant{ id: "t".to_identifier(), description: "desc".into() }).await.unwrap();

    let e = ServiceGroup::create(&tx, &NewServiceGroup {
      id: "sg".try_into().unwrap(),
      description: "sg".into(),
      tenant_id: "t2".try_into().unwrap()
    }).await.unwrap_err();
    assert!(matches!(e, CreateServiceGroupError::MissingTenant));
  }

  #[actix_rt::test]
  async fn can_create_membership() {
    let tx = test_transaction().await;
    let t = Tenant::create(&tx, NewTenant{ id: "t".to_identifier(), description: "desc".into() }).await.unwrap();
    let s = Service::create(&tx, &NewService { tenant_id: t.id.to_string(), id: "s".into() }).await.unwrap();

    let sg = ServiceGroup::create(&tx, &NewServiceGroup {
      id: "sg".try_into().unwrap(),
      description: "sg".into(),
      tenant_id: t.id.clone()
    }).await.unwrap();

    ServiceGroup::add_membership(&tx, t.id.as_str(), sg.id.as_str(), s.id.as_str()).await.unwrap();
  }
}
