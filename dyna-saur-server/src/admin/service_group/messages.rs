use std::ops::Deref;

use actix::{Handler, Message, ResponseFuture};

use crate::{actors::RealDbActor, util::Identifier};

use super::model::{CreateServiceGroupError, NewServiceGroup, ServiceGroup, ServiceGroupMembership};

pub struct CreateServiceGroup(pub NewServiceGroup);

impl Message for CreateServiceGroup {
  type Result = Result<ServiceGroup, CreateServiceGroupError>;
}

impl Handler<CreateServiceGroup> for RealDbActor {
  type Result = ResponseFuture<<CreateServiceGroup as Message>::Result>;
  fn handle(&mut self, msg: CreateServiceGroup, _ctx: &mut Self::Context) -> Self::Result {
    let pool = self.pool.clone();
    Box::pin(async move {
      let client = pool.get().await.unwrap();
      ServiceGroup::create(client.deref(), &msg.0).await
    })
  }
}

pub struct AddServiceToGroup {
  pub tenant_id: Identifier,
  pub service_group_id: Identifier,
  pub service_id: Identifier
}

impl Message for AddServiceToGroup {
  type Result = Result<ServiceGroupMembership, tokio_postgres::Error>;
}

impl Handler<AddServiceToGroup> for RealDbActor {
  type Result = ResponseFuture<<AddServiceToGroup as Message>::Result>;
  fn handle(&mut self, msg: AddServiceToGroup, _ctx: &mut Self::Context) -> Self::Result {
    let pool = self.pool.clone();
    Box::pin(async move {
      let client = pool.get().await.unwrap();
      ServiceGroup::add_membership(client.deref(), msg.tenant_id.as_str(), msg.service_group_id.as_str(), msg.service_id.as_str()).await
    })
  }
}
