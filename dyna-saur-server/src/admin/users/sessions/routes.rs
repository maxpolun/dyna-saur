use actix_web::{Result, HttpResponse};

use crate::{shared::subject::model::SubjectCredential, problem_json::forbidden};

pub async fn get_csrf_token(sub: SubjectCredential) -> Result<HttpResponse> {
  match sub {
    SubjectCredential::User(session) => {
      Ok(HttpResponse::Ok().json(session.csrf_token()))
    },
    SubjectCredential::Service(_) => Err(forbidden().into()),
}
}
