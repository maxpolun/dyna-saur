use chrono::{Duration, Utc};
use derive_pgmap::DbMap;
use dyna_db::DbMap;
use dyna_saur_server_users::model::User;
use rand::{thread_rng, Rng};
use sha2::{Sha256, Digest};
use tokio_postgres::{GenericClient};

use crate::{query::SqlCols, sql, util::{Timestamp, ident::UniqueId}};


#[derive(Debug, Clone, DbMap)]
pub struct UserSession {
  pub id: UniqueId,
  pub user_id: UniqueId,
  pub csrf_seed: i64,
  pub created_at: Timestamp
}

impl UserSession {
  const TABLE: &'static str = "user_sessions";
  const ALL_COLS: SqlCols<4> = SqlCols(["id", "user_id", "csrf_seed", "created_at"]);
  const CREATE_COLS: SqlCols<3> = SqlCols(["id", "user_id", "csrf_seed"]);
  // default session length in seconds
  const DEFAULT_SESSION_LENGTH: i64 = 60 * 60 * 12;

  pub async fn create(client: &impl GenericClient, user_id: UniqueId) -> Result<UserSession, tokio_postgres::Error> {
    let id = UniqueId::new();
    let csrf_seed = thread_rng().gen::<i64>();
    sql!("INSERT INTO {:table} ({:insert_cols}) VALUES ({id}, {user_id}, {csrf_seed}) RETURNING {:all_cols}",
      :table = Self::TABLE, :insert_cols = Self::CREATE_COLS, :all_cols = Self::ALL_COLS,
      id, user_id, csrf_seed
    ).query_one(client).await.map(DbMap::db_map)
  }

  pub async fn get(client: &impl GenericClient, id: UniqueId) -> Result<UserSession, tokio_postgres::Error> {
    let id = id.to_string();

    sql!("SELECT {:cols} FROM {:table} WHERE id={id}", :cols = Self::ALL_COLS, :table = Self::TABLE, id)
        .query_one(client)
        .await
        .map(UserSession::db_map)
  }

  pub async fn delete(client: &impl GenericClient, id: UniqueId) -> Result<(), tokio_postgres::Error> {
    let id = id.to_string();

    sql!("DELETE FROM {:table} WHERE id={id}", :cols = Self::ALL_COLS, :table = Self::TABLE, id)
        .execute(client)
        .await
        .map(|_| ())
  }

  pub fn session_length() -> Duration {
    let num = std::env::var("SESSION_LENGTH_SECONDS").map(|sl| {
      sl.parse::<i64>().unwrap_or(Self::DEFAULT_SESSION_LENGTH)
    }).unwrap_or(Self::DEFAULT_SESSION_LENGTH);
    Duration::seconds(num)
  }

  pub fn valid(&self) -> bool {
    self.created_at + Self::session_length() >= Utc::now()
  }

  pub fn remaining_duration(&self) -> Duration {
    ((Utc::now() - self.created_at) + Self::session_length()).clamp(Duration::seconds(0), Self::session_length())
  }

  pub async fn login_local(client: &impl GenericClient, email: &str, password: &str) -> Result<Option<Self>, tokio_postgres::Error> {
    let user = match User::find_by_email(client, email).await? {
      None => return Ok(None),
      Some(u) => u
    };

    let cred = match user.local_credential(client).await? {
        Some(c) => c,
        None => return Ok(None),
    };

     if cred.validate_password(password) {
       Ok(Some(UserSession::create(client, user.id).await?))
     } else {
       Ok(None)
     }
  }

  pub async fn delete_expired(client: &impl GenericClient) -> Result<u64, tokio_postgres::Error> {
    let expired_time = Utc::now() - Self::session_length();
    sql!("DELETE FROM {:table} WHERE created_at < {expired_time}",
      :table = Self::TABLE, expired_time
    ).execute(client).await
  }

  pub fn csrf_token(&self) -> String {
    let token_id = thread_rng().gen::<[u8;4]>();
    let mut hasher = Sha256::new();
    hasher.update(&self.csrf_seed.to_le_bytes());
    hasher.update(&token_id);
    let hash = hasher.finalize();
    let hash_b64 = base64::encode(hash);
    let id_b64 = base64::encode(token_id);
    format!("{}:{}", id_b64, hash_b64)
  }

  pub fn csrf_token_is_valid(&self, token: &str) -> bool {
    if let Some((token_id, hash)) = (|| -> Option<(&str, &str)> {
      let mut i = token.split(':');
      Some((i.next()?, i.next()?))
    })() {
      if let (Ok(parsed_token), Ok(parsed_hash)) = (base64::decode(token_id), base64::decode(hash)) {
        let mut hasher = Sha256::new();
        hasher.update(&self.csrf_seed.to_le_bytes());
        hasher.update(&parsed_token);
        let new_hash = hasher.finalize();
        new_hash.as_slice() == parsed_hash.as_slice()
      } else {
        false
      }
    } else {
      false
    }
  }
}
