use std::ops::Deref;

use actix::{Message, Handler, ResponseFuture};

use crate::actors::RealDbActor;

use super::model::UserSession;


pub struct LoginUserLocalCredential {
  pub email: String,
  pub password: String
}

impl Message for LoginUserLocalCredential {
    type Result = Result<Option<UserSession>, tokio_postgres::Error>;
}

impl Handler<LoginUserLocalCredential> for RealDbActor {
    type Result = ResponseFuture<<LoginUserLocalCredential as Message>::Result>;

    fn handle(&mut self, msg: LoginUserLocalCredential, _ctx: &mut Self::Context) -> Self::Result {
      let pool = self.pool.clone();
      Box::pin(async move {
        let client = pool.get().await.unwrap();
        UserSession::login_local(client.deref(), &msg.email, &msg.password).await
      })
    }
}
