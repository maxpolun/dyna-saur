use std::{rc::Rc, future::Ready, pin::Pin, task::{Context, Poll}};

use actix_web::{dev::{Transform, ServiceRequest, ServiceResponse, Service}, Error, HttpMessage, error::ErrorForbidden};
use futures::Future;
use tracing::{Instrument, span, Level, warn};

use crate::shared::subject::model::SubjectCredential;

pub const CSRF_HEADER: &str = "X-Csrf-Token";

pub struct RequireCsrf;

pub struct RequireCsrfMiddleware<S> {
  service: Rc<S>
}

impl<S, B> Transform<S, ServiceRequest> for RequireCsrf
where
  S: Service<ServiceRequest, Response = ServiceResponse<B>, Error = Error> + 'static,
  S::Future: 'static + std::future::Future,
  B: 'static,
{
  type Response = ServiceResponse<B>;
  type Error = Error;
  type InitError = ();
  type Transform = RequireCsrfMiddleware<S>;
  type Future = Ready<Result<Self::Transform, Self::InitError>>;

  fn new_transform(&self, service: S) -> Self::Future {
    std::future::ready(Ok(RequireCsrfMiddleware {
      service: Rc::new(service),
    }))
  }
}

impl<S, B> Service<ServiceRequest> for RequireCsrfMiddleware<S>
where
  B: 'static,
  S: Service<ServiceRequest, Response = ServiceResponse<B>, Error = Error> + 'static,
  S::Future: 'static + std::future::Future,
{
  type Response = ServiceResponse<B>;
  type Error = Error;
  #[allow(clippy::type_complexity)]
  type Future = Pin<Box<dyn Future<Output = Result<Self::Response, Self::Error>>>>;

  fn poll_ready(&self, ctx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
    self.service.poll_ready(ctx)
  }

  fn call(&self, req: ServiceRequest) -> Self::Future {
    let srv = self.service.clone();

    Box::pin(async move {
      let session = req.extensions().get::<SubjectCredential>()
        .cloned()
        .and_then(|s| match s {
          SubjectCredential::User(session) => Some(session),
          _ => None
        });
      let header = req.headers().get(CSRF_HEADER)
        .and_then(|h| h.to_str().ok());
      let is_valid = match (session, header) {
        (Some(s), Some(h)) => s.csrf_token_is_valid(h),
        _ => false
      };

      if !is_valid {
        warn!("request without valid csrf token detected!");
        return Err(ErrorForbidden("Forbidden"))
      }

      srv.call(req).await
    }.instrument(span!(Level::INFO, "checking for csrf")))
  }
}
