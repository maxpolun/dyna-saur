use std::{ops::Deref};

use actix::{Handler, Message, ResponseFuture};

use crate::{actors::RealDbActor};

use super::model::{NewRoleAssignment, RoleAssignment};

pub use authorization::{Permission, HasGlobalPermission};

pub struct CreateRoleAssignment(pub NewRoleAssignment);

impl Message for CreateRoleAssignment {
  type Result = Result<RoleAssignment, tokio_postgres::Error>;
}

impl Handler<CreateRoleAssignment> for RealDbActor {
  type Result = ResponseFuture<<CreateRoleAssignment as Message>::Result>;
  fn handle(&mut self, msg: CreateRoleAssignment, _ctx: &mut Self::Context) -> Self::Result {
    let pool = self.pool.clone();
    Box::pin(async move {
      let client = pool.get().await.unwrap();
      RoleAssignment::create(client.deref(), msg.0).await
    })
  }
}
