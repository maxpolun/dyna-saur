use derive_pgmap::DbMap;
use dyna_db::DbMap;
use tokio_postgres::{GenericClient};
use tracing::debug;

use crate::{pagination::{GenericPaginatedApiError, PaginationFields, PaginationParams, SortOrder}, query::SqlCols, shared::scope::model::{Scope, ScopeDescriptor}, sql, util::{Timestamp, ident::UniqueId}};

#[derive(Debug, DbMap)]
pub struct RoleAssignment {
  pub user_id: UniqueId,
  pub scope_id: UniqueId,
  pub role_id: String,
  pub created_at: Timestamp,
  pub updated_at: Timestamp
}

pub struct NewRoleAssignment {
  pub user_id: UniqueId,
  pub scope_id: UniqueId,
  pub role_id: String,
}

impl RoleAssignment {
  const TABLE: &'static str = "role_assignments";
  const ALL_COLUMNS: SqlCols<5> = SqlCols(["user_id", "scope_id", "role_id", "created_at", "updated_at"]);
  const INSERT_COLUMNS: SqlCols<3> = SqlCols(["user_id", "scope_id", "role_id"]);

  pub const READER_ROLE: &'static str = "reader";
  pub const WRITER_ROLE: &'static str = "writer";
  pub async fn create(client: &impl GenericClient, nra: NewRoleAssignment) -> Result<RoleAssignment, tokio_postgres::Error> {
    let NewRoleAssignment { user_id, scope_id, role_id } = nra;
    let user_id = user_id.to_string();
    let scope_id_string = scope_id.to_string();
    let result = sql!(
      "INSERT INTO {:table} ({:cols}) VALUES ({user_id}, {scope_id}, {role_id}) RETURNING {:all_cols}",
      :table = Self::TABLE, :cols = Self::INSERT_COLUMNS, :all_cols = Self::ALL_COLUMNS,
      user_id, scope_id = scope_id_string, role_id
    ).query_one(client).await.map(RoleAssignment::db_map);

    result
  }

  pub async fn get(client: &impl GenericClient, user_id: UniqueId, scope_id: UniqueId, role_id: Option<&str>) -> Result<Option<RoleAssignment>, tokio_postgres::Error> {
    let user_id = user_id.to_string();
    let scope_id = scope_id.to_string();
    sql!(
      "SELECT {:cols} FROM {:table} WHERE user_id = {user_id} AND scope_id = {scope_id}{?role_id} AND role_id = {role_id}{/?role_id}",
      :cols = Self::ALL_COLUMNS, :table = Self::TABLE, user_id, scope_id, ?role_id
    ).query_opt(client).await.map(|opt| opt.map(RoleAssignment::db_map))
  }

  pub async fn get_all_by_scope_ids(client: &impl GenericClient, user_id: UniqueId, scope_ids: Vec<UniqueId>) -> Result<Vec<RoleAssignment>, tokio_postgres::Error> {
    Ok(sql!(
      "SELECT {:cols} FROM {:table} WHERE user_id = {user_id} AND scope_id = ANY({scope_ids})",
      :cols = Self::ALL_COLUMNS, :table = Self::TABLE,
      user_id, scope_ids).query(client).await?.into_iter().map(RoleAssignment::db_map).collect())
  }

  pub async fn get_all_scopeids_for_user(client: &impl GenericClient, user_id: UniqueId) -> Result<Vec<UniqueId>, tokio_postgres::Error> {
    let mut assigned_scopeids: Vec<UniqueId> = sql!(
      "SELECT DISTINCT scope_id FROM {:table} WHERE user_id = {user_id}",
      :table = Self::TABLE, user_id
    ).query(client).await?.into_iter().map(|row| row.get::<_, UniqueId>("scope_id")).collect();

    debug!("got {} direct assigned scopes for user {}", assigned_scopeids.len(), user_id);

    let global_scope = Scope::get(client, ScopeDescriptor::Global).await?;

    if assigned_scopeids.contains(&global_scope.id) {
      debug!("user {} has global scope, fetching all", user_id);
      return Scope::get_all(client, &PaginationParams::PaginationFields(PaginationFields {
        sort_by: "id".into(), sort_order: SortOrder::Decending, page_size: 1000
      }))
      .await
      .map(|result| result.items().iter().map(|s| s.id).collect())
      .map_err(|e| match e {
        GenericPaginatedApiError::Db(inner) => inner,
        e => panic!("only expected db error, got {}", e)
      })
    }

    debug!("fetching child scopeids");
    let mut child_scopeids: Vec<UniqueId> = sql!(
      "SELECT DISTINCT id FROM scopes WHERE tenant_id IN (
        SELECT DISTINCT tenant_id FROM scopes WHERE id = ANY ({ids}) AND service_id IS NULL AND service_group_id IS NULL
      )",
      ids = assigned_scopeids
    ).query(client).await?.into_iter().map(|row| row.get::<_, UniqueId>("id")).collect();
    debug!("got {} additional scopeids", child_scopeids.len());
    assigned_scopeids.append(&mut child_scopeids);
    Ok(assigned_scopeids)
  }
}

#[cfg(test)]
mod tests {
  use futures::future::join_all;

use crate::{admin::{service::model::{NewService, Service}, service_group::model::{NewServiceGroup, ServiceGroup}, tenant::model::{NewTenant, Tenant}, users::model::User}, shared::scope::model::{HasScope, Scope, ScopeDescriptor}, util::{ident::ToIdentifierExt, test_transaction}};

use super::*;

  #[actix_web::test]
  async fn can_create() {
    let tx = test_transaction().await;

    let s = Scope::get(&tx, ScopeDescriptor::Global).await.unwrap();
    let u = User::create(&tx, "test@example.com", "testuser").await.unwrap();

    RoleAssignment::create(&tx, NewRoleAssignment { user_id: u.id, scope_id: s.id, role_id: "reader".into() }).await.unwrap();
  }

  #[actix_web::test]
  async fn can_get() {
    let tx = test_transaction().await;

    let s = Scope::get(&tx, ScopeDescriptor::Global).await.unwrap();
    let u = User::create(&tx, "test@example.com", "testuser").await.unwrap();

    RoleAssignment::create(&tx, NewRoleAssignment { user_id: u.id, scope_id: s.id, role_id: "reader".into() }).await.unwrap();
    assert!(RoleAssignment::get(&tx, u.id, s.id, Some("reader")).await.unwrap().is_some());
    assert!(RoleAssignment::get(&tx, u.id, s.id, Some("writer")).await.unwrap().is_none());
  }

  #[actix_web::test]
  async fn can_get_assigned_scopes_for_user() {
    let tx = test_transaction().await;

    let u = User::create(&tx, "test@example.com", "testuser").await.unwrap();
    let t1 = Tenant::create(&tx, NewTenant { id: "t1".to_identifier(), description: "".into() }).await.unwrap();
    let t2 = Tenant::create(&tx, NewTenant { id: "t2".to_identifier(), description: "".into() }).await.unwrap();

    let s = Service::create(&tx, &NewService{ tenant_id: "t1".into(), id: "s".into() }).await.unwrap();
    let sg = ServiceGroup::create(&tx, &NewServiceGroup{ id: "sg".to_identifier(), description: "".into(), tenant_id: "t1".to_identifier() }).await.unwrap();

    let scopes = join_all(vec![
      t1.scope_descriptor(),
      t2.scope_descriptor(),
      s.scope_descriptor(),
      sg.scope_descriptor()
    ]
    .into_iter().map(|s| Scope::get(&tx, s)))
    .await.into_iter().map(|r|r.unwrap()).collect::<Vec<_>>();

    RoleAssignment::create(&tx, NewRoleAssignment{ user_id: u.id, scope_id: scopes[1].id, role_id: "reader".to_owned() }).await.unwrap();
    RoleAssignment::create(&tx, NewRoleAssignment{ user_id: u.id, scope_id: scopes[2].id, role_id: "writer".to_owned() }).await.unwrap();
    RoleAssignment::create(&tx, NewRoleAssignment{ user_id: u.id, scope_id: scopes[3].id, role_id: "reader".to_owned() }).await.unwrap();

    let assigned_ids = RoleAssignment::get_all_scopeids_for_user(&tx, u.id).await.unwrap();

    assert!(assigned_ids.contains(&scopes[1].id));
    assert!(assigned_ids.contains(&scopes[2].id));
    assert!(assigned_ids.contains(&scopes[3].id));

    assert!(!assigned_ids.contains(&scopes[0].id))
  }

  #[actix_web::test]
  async fn can_get_child_scopes() {
    let tx = test_transaction().await;

    let u = User::create(&tx, "test@example.com", "testuser").await.unwrap();
    let t1 = Tenant::create(&tx, NewTenant { id: "t1".to_identifier(), description: "".into() }).await.unwrap();

    let s = Service::create(&tx, &NewService{ tenant_id: "t1".into(), id: "s".into() }).await.unwrap();
    let sg = ServiceGroup::create(&tx, &NewServiceGroup{ id: "sg".to_identifier(), description: "".into(), tenant_id: "t1".to_identifier() }).await.unwrap();

    let scopes = join_all(vec![
      t1.scope_descriptor(),
      s.scope_descriptor(),
      sg.scope_descriptor()
    ]
    .into_iter().map(|s| Scope::get(&tx, s)))
    .await.into_iter().map(|r|r.unwrap()).collect::<Vec<_>>();

    RoleAssignment::create(&tx, NewRoleAssignment{ user_id: u.id, scope_id: scopes[0].id, role_id: "reader".to_owned() }).await.unwrap();

    let assigned_ids = RoleAssignment::get_all_scopeids_for_user(&tx, u.id).await.unwrap();

    assert!(assigned_ids.contains(&scopes[0].id));
    assert!(assigned_ids.contains(&scopes[1].id));
    assert!(assigned_ids.contains(&scopes[2].id));
  }

  #[actix_web::test]
  async fn user_with_global_scope_gets_all() {
    let tx = test_transaction().await;

    let u = User::create(&tx, "test@example.com", "testuser").await.unwrap();
    let t1 = Tenant::create(&tx, NewTenant { id: "t1".to_identifier(), description: "".into() }).await.unwrap();
    let t2 = Tenant::create(&tx, NewTenant { id: "t2".to_identifier(), description: "".into() }).await.unwrap();

    let s = Service::create(&tx, &NewService{ tenant_id: "t1".into(), id: "s".into() }).await.unwrap();
    let sg = ServiceGroup::create(&tx, &NewServiceGroup{ id: "sg".to_identifier(), description: "".into(), tenant_id: "t1".to_identifier() }).await.unwrap();

    let global_scope = Scope::get(&tx, ScopeDescriptor::Global).await.unwrap();

    println!("all scopes {:?}", tx.query("SELECT name FROM scopes", &[]).await.unwrap().iter().map(|r| r.get::<_, String>("name")).collect::<Vec<_>>());

    let scopes = vec![
      Scope::get(&tx, t1.scope_descriptor()).await.unwrap(),
      Scope::get(&tx, t2.scope_descriptor()).await.unwrap(),
      Scope::get(&tx, s.scope_descriptor()).await.unwrap(),
      Scope::get(&tx, sg.scope_descriptor()).await.unwrap()
    ];

    RoleAssignment::create(&tx, NewRoleAssignment{ user_id: u.id, scope_id: global_scope.id, role_id: "reader".to_owned() }).await.unwrap();
    let assigned_ids = RoleAssignment::get_all_scopeids_for_user(&tx, u.id).await.unwrap();

    for scope in scopes {
      assert!(assigned_ids.contains(&scope.id))
    }
  }
}
