use std::str::FromStr;
use actix_web::{
  web::{Data, Json, Path},
  HttpResponse,
};
use serde::{Deserialize, Serialize};
use serde_qs::actix::QsQuery;
use subject::Subject;
use tracing::{info, warn};

use crate::{admin::{tenant::{messages::GetTenantsById, model::CreateTenantError}, users::role_assignments::{messages::{Permission}}}, pagination::{PaginationFields, PaginationParams, SortOrder::Ascending}, problem_json::{ProblemJsonExt, ToProblemJson, conflict, internal_server_error, not_found, unprocessable_entity}, shared::{scope::model::ScopeDescriptor}, util::{Identifier, permissions::{permission_at_self_or_any_parent, user_has_global_permission}, msg::Msg}, DbAddr};

use super::{
  messages::{CreateTenant, DeleteTenant, IndexTenants, UpdateTenant},
  model::NewTenant,
};

pub async fn create_tenant(db: Data<DbAddr>, new_tenant: Json<NewTenant>, subject: Subject) -> actix_web::Result<HttpResponse> {
  let db = db.into_inner();
  user_has_global_permission(db.as_ref(), subject.clone(), Permission::Write).await?;
  match db
    .send(Msg::new(CreateTenant(new_tenant.into_inner()), Some(subject)))
    .await
    .map_err(ToProblemJson::to_problem_json)?
  {
    Ok(t) => {
      info!("successfully created tenant with id {}", t.id);
      Ok(HttpResponse::Ok().json(t))
    }
    Err(e) => {
      warn!("got error creating tenant: {:?}", e);
      match e {
        CreateTenantError::BadId => Err(unprocessable_entity().into()),
        CreateTenantError::AlreadyExists => Err(conflict().into()),
        _ => Err(internal_server_error().into()),
      }
    }
  }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct IndexParams {
  pagination: Option<PaginationParams>,
}

pub async fn index_tenants(db: Data<DbAddr>, params: QsQuery<IndexParams>, subject: Subject) -> actix_web::Result<HttpResponse> {
  let db = db.into_inner();
  user_has_global_permission(&db, subject, Permission::Read).await?;
  let params = params.into_inner().pagination.unwrap_or_else(|| {
    PaginationParams::PaginationFields(PaginationFields {
      sort_by: "id".into(),
      sort_order: Ascending,
      page_size: 25,
    })
  });
  let ts = db
    .send(IndexTenants(params))
    .await
    .err_problem_json()?
    .err_problem_json()?;
  Ok(HttpResponse::Ok().json(ts))
}

pub async fn get_tenant_by_id(db: Data<DbAddr>, id: Path<Identifier>, subject: Subject) -> actix_web::Result<HttpResponse> {
  let id = id.into_inner();
  let db = db.into_inner();
  permission_at_self_or_any_parent(&db, ScopeDescriptor::Tenant{tenant_id: id.clone()}, Permission::Read, &subject).await?;
  match db.send(GetTenantsById(vec![id.clone()])).await.err_problem_json()? {
    Ok(t) => {
      let tenant_opt = t.get(&id);
      if let Some(tenant) = tenant_opt {
        Ok(HttpResponse::Ok().json(tenant))
      } else {
        Err(not_found().into())
      }
    }
    Err(e) => {
      warn!("error getting tenant {}", e);
      Err(internal_server_error().into())
    }
  }
}

pub async fn tenant_report(db: Data<DbAddr>, ids: Json<Vec<Identifier>>, subject: Subject) -> actix_web::Result<HttpResponse> {
  let db = db.into_inner();
  user_has_global_permission(&db, subject, Permission::Read).await?;
  match db
    .send(GetTenantsById(ids.into_inner()))
    .await
    .map_err(ToProblemJson::to_problem_json)?
  {
    Ok(tenants) => Ok(HttpResponse::Ok().json(tenants)),
    Err(e) => {
      warn!("error getting tenant {}", e);
      Err(internal_server_error().into())
    }
  }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct PutTenantBody {
  description: String,
}

pub async fn update_tenant(
  db: Data<DbAddr>,
  id: Path<Identifier>,
  body: Json<PutTenantBody>,
  subject: Subject
) -> actix_web::Result<HttpResponse> {
  let db = db.into_inner();
  permission_at_self_or_any_parent(&db, ScopeDescriptor::Tenant{tenant_id: id.clone()}, Permission::Write, &subject).await?;
  let new = NewTenant {
    id: id.into_inner(),
    description: body.into_inner().description,
  };
  match db
    .send(Msg::new(UpdateTenant(new), Some(subject)))
    .await
    .map_err(ToProblemJson::to_problem_json)?
  {
    Ok(t) => Ok(HttpResponse::Ok().json(t)),
    Err(e) => {
      warn!("error updating tenant: {}", e);
      Err(internal_server_error().into())
    }
  }
}
pub async fn delete_tenant(db: Data<DbAddr>, id: Path<String>, subject: Subject) -> actix_web::Result<HttpResponse> {
  let db = db.into_inner();
  permission_at_self_or_any_parent(&db, ScopeDescriptor::Tenant{tenant_id: Identifier::from_str(&id).unwrap()}, Permission::Write, &subject).await?;
  match db
    .send(DeleteTenant(id.into_inner()))
    .await
    .map_err(ToProblemJson::to_problem_json)?
  {
    Ok(()) => Ok(HttpResponse::NoContent().finish()),
    Err(e) => {
      warn!("error deleting tenant: {}", e);
      Err(internal_server_error().into())
    }
  }
}

#[cfg(test)]
mod tests {
  use std::{collections::HashMap, str::FromStr};

  use super::*;
  use crate::{actors::RealDbActor, admin::{tenant::{
      messages::{CreateTenant, GetTenantsById},
      model::Tenant,
    }, users::{role_assignments::messages::HasGlobalPermission}}, pagination::{
      Cursor, GenericPaginatedApiError, InvalidPaginationError, Paginated, PaginationFields, SortOrder::Ascending,
    }, util::{ident::UniqueId, parse_http_error_json, parse_http_response, permissions::PermissionAtSelfOrAnyParent}};
  use actix_mock_helper::MockActorSequence;
use actix_web::http::StatusCode;
  use chrono::Utc;
  use dyna_saur_core::id::ToIdentifierExt;
use serde_json::Value;

  #[actix_rt::test]
  async fn can_create_tenant() {
    let addr = MockActorSequence::new()
        .msg(|_msg: &HasGlobalPermission| true )
        .msg(|msg: &Msg<CreateTenant>| Ok(Tenant {
          id: msg.payload.0.id.clone(),
          description: msg.payload.0.description.clone(),
          created_at: Utc::now(),
          updated_at: Utc::now(),
        }))
        .build::<RealDbActor>();

    let res = create_tenant(
      Data::new(addr),
      Json(NewTenant {
        id: "test".to_identifier(),
        description: "desc".into(),
      }),
      Subject::User {
        user_id: UniqueId::new(),
      }
    )
    .await
    .unwrap();

    assert_eq!(parse_http_response::<Tenant>(res).id.as_str(), "test");
  }

  #[actix_rt::test]
  async fn index_works() {
    let addr = MockActorSequence::new()
        .msg(|_msg: &HasGlobalPermission| true)
        .msg(|msg: &IndexTenants| {
          let fields = match msg.0.clone() {
            PaginationParams::Cursor(_) => panic!("wrong match arm"),
            PaginationParams::PaginationFields(f) => f,
          };
          let items: Vec<Tenant> = (0..10)
            .map(|i| Tenant {
              id: format!("tenant{}", i).to_identifier(),
              description: format!("tenant desc {}", i),
              created_at: Utc::now(),
              updated_at: Utc::now(),
            })
            .collect();
          Ok(Paginated::new(
            items.last().map(|t| Cursor {
              last_id: t.id.to_string(),
              fields,
            }),
            items,
          ))
        })
        .build();

    let res = index_tenants(
      Data::new(addr),
      QsQuery(IndexParams {
        pagination: Some(PaginationParams::PaginationFields(PaginationFields {
          sort_by: "id".into(),
          sort_order: Ascending,
          page_size: 10,
        })),
      }),
      Subject::User{
        user_id: UniqueId::new(),
      }
    )
    .await
    .unwrap();

    assert_eq!(res.status(), StatusCode::OK);
    let body = parse_http_response::<Paginated<Vec<Tenant>>>(res);
    assert_eq!(body.items().len(), 10);
  }

  #[actix_rt::test]
  async fn index_works_with_cursor() {
    let addr = MockActorSequence::new()
      .msg(|_msg: &HasGlobalPermission| true)
      .msg(|msg: &IndexTenants| {
        let cursor = match msg.0.clone() {
          PaginationParams::Cursor(c) => c,
          PaginationParams::PaginationFields(_) => panic!("wrong match arm"),
        };
        let fields = cursor.parse::<Cursor>().unwrap().fields;
        let items: Vec<Tenant> = (0..10)
          .map(|i| Tenant {
            id: format!("tenant{}", i).to_identifier(),
            description: format!("tenant desc {}", i),
            created_at: Utc::now(),
            updated_at: Utc::now(),
          })
          .collect();
        Ok(Paginated::new(
          items.last().map(|t| Cursor {
            last_id: t.id.to_string(),
            fields,
          }),
          items,
        ))
      })
        .build();

    let res = index_tenants(
      Data::new(addr),
      QsQuery(IndexParams {
        pagination: Some(PaginationParams::Cursor(
          Cursor {
            last_id: "tenant0".into(),
            fields: PaginationFields {
              sort_by: "id".into(),
              sort_order: Ascending,
              page_size: 10,
            },
          }
          .to_string(),
        )),
      }),
      Subject::User {
        user_id: UniqueId::new()
      }
    )
    .await
    .unwrap();

    assert_eq!(res.status(), StatusCode::OK);
    let body = parse_http_response::<Paginated<Vec<Tenant>>>(res);
    assert_eq!(body.items().len(), 10);
  }

  #[actix_rt::test]
  async fn index_422_with_bad_params() {
    let addr = MockActorSequence::new()
      .msg(|_msg: &HasGlobalPermission| true)
      .msg(|_msg: &IndexTenants|  {
        Err(GenericPaginatedApiError::Pagination(
          InvalidPaginationError::ExpiredCursor,
        ))
      })
      .build();

    let res = index_tenants(
      Data::new(addr),
      QsQuery(IndexParams {
        pagination: Some(PaginationParams::Cursor(
          Cursor {
            last_id: "tenant0".into(),
            fields: PaginationFields {
              sort_by: "id".into(),
              sort_order: Ascending,
              page_size: 10,
            },
          }
          .to_string(),
        )),
      }),
      Subject::User{
        user_id: UniqueId::new(),
      }
    )
    .await
    .unwrap_err()
    .as_response_error()
    .error_response();

    assert_eq!(res.status(), StatusCode::UNPROCESSABLE_ENTITY);
    let body = parse_http_error_json(res);
    assert_eq!(
      body["type"],
      Value::String("http://localhost:3500/probs/cursor-expired".into())
    );
  }

  #[actix_rt::test]
  async fn get_works() {
    let addr = MockActorSequence::new()
      .msg(|_msg: &PermissionAtSelfOrAnyParent| true)
      .msg(|msg: &GetTenantsById| {
        Ok(
          msg
            .0
            .iter()
            .map(|id| {
              (
                id.clone(),
                Tenant {
                  id: id.clone(),
                  description: format!("desc {}", id),
                  created_at: Utc::now(),
                  updated_at: Utc::now(),
                },
              )
            })
            .collect::<HashMap<Identifier, Tenant>>(),
        )
      })
        .build();

    let res = get_tenant_by_id(Data::new(addr), Identifier::from_str("t1").unwrap().into(), Subject::User {
      user_id: UniqueId::new(),

    }).await.unwrap();

    assert_eq!(res.status(), StatusCode::OK);
    let body = parse_http_response::<Tenant>(res);
    assert_eq!(body.id.as_str(), "t1");
  }

  #[actix_rt::test]
  async fn get_404s_when_tenant_missing() {
    let addr = MockActorSequence::new()
      .msg(|_msg: &PermissionAtSelfOrAnyParent| true)
      .msg(|_msg: &GetTenantsById| Ok(HashMap::new()))
      .build();

    let e = get_tenant_by_id(Data::new(addr), Identifier::from_str("t1").unwrap().into(), Subject::User {
      user_id: UniqueId::new(),
    })
      .await
      .unwrap_err();
    let res = e.as_response_error();

    assert_eq!(res.status_code(), StatusCode::NOT_FOUND);
  }

  #[actix_rt::test]
  async fn report_works() {
    let addr = MockActorSequence::new()
    .msg(|_msg: &HasGlobalPermission| true)
    .msg(|msg: &GetTenantsById| {
      Ok(
        msg
          .0
          .iter()
          .map(|id| {
            (
              id.clone(),
              Tenant {
                id: id.clone(),
                description: format!("desc {}", id),
                created_at: Utc::now(),
                updated_at: Utc::now(),
              },
            )
          })
          .collect::<HashMap<Identifier, Tenant>>(),
      )
    })
        .build();

    let res = tenant_report(
      Data::new(addr),
      Json((0..100).map(|i| format!("tenant_{}", i).to_identifier()).collect::<Vec<_>>()),
        Subject::User {
          user_id: UniqueId::new(),
        }
    )
    .await
    .unwrap();

    assert_eq!(res.status(), StatusCode::OK);
    let body = parse_http_response::<HashMap<String, Tenant>>(res);
    assert_eq!(body.len(), 100);
  }
}
