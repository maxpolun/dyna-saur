use std::{collections::HashMap, ops::Deref};

use actix::{Handler, Message, ResponseFuture};
use dyna_saur_core::id::Identifier;
use dyna_saur_server_audits::{Audit};
use tracing::{span, Instrument, Level};

use crate::{
  actors::RealDbActor,
  pagination::{GenericPaginatedApiError, Paginated, PaginationParams},
  util::msg::Msg,
};

use super::model::{CreateTenantError, NewTenant, Tenant};

pub struct CreateTenant(pub NewTenant);

impl Message for Msg<CreateTenant> {
  type Result = Result<Tenant, CreateTenantError>;
}

impl Handler<Msg<CreateTenant>> for RealDbActor {
  type Result = ResponseFuture<<Msg<CreateTenant> as Message>::Result>;
  fn handle(&mut self, msg: Msg<CreateTenant>, _ctx: &mut Self::Context) -> Self::Result {
    let pool = self.pool.clone();
    let span = span!(parent: msg.span.clone(), Level::INFO, "CreateTenant");
    Box::pin(
      async move {
        let mut client = pool.get().await.unwrap();
        let tx = client.transaction().await?;
        let who_changed_id = msg.subject_id();
        let tenant = Tenant::create(&tx, msg.payload.0).await?;
        Audit::created(&tx, tenant.clone(), &who_changed_id).await?;
        tx.commit().await?;
        Ok(tenant)
      }
      .instrument(span),
    )
  }
}

pub struct IndexTenants(pub PaginationParams);

impl Message for IndexTenants {
  type Result = Result<Paginated<Vec<Tenant>>, GenericPaginatedApiError>;
}

impl Handler<IndexTenants> for RealDbActor {
  type Result = ResponseFuture<<IndexTenants as Message>::Result>;

  fn handle(&mut self, msg: IndexTenants, _ctx: &mut Self::Context) -> Self::Result {
    let pool = self.pool.clone();
    Box::pin(async move {
      let client = pool.get().await.unwrap();
      Tenant::get_paginated(client.deref(), msg.0).await
    })
  }
}

pub struct GetTenantsById(pub Vec<Identifier>);

impl Message for GetTenantsById {
  type Result = Result<HashMap<Identifier, Tenant>, tokio_postgres::Error>;
}

impl Handler<GetTenantsById> for RealDbActor {
  type Result = ResponseFuture<<GetTenantsById as Message>::Result>;

  fn handle(&mut self, msg: GetTenantsById, _ctx: &mut Self::Context) -> Self::Result {
    let pool = self.pool.clone();
    Box::pin(async move {
      let client = pool.get().await.unwrap();
      let addrs = msg.0.iter().collect::<Vec<_>>();
      Tenant::resolve_ids(client.deref(), addrs.as_slice()).await
    })
  }
}

pub struct UpdateTenant(pub NewTenant);

impl Message for Msg<UpdateTenant> {
  type Result = Result<Tenant, tokio_postgres::Error>;
}

impl Handler<Msg<UpdateTenant>> for RealDbActor {
  type Result = ResponseFuture<<Msg<UpdateTenant> as Message>::Result>;

  fn handle(&mut self, msg: Msg<UpdateTenant>, _ctx: &mut Self::Context) -> Self::Result {
    let pool = self.pool.clone();
    let span = span!(parent: msg.span.clone(), Level::INFO, "UpdateTenant");
    let NewTenant { id, description } = msg.payload.0.clone();
    Box::pin(
      async move {
        let mut client = pool.get().await.unwrap();
        let tx = client.transaction().await?;
        let new_tenant = Audit::updated(&tx,
          Tenant::update(&tx, &id, &description),
          id.clone(),
          &msg.subject_id()
        ).await?;

        tx.commit().await?;
        Ok(new_tenant)
      }
      .instrument(span),
    )
  }
}

pub struct DeleteTenant(pub String);

impl Message for DeleteTenant {
  type Result = Result<(), tokio_postgres::Error>;
}

impl Handler<DeleteTenant> for RealDbActor {
  type Result = ResponseFuture<<DeleteTenant as Message>::Result>;

  fn handle(&mut self, msg: DeleteTenant, _ctx: &mut Self::Context) -> Self::Result {
    let pool = self.pool.clone();
    Box::pin(async move {
      let client = pool.get().await.unwrap();
      Tenant::delete(client.deref(), &msg.0).await
    })
  }
}
