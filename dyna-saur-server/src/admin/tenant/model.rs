use crate::{pagination::{GenericPaginatedApiError, Paginated, PaginatedQuery, PaginationParams}, shared::scope::model::{HasScope, Scope, ScopeDescriptor}, util::{PgMappingError, Timestamp, db_error_constraint}};
use async_trait::async_trait;
use derive_pgmap::DbMap;
use dyna_db::{DbError, DbMap};
use dyna_saur_core::id::Identifier;
use dyna_saur_server_audits::Auditable;
use serde::{Deserialize, Serialize};
use sqlquery::{SqlCols, sql};
use std::{
  collections::HashMap
};
use thiserror::Error;
use tokio_postgres::{Error, GenericClient};

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize, Clone, DbMap)]
pub struct Tenant {
  pub id: Identifier,
  pub description: String,
  pub created_at: Timestamp,
  pub updated_at: Timestamp,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct NewTenant {
  pub id: Identifier,
  pub description: String,
}

#[derive(Debug, Error)]
pub enum CreateTenantError {
  #[error("id must consist of only ascii alphanumeric characters and _")]
  BadId,
  #[error("Tenant already exists with this id")]
  AlreadyExists,
  #[error("error with database")]
  DbError(#[from] Error),
  #[error("error mapping from database")]
  MappingError(#[from] PgMappingError),
}

const SORTABLE_COLUMNS: [&str; 1] = ["id"];

impl Tenant {
  const TABLE_NAME: &'static str = "tenants";
  const ALL_COLS: SqlCols<4> = SqlCols(["id", "description", "created_at", "updated_at"]);
  const INSERT_COLS: SqlCols<2> = SqlCols(["id", "description"]);

  pub async fn create<Conn: GenericClient>(client: &Conn, new_tenant: NewTenant) -> Result<Tenant, CreateTenantError> {
    if !new_tenant.id.as_str().chars().all(|c| c.is_ascii_alphanumeric() || c == '_') {
      return Err(CreateTenantError::BadId);
    }
    let res = sql!(
        "INSERT INTO {:table} ({:insert_cols}) VALUES ({id}, {desc}) RETURNING {:all_cols}",
        :table = Self::TABLE_NAME, :insert_cols = Self::INSERT_COLS, :all_cols = Self::ALL_COLS,
        id = new_tenant.id, desc = new_tenant.description
      ).query_one(client)
      .await;
    if let Some(constraint) = res.as_ref().err().and_then(db_error_constraint) {
      if constraint.contains("tenants_pkey") {
        return Err(CreateTenantError::AlreadyExists);
      }
    };
    Ok(Tenant::db_map(res?))
  }

  pub async fn get_paginated<Conn: GenericClient>(
    client: &Conn,
    params: PaginationParams,
  ) -> Result<Paginated<Vec<Tenant>>, GenericPaginatedApiError> {
    let PaginatedQuery { fields, last_id } = params.clone().query()?;
    let (dir, cmp) = fields.sort_order();
    let col = fields.sort_col(&SORTABLE_COLUMNS)?;
    let page_size = fields.page_size()?;

    let items = sql!("
      SELECT {:cols} FROM {:table}
        {?id}
          WHERE id {:cmp} {id}
        {/?id}
        ORDER BY {col} {:dir} LIMIT {page_size}",
      :cols = Self::ALL_COLS, :table = Self::TABLE_NAME,
      ?id = last_id,
      :cmp, col, :dir, page_size
    )
      .query(client)
      .await?
      .into_iter()
      .map(DbMap::db_map)
      .collect::<Vec<Tenant>>();
    let cursor = items.last().map(|t| fields.into_cursor(t.id.as_str()));
    Ok(Paginated::new(cursor, items))
  }

  pub async fn resolve_ids(
    client: &impl GenericClient,
    ids: &[&Identifier],
  ) -> Result<HashMap<Identifier, Tenant>, tokio_postgres::Error> {
    let mut result = HashMap::new();
    let items = sql!(
      "SELECT {:cols} FROM tenants WHERE id = ANY ({ids})",
      :cols = Self::ALL_COLS, ids
    ).query(client).await?;
    for i in items {
      let t = Tenant::db_map(i);
      result.insert(t.id.clone(), t);
    }
    Ok(result)
  }

  pub async fn update(
    client: &impl GenericClient,
    id: &Identifier,
    description: &str,
  ) -> Result<Tenant, tokio_postgres::Error> {
    Ok(
      DbMap::db_map(client
        .query_one(
          "UPDATE tenants SET description=$1 WHERE id = $2 RETURNING id, description, created_at, updated_at",
          &[&description, &id],
        )
        .await?)
    )
  }

  pub async fn delete(client: &impl GenericClient, id: &str) -> Result<(), tokio_postgres::Error> {
    client.execute("DELETE FROM tenants WHERE id = $1", &[&id]).await?;
    Ok(())
  }

  pub async fn scope(&self, client: &impl GenericClient) -> Result<Scope, tokio_postgres::Error> {
    Scope::get(client, ScopeDescriptor::Tenant {tenant_id: self.id.clone()}).await
  }
}

impl HasScope for Tenant {
  fn scope_descriptor(&self) -> ScopeDescriptor {
    ScopeDescriptor::Tenant {tenant_id: self.id.clone()}
  }
}

#[async_trait(?Send)]
impl Auditable for Tenant {
    type Id = Identifier;

    fn table_name() -> &'static str {
        "tenants"
    }

    fn id(&self) -> Self::Id {
      self.id.clone()
    }

    async fn get_by_id(conn: &impl GenericClient, id: &Self::Id) -> Result<Option<Self>, DbError> {
      let mut ids = Tenant::resolve_ids(conn, &[id]).await?;
      Ok(ids.remove(id))
    }
}

#[cfg(test)]
mod tests {
  use dyna_saur_core::id::ToIdentifierExt;
use futures::future::join_all;
  use pretty_assertions::assert_eq;

  use super::*;
  use crate::{
    pagination::PaginationFields,
    util::{test_transaction},
  };

  #[actix_rt::test]
  async fn can_create_tenant() {
    let tx = test_transaction().await;
    let t1 = Tenant::create(
      &tx,
      NewTenant {
        id: "test".to_identifier(),
        description: "this is the description".into(),
      },
    )
    .await
    .unwrap();

    let t2 = Tenant::db_map(
      tx.query_one("SELECT * FROM tenants WHERE id = 'test'", &[])
        .await
        .unwrap(),
    );
    assert_eq!(t1, t2);
  }

  #[actix_rt::test]
  async fn can_fetch_tenants() {
    let tx = test_transaction().await;
    join_all((0..1000).map(|i: i32| {
      Tenant::create(
        &tx,
        NewTenant {
          id: format!("t{:03}", i).to_identifier(),
          description: format!("desc {}", i),
        },
      )
    }))
    .await;

    let result = Tenant::get_paginated(
      &tx,
      PaginationParams::PaginationFields(PaginationFields {
        sort_by: "id".into(),
        sort_order: crate::pagination::SortOrder::Ascending,
        page_size: 5,
      }),
    )
    .await
    .unwrap();

    let result2 = Tenant::get_paginated(&tx, PaginationParams::Cursor(result.cursor().unwrap().to_owned()))
      .await
      .unwrap();

    assert_eq!(result2.items().last().unwrap().id.as_str(), "t009")
  }

  #[actix_rt::test]
  async fn can_get_tenants_by_id() {
    let tx = test_transaction().await;
    join_all((0..1000).map(|i: i32| {
      Tenant::create(
        &tx,
        NewTenant {
          id: format!("t{}", i).to_identifier(),
          description: format!("desc {}", i),
        },
      )
    }))
    .await;

    let result = Tenant::resolve_ids(&tx, &[
      &"t1".to_identifier(),
      &"t5".to_identifier(),
      &"t99".to_identifier()
      ]).await.unwrap();

    assert_eq!(result.len(), 3);
  }

  #[actix_rt::test]
  async fn missing_tenants_arent_in_hashtable() {
    let tx = test_transaction().await;
    join_all((0..1000).map(|i: i32| {
      Tenant::create(
        &tx,
        NewTenant {
          id: format!("t{}", i).to_identifier(),
          description: format!("desc {}", i),
        },
      )
    }))
    .await;

    let result = Tenant::resolve_ids(&tx, &[
      &"t1".to_identifier(),
      &"t5".to_identifier(),
      &"x55".to_identifier()
    ]).await.unwrap();

    assert_eq!(result.len(), 2);
  }

  #[actix_rt::test]
  async fn can_update() {
    let tx = test_transaction().await;
    let t1 = Tenant::create(
      &tx,
      NewTenant {
        id: "test".to_identifier(),
        description: "this is the description".into(),
      },
    )
    .await
    .unwrap();

    let t2 = Tenant::update(&tx, &"test".to_identifier(), "changed desc").await.unwrap();

    assert_eq!(t1.id, t2.id);
    assert_eq!(t2.description, "changed desc");
  }

  #[actix_rt::test]
  async fn can_delete() {
    let tx = test_transaction().await;
    Tenant::create(
      &tx,
      NewTenant {
        id: "test".to_identifier(),
        description: "this is the description".into(),
      },
    )
    .await
    .unwrap();

    Tenant::delete(&tx, "test").await.unwrap();

    let t = Tenant::resolve_ids(&tx, &[&"test".to_identifier()]).await.unwrap();

    assert_eq!(t.len(), 0)
  }
}
