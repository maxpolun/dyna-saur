use std::{collections::HashMap};
use derive_pgmap::DbMap;
use dyna_saur_core::ConstraintOperation;
use tokio_postgres::{GenericClient};
use serde::{Serialize, Deserialize};
use tracing::debug;

use crate::{query::SqlCols, sql, util::{Timestamp, ident::UniqueId, DbMap}};

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq, Eq, DbMap)]
pub struct FieldValue {
  pub id: UniqueId,
  pub field_id: UniqueId,
  pub payload: serde_json::Value,
  pub ordinal: i32,
  pub created_at: Timestamp,
  pub updated_at: Timestamp
}

#[derive(Debug)]
pub struct NewFieldValue {
  pub field_id: UniqueId,
  pub payload: serde_json::Value,
  pub ordinal: i32
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq, Eq, DbMap)]
pub struct FieldValueConstraint {
  pub id: UniqueId,
  pub field_value_id: UniqueId,
  pub context_field: String,
  pub operation: ConstraintOperation,
  pub constrained_to_value: serde_json::Value,
  pub ordinal: i32,
  pub created_at: Timestamp,
  pub updated_at: Timestamp
}

#[derive(Debug)]
pub struct NewFieldValueConstraint {
  pub field_value_id: UniqueId,
  pub context_field: String,
  pub operation: ConstraintOperation,
  pub constrained_to_value: serde_json::Value,
  pub ordinal: i32,
}

impl FieldValue {
  const TABLE: &'static str = "field_values";
  const ALL_COLUMNS: SqlCols<6> = SqlCols(["id", "field_id", "payload", "ordinal", "created_at", "updated_at"]);

  pub async fn create(client: &impl GenericClient, NewFieldValue { field_id, payload, ordinal }: NewFieldValue) -> Result<FieldValue, tokio_postgres::Error> {
    let id = UniqueId::new();
    sql!(
      "INSERT INTO {:table} (id, field_id, payload, ordinal) VALUES ({id}, {field_id}, {payload}, {ordinal}) RETURNING {:all_cols}",
      :table = Self::TABLE, id, field_id, payload, ordinal, :all_cols = Self::ALL_COLUMNS
    ).query_one(client).await.map(FieldValue::db_map)
  }

  pub async fn get_all_by_field_ids(client: &impl GenericClient, field_ids: &[UniqueId]) -> Result<HashMap<UniqueId, Vec<FieldValue>>, tokio_postgres::Error> {
    Ok(sql!("SELECT {:cols} FROM {:table} WHERE field_id = ANY({field_ids})",
      :cols = FieldValue::ALL_COLUMNS, :table = FieldValue::TABLE, field_ids)
      .query(client).await?.into_iter().fold(HashMap::new(), |mut accum, item| {
        let val = FieldValue::db_map(item);
        accum.entry(val.field_id).or_insert_with(Vec::new).push(val);
        accum
      }))
  }
}

impl FieldValueConstraint {
  const TABLE: &'static str = "field_value_constraints";
  const ALL_COLUMNS: SqlCols<8> = SqlCols(["id", "field_value_id", "context_field",
      "operation", "constrained_to_value", "ordinal",
      "created_at", "updated_at"]);
  const INSERT_COLS: SqlCols<6> = SqlCols(["id", "field_value_id", "context_field", "operation", "constrained_to_value", "ordinal"]);

  pub async fn create(client: &impl GenericClient, NewFieldValueConstraint {
    field_value_id, context_field, operation, constrained_to_value, ordinal
  }: NewFieldValueConstraint) -> Result<FieldValueConstraint, tokio_postgres::Error> {
    let id = UniqueId::new().to_string();
    let field_value_id = field_value_id.to_string();
    sql!(
      "INSERT INTO {:table} ({:cols})
      VALUES ({id}, {field_value_id}, {context_field}, {operation}, {constrained_to_value}, {ordinal})
      RETURNING {:all_cols}",
      :table = Self::TABLE, :cols = Self::INSERT_COLS, id, field_value_id, context_field, operation, constrained_to_value, ordinal, :all_cols = Self::ALL_COLUMNS
    ).query_one(client).await.map(FieldValueConstraint::db_map)
  }

  pub async fn get_all_by_field_ids(client: &impl GenericClient, field_ids: &[UniqueId]) -> Result<HashMap<UniqueId, HashMap<UniqueId, Vec<Self>>>, tokio_postgres::Error>{
    let all_qualified = Self::ALL_COLUMNS.qualified(Self::TABLE);
    Ok(
      sql!("SELECT {:fv}.field_id, {:cols} FROM {:fv} LEFT JOIN {:fvc} ON {:fv}.id = {:fvc}.field_value_id WHERE {:fv}.field_id = ANY({field_ids}) AND {:fvc}.id IS NOT NULL",
        :cols = all_qualified, :fv = FieldValue::TABLE, :fvc = Self::TABLE, field_ids
      )
        .query(client)
        .await?
        .into_iter()
        .fold(HashMap::new(), |mut accum, r| {
          let field_id = r.get("field_id");
          debug!("row: {:?}", r);
          let constraint = FieldValueConstraint::db_map(r);
          accum.entry(field_id).or_default().entry(constraint.field_value_id).or_default().push(constraint);
          accum
        })
    )
  }
}


#[cfg(test)]
mod tests {
use dyna_saur_core::id::ToIdentifierExt;
use serde_json::json;
use tokio_postgres::Client;

use crate::{admin::{field::model::{FieldDefinition, NewFieldDefinition}, tenant::model::{NewTenant, Tenant}}, shared::scope::model::{Scope, ScopeDescriptor}, util::{test_transaction}};

  use super::*;

  async fn setup() -> (Client, Tenant, Scope, FieldDefinition) {
    let tx = test_transaction().await;
    let t = Tenant::create(&tx, NewTenant {
        id: "test_tenant".to_identifier(),
        description: "test".into(),
    }).await.unwrap();
    let s = Scope::get(&tx, ScopeDescriptor::Tenant{ tenant_id: t.id.clone() }).await.unwrap();
    let f = FieldDefinition::create(&tx, NewFieldDefinition {
        scope_id: s.id,
        name: "field1".into(),
        description: None,
    }).await.unwrap();

    (tx, t, s, f)
  }

  #[actix_rt::test]
  async fn can_create_field_value() {
    let (tx, _t, _s, f) = setup().await;

    FieldValue::create(&tx, NewFieldValue { field_id: f.id, payload: serde_json::Value::Bool(true), ordinal: 0 }).await.unwrap();
  }

  #[actix_rt::test]
  async fn can_create_field_value_constraint() {
    let (tx, _t, _s, f) = setup().await;

    let fv = FieldValue::create(&tx, NewFieldValue { field_id: f.id, payload: serde_json::Value::Bool(true), ordinal: 0 }).await.unwrap();
    FieldValueConstraint::create(&tx, NewFieldValueConstraint {
      field_value_id: fv.id, context_field: "test".into(), operation: ConstraintOperation::Equals, constrained_to_value: json!("x"), ordinal: 0
    }).await.unwrap();
  }
}
