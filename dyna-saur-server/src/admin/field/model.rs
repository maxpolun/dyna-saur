use std::{collections::HashMap};
use actix_web::http::StatusCode;
use derive_pgmap::DbMap;
use dyna_db::DbMap;
use dyna_saur_core::{ConstraintOperation, ClientField, ClientFieldValue, ClientFieldValueConstraint, id::UniqueId, Timestamp};
use futures::future::join_all;
use rand::Rng;
use tokio_postgres::{GenericClient};
use serde::{Serialize, Deserialize};
use tracing::{span, Level, debug};

use crate::{admin::users::role_assignments::model::RoleAssignment, pagination::{GenericPaginatedApiError, Paginated, PaginatedQuery, PaginationParams}, problem_json::{ProblemJson, ToProblemJson, internal_server_error}, query::SqlCols, shared::scope::model::{Scope, ScopeDescriptor}, sql};

use super::value::model::{FieldValue, FieldValueConstraint, NewFieldValue, NewFieldValueConstraint};

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq, DbMap)]
pub struct FieldDefinition {
  pub id: UniqueId,
  pub scope_id: UniqueId,
  pub name: String,
  pub description: Option<String>,
  pub salt: i32,
  pub created_at: Timestamp,
  pub updated_at: Timestamp
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct NewFieldDefinition {
  pub scope_id: UniqueId,
  pub name: String,
  pub description: Option<String>,
}

#[derive(Debug, thiserror::Error)]
pub enum NewFieldDefError {
  #[error("fields cannot be created at global scope")]
  InvalidScope,
  #[error("scope does not exist: {0}")]
  MissingScope(UniqueId),
  #[error("database error: {0}")]
  Db(#[from] tokio_postgres::Error),
}

impl ToProblemJson for NewFieldDefError {
    fn to_problem_json(self) -> crate::problem_json::ProblemJson {
        let msg = self.to_string();
        match self {
            NewFieldDefError::InvalidScope => ProblemJson::new_custom(
              StatusCode::UNPROCESSABLE_ENTITY, "invalid-field-scope-id", msg
            ),
            NewFieldDefError::MissingScope(_) => ProblemJson::new_custom(
              StatusCode::UNPROCESSABLE_ENTITY, "field-scope-id-missing", "scope_id does not exist".into()
            )
            .set_detail(msg),
            NewFieldDefError::Db(_) => internal_server_error(),
        }
    }
}

const TABLE_NAME: &str = "fields";

const ALL_COLUMNS: SqlCols<7> = SqlCols([
  "id",
  "scope_id",
  "name",
  "description",
  "salt",
  "created_at",
  "updated_at",
]);

const CREATE_COLUMNS: SqlCols<5> = SqlCols([
  "id",
  "scope_id",
  "name",
  "description",
  "salt",
]);

const SORTABLE_COLUMNS: &[&str]= &[
  "name",
  "updated_at"
];

impl FieldDefinition {
  pub async fn create(client: &impl GenericClient, new: NewFieldDefinition) -> Result<FieldDefinition, NewFieldDefError> {
    let NewFieldDefinition {scope_id, name, description, } = new;
    let scope = Scope::get_by_id_opt(client, scope_id).await?;
    let scope = if let Some(s) = scope { s } else { return Err(NewFieldDefError::MissingScope(scope_id))};
    if scope.descriptor == ScopeDescriptor::Global {
      return Err(NewFieldDefError::InvalidScope)
    }
    let id = UniqueId::new();
    let salt = rand::thread_rng().gen::<i32>();
    let query = sql!("
      INSERT INTO {:table_name} ({:cols})
      VALUES
        ({id}, {scope_id}, {name}, {description}, {salt})
      RETURNING
        {:all_cols}",
      :table_name = TABLE_NAME, :cols = CREATE_COLUMNS, :all_cols = ALL_COLUMNS, id, scope_id, name, description, salt);
    let row = query.query_one(client).await?;
    Ok(FieldDefinition::db_map(row))
  }

  pub async fn get_all_accessible(client: &impl GenericClient, user_id: UniqueId, filter_name: Option<String>, params: PaginationParams) -> Result<Paginated<Vec<FieldDefinition>>, GenericPaginatedApiError> {
    let _span = span!(Level::DEBUG, "getting all accessible fields for user", %user_id);
    let assigned_scopes = RoleAssignment::get_all_scopeids_for_user(client, user_id).await?;
    debug!("got {} scopes while trying to find all fields for user {}", assigned_scopes.len(), user_id);

    let PaginatedQuery { fields, last_id } = params.query()?;
    let (dir, cmp) = fields.sort_order();
    let col = fields.sort_col(SORTABLE_COLUMNS)?;
    let page_size = fields.page_size()?;

    let items = sql!("
          SELECT {:cols} FROM {:table} WHERE scope_id = ANY ({assigned_scopes}) {?last_id} AND {:col} {:cmp} {last_id}{/?last_id}
          {?name} AND name = {name} {/?name}
          ORDER BY {:col} {:dir} LIMIT {page_size}",
          :cols = ALL_COLUMNS, :table = TABLE_NAME, assigned_scopes,
          ?name = filter_name,
          :dir, :col, page_size, :cmp, ?last_id
        ).query(client).await?
        .into_iter().map(FieldDefinition::db_map).collect::<Vec<_>>();

    let cursor = items.last().map(|t| {
      match col {
        "name" => fields.into_cursor(&t.name),
        "updated_at" => fields.into_cursor(&t.updated_at.to_string()),
        _ => panic!("unexpected column {}", col)
      }
    });
    Ok(Paginated::new(cursor, items))
  }

  pub async fn get_all_in_scopes(client: &impl GenericClient, scopes: &[UniqueId]) -> Result<Vec<FieldDefinition>, tokio_postgres::Error> {
    Ok(sql!("
          SELECT {:cols} FROM {:table} WHERE scope_id = ANY ({scopes})",
          :cols = ALL_COLUMNS, :table = TABLE_NAME, scopes
    )
      .query(client)
      .await?
      .into_iter().map(FieldDefinition::db_map)
      .collect::<Vec<_>>())
  }
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct Field {
  pub definition: FieldDefinition,
  pub values: Vec<FieldValueDef>
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct FieldValueDef {
  pub definition: FieldValue,
  pub constraints: Vec<FieldValueConstraint>
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct CreateFieldValueConstraint {
  pub context_field: String,
  pub operation: ConstraintOperation,
  pub constrained_to_value: serde_json::Value,
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct CreateFieldValue {
  pub payload: serde_json::Value,
  #[serde(default)]
  pub constraints: Vec<CreateFieldValueConstraint>
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct NewField {
  pub definition: NewFieldDefinition,
  #[serde(default)]
  pub values: Vec<CreateFieldValue>
}

impl Field {
  pub async fn create(client: &impl GenericClient, new_field: NewField) -> Result<Field, NewFieldDefError> {
    let f = FieldDefinition::create(client, new_field.definition).await?;
    let fid = f.id;
    let values = join_all(
      new_field.values.into_iter().enumerate().map(|(i, new_val)| {
      async move {
        let CreateFieldValue { payload, constraints } = new_val;
        let v = FieldValue::create(client, NewFieldValue { field_id: fid, payload, ordinal: i as i32 }).await?;
        let vid = v.id;
        let c = join_all(
          constraints.into_iter().enumerate().map(|(j, new_constraint)| async move {
          FieldValueConstraint::create(client, NewFieldValueConstraint {
            field_value_id: vid, context_field: new_constraint.context_field, operation: new_constraint.operation, constrained_to_value: new_constraint.constrained_to_value, ordinal: j as i32
          }).await
        })).await.into_iter().collect::<Result<Vec<_>, _>>()?;

        Ok::<_, tokio_postgres::Error>(FieldValueDef {
          definition: v,
          constraints: c
        })
      }
    })).await.into_iter().collect::<Result<Vec<_>, _>>()?;
    Ok(Field {
      definition: f,
      values
    })
  }

  pub async fn get_all_by_ids(client: &impl GenericClient, ids: Vec<UniqueId>) -> Result<Vec<Field>, tokio_postgres::Error> {

    let defs = sql!(
      "SELECT {:cols} FROM {:table} WHERE id = ANY({ids}) ORDER BY id",
      :cols = ALL_COLUMNS, :table = TABLE_NAME, ids)
      .query(client).await?.into_iter().map(|r| {
        let def = FieldDefinition::db_map(r);
        (def.id, def)
      }).collect::<HashMap<_, _>>();
    let vals = FieldValue::get_all_by_field_ids(client, &ids).await?;
    let constraints = FieldValueConstraint::get_all_by_field_ids(client, &ids).await?;

    Ok(ids
      .into_iter()
      .map(|id| {
        Field {
          definition: defs[&id].clone(),
          values: vals.get(&id).map(|vals_for_field| vals_for_field.iter().cloned().map(|field_value| {
            let constraints_for_field = constraints.get(&id).and_then(|cs| {
              cs.get(&field_value.id).cloned()
            }).unwrap_or_default();
            FieldValueDef {
                definition: field_value,
                constraints: constraints_for_field,
            }
          }).collect()).unwrap_or_else(Vec::new)
        }
      }).collect()
    )
  }

  pub fn to_client(self) -> ClientField {
    ClientField {
        id: self.definition.id,
        name: self.definition.name,
        scope_id: self.definition.scope_id,
        salt: self.definition.salt,
        values: self.values.into_iter().map(|v| ClientFieldValue {
            id: v.definition.id,
            payload:v.definition.payload,
            constraints: v.constraints.into_iter().map(|c| ClientFieldValueConstraint {
                id: c.id,
                context_field: c.context_field,
                operation: c.operation,
                constrained_to_value: c.constrained_to_value,
            }).collect(),
        }).collect(),
    }
  }
}

#[cfg(test)]
mod tests {
    use futures::future::try_join_all;
    use serde_json::json;
    use tokio_postgres::Client;

    use crate::{admin::{service::model::{NewService, Service}, service_group::model::{NewServiceGroup, ServiceGroup}, tenant::model::{NewTenant, Tenant}, users::{model::User, role_assignments::model::NewRoleAssignment}}, pagination::{PaginationFields, SortOrder}, shared::scope::model::{HasScope, Scope, ScopeDescriptor}, util::{ident::ToIdentifierExt, test_transaction}};

    use super::*;


  #[actix_rt::test]
  async fn can_create_def () {
    let client = test_transaction().await;
    let t = Tenant::create(&client, NewTenant {
        id: "t1".to_identifier(),
        description: "t1".into(),
    }).await.unwrap();
    let s = Scope::get(&client, ScopeDescriptor::Tenant{ tenant_id: t.id.clone() }).await.unwrap();

    FieldDefinition::create(&client, NewFieldDefinition {
      scope_id: s.id,
      name: "test_flag".into(),
      description: None
    }).await.unwrap();
  }

  #[actix_rt::test]
  async fn can_create_field () {
    let client = test_transaction().await;
    let t = Tenant::create(&client, NewTenant {
        id: "t1".to_identifier(),
        description: "t1".into(),
    }).await.unwrap();
    let s = Scope::get(&client, ScopeDescriptor::Tenant{ tenant_id: t.id.clone() }).await.unwrap();

    Field::create(&client, NewField {
      definition: NewFieldDefinition {
        scope_id: s.id,
        name: "test_flag".into(),
        description: None
      },
      values: vec![
        CreateFieldValue {
          payload: json!(5),
          constraints: vec![
            CreateFieldValueConstraint {
              context_field: "test".into(),
              constrained_to_value: json!(true),
              operation: ConstraintOperation::Equals
            }
          ]
        },
        CreateFieldValue {
          payload: json!(0),
          constraints: Vec::new()
        }
      ],
    }).await.unwrap();
  }

  struct SetupData {
    u: User,
    // t1: Tenant,
    // t2: Tenant,
    // s: Service,
    // sg: ServiceGroup,
    scopes: Vec<Scope>,
  }

  async fn setup(tx: &Client) -> SetupData {
    let u = User::create(tx, "test@example.com", "testuser").await.unwrap();
    let t1 = Tenant::create(tx, NewTenant { id: "t1".to_identifier(), description: "".into() }).await.unwrap();
    let t2 = Tenant::create(tx, NewTenant { id: "t2".to_identifier(), description: "".into() }).await.unwrap();

    let s = Service::create(tx, &NewService{ tenant_id: "t1".into(), id: "s".into() }).await.unwrap();
    let sg = ServiceGroup::create(tx, &NewServiceGroup{ id: "sg".to_identifier(), description: "".into(), tenant_id: "t1".to_identifier() }).await.unwrap();

    let scopes = join_all(vec![
      t1.scope_descriptor(),
      t2.scope_descriptor(),
      s.scope_descriptor(),
      sg.scope_descriptor()
    ]
    .into_iter().map(|s| Scope::get(tx, s)))
    .await.into_iter().map(|r|r.unwrap()).collect::<Vec<_>>();

    RoleAssignment::create(tx, NewRoleAssignment{ user_id: u.id, scope_id: scopes[0].id, role_id: "reader".to_owned() }).await.unwrap();

    SetupData {
      u,
      // t1,
      // t2,
      // s,
      // sg,
      scopes
    }
  }

  #[actix_rt::test]
  async fn can_get_all_accessible_fields () {
    let client = test_transaction().await;
    let data = setup(&client).await;

    for scope in data.scopes {
      Field::create(&client, NewField {
        definition: NewFieldDefinition {
          scope_id: scope.id,
          name: scope.name.clone(),
          description: None
        },
        values: Vec::new(),
      }).await.unwrap();
    }

    let defs = FieldDefinition::get_all_accessible(
      &client,
      data.u.id,
      None,
      PaginationParams::PaginationFields(PaginationFields{ sort_by: "name".into(), sort_order: SortOrder::Ascending, page_size: 3 })
    ).await.unwrap();

    assert_eq!(defs.items().len(), 3);

  }

  #[actix_rt::test]
  async fn can_filter_all_accessible_fields_by_name () {
    let client = test_transaction().await;
    let data = setup(&client).await;

    for scope in data.scopes.iter() {
      Field::create(&client, NewField {
        definition: NewFieldDefinition {
          scope_id: scope.id,
          name: "test1".into(),
          description: None
        },
        values: Vec::new(),
      }).await.unwrap();
    }

    for scope in data.scopes.iter() {
      Field::create(&client, NewField {
        definition: NewFieldDefinition {
          scope_id: scope.id,
          name: "test2".into(),
          description: None
        },
        values: Vec::new(),
      }).await.unwrap();
    }

    let defs = FieldDefinition::get_all_accessible(
      &client,
      data.u.id,
      Some("test1".into()),
      PaginationParams::PaginationFields(PaginationFields{ sort_by: "name".into(), sort_order: SortOrder::Ascending, page_size: 25 })
    ).await.unwrap();

    assert_eq!(defs.items().len(), 3);
  }

  #[actix_rt::test]
  async fn can_get_fields_by_id () {
    let client = test_transaction().await;
    let data = setup(&client).await;

    let fields = try_join_all(data.scopes.iter().map(|scope| {
      Field::create(&client, NewField {
        definition: NewFieldDefinition {
          scope_id: scope.id,
          name: scope.name.clone(),
          description: None
        },
        values: (0..5).map(|i| {
          CreateFieldValue {
            payload: i.into(),
            constraints: (0..2).map(|j| CreateFieldValueConstraint {
                context_field: "a".into(),
                operation: ConstraintOperation::Equals,
                constrained_to_value: j.into(),
            }).collect()
          }
        }).collect(),
      })
    })).await.unwrap();

    let new_fields = Field::get_all_by_ids(&client, fields.iter().map(|f| f.definition.id).collect()).await.unwrap();

    pretty_assertions::assert_eq!(fields, new_fields)
  }
}
