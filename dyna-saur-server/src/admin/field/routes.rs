use std::str::FromStr;

use actix_web::{HttpResponse, Result, web::{Data, Json}};
use serde::{Serialize, Deserialize};
use serde_qs::actix::QsQuery;

use crate::{DbAddr, pagination::{PaginationParams, PaginationFields, SortOrder, Paginated, Cursor}, problem_json::unauthorized, shared::subject::{model::SubjectCredential}};
use crate::problem_json::ProblemJsonExt;

use super::{messages::{CreateField, GetAccessibleFieldDefs, GetFieldsByIds}, model::NewField};

pub async fn create_field(
  db: Data<DbAddr>,
  body: Json<NewField>,
) -> Result<HttpResponse> {
  let field = db.send(CreateField(body.into_inner())).await.err_problem_json()?.err_problem_json()?;
  Ok(HttpResponse::Ok().json(field))
}

#[derive(Debug, Serialize, Deserialize)]
pub struct IndexFieldsParams {
  pub pagination: Option<PaginationParams>,
  pub name: Option<String>,
  pub embed_children: Option<bool>
}

pub async fn index_fields(
  db: Data<DbAddr>,
  params: QsQuery<IndexFieldsParams>,
  subject: SubjectCredential
) -> Result<HttpResponse> {
  let session = match subject {
    SubjectCredential::User(session) => session,
    SubjectCredential::Service(_) => return Err(unauthorized().into()),
  };
  let params = params.into_inner();
  let field_defs = db.send(GetAccessibleFieldDefs {
    user_id: session.user_id,
    name: params.name,
    params: params.pagination.unwrap_or_else(|| PaginationParams::PaginationFields(PaginationFields {
      sort_by: "name".into(), sort_order: SortOrder::Ascending, page_size: 100
    })),
  }).await.err_problem_json()?.err_problem_json()?;

  if params.embed_children.unwrap_or(false) {
    let full_fields = db.send(GetFieldsByIds(field_defs.items().iter().map(|d| d.id).collect())).await.err_problem_json()?.err_problem_json()?;
    let pag_fields = Paginated::new(field_defs.cursor().map(|s| Cursor::from_str(s).unwrap()), full_fields);
    Ok(HttpResponse::Ok().json(pag_fields))
  } else {
    Ok(HttpResponse::Ok().json(field_defs))
  }

}
