use std::ops::Deref;

use actix::{Handler, Message, ResponseFuture};
use pagination::{PaginationParams, Paginated, GenericPaginatedApiError};
use crate::{actors::RealDbActor, util::ident::UniqueId};

use super::{model::{Field, FieldDefinition, NewField, NewFieldDefError}};

pub struct CreateField(pub NewField);

impl Message for CreateField {
  type Result = Result<Field, NewFieldDefError>;
}

impl Handler<CreateField> for RealDbActor {
  type Result = ResponseFuture<<CreateField as Message>::Result>;
  fn handle(&mut self, msg: CreateField, _ctx: &mut Self::Context) -> Self::Result {
    let pool = self.pool.clone();
    Box::pin(async move {
      let mut client = pool.get().await.unwrap();
      let tx = client.transaction().await.unwrap();
      let f = Field::create(&tx, msg.0).await?;
      tx.commit().await?;
      Ok(f)
    })
  }
}

pub struct GetAccessibleFieldDefs {
  pub user_id: UniqueId,
  pub params: PaginationParams,
  pub name: Option<String>
}

impl Message for GetAccessibleFieldDefs {
  type Result = Result<Paginated<Vec<FieldDefinition>>, GenericPaginatedApiError>;
}

impl Handler<GetAccessibleFieldDefs> for RealDbActor {
  type Result = ResponseFuture<<GetAccessibleFieldDefs as Message>::Result>;
  fn handle(&mut self, msg: GetAccessibleFieldDefs, _ctx: &mut Self::Context) -> Self::Result {
    let pool = self.pool.clone();
    Box::pin(async move {
      let client = pool.get().await.unwrap();
      FieldDefinition::get_all_accessible(client.deref(), msg.user_id, msg.name, msg.params).await
    })
  }
}

pub struct GetFieldsByIds(pub Vec<UniqueId>);

impl Message for GetFieldsByIds {
    type Result = Result<Vec<Field>, tokio_postgres::Error>;
}

impl Handler<GetFieldsByIds> for RealDbActor {
  type Result = ResponseFuture<<GetFieldsByIds as Message>::Result>;
  fn handle(&mut self, msg: GetFieldsByIds, _ctx: &mut Self::Context) -> Self::Result {
    let pool = self.pool.clone();
    Box::pin(async move {
      let client = pool.get().await.unwrap();
      Field::get_all_by_ids(client.deref(), msg.0).await
    })
  }
}
