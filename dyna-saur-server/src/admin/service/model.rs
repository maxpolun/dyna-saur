use actix_web::http::StatusCode;
use derive_pgmap::DbMap;
use dyna_db::DbMap;
use serde::{Deserialize, Serialize};
use thiserror::Error;
use tokio_postgres::{GenericClient};
use url::Url;

use crate::{pagination::{GenericPaginatedApiError, Paginated, PaginatedQuery, PaginationParams}, problem_json::{internal_server_error, not_found, ProblemJson, ToProblemJson}, query::SqlCols, shared::scope::model::{HasScope, Scope, ScopeDescriptor}, sql, util::{Timestamp, db_error_constraint, ident::ToIdentifierExt}};

#[derive(Debug, Serialize, Deserialize, DbMap)]
pub struct Service {
  pub id: String,
  pub tenant_id: String,
  pub created_at: Timestamp,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct NewService {
  pub tenant_id: String,
  pub id: String,
}

#[derive(Debug, Error)]
pub enum CreateServiceError {
  #[error("id must consist of only ascii alphanumeric characters and _")]
  BadId,
  #[error("Tenant does not exist")]
  NoTenant,
  #[error("Service with this id already exists for this tenant")]
  AlreadyExists,
  #[error("Database Error: {0}")]
  DbError(#[from] tokio_postgres::Error),
}

impl ToProblemJson for CreateServiceError {
  fn to_problem_json(self) -> crate::problem_json::ProblemJson {
    let msg = format!("{}", &self);
    match self {
      CreateServiceError::BadId => ProblemJson::new(
        StatusCode::UNPROCESSABLE_ENTITY,
        Url::parse("/probs/bad-id").unwrap(),
        msg,
      ),
      CreateServiceError::NoTenant => not_found(),
      CreateServiceError::AlreadyExists => ProblemJson::new(StatusCode::CONFLICT, Url::parse("input").unwrap(), msg),
      _ => internal_server_error(),
    }
  }
}

const ALL_COLUMNS: SqlCols<3> = SqlCols(["id", "tenant_id", "created_at"]);
const CREATE_COLUMNS: SqlCols<2> = SqlCols(["id", "tenant_id"]);
const SORTABLE_COLUMNS: [&str; 1] = ["id"];

impl Service {
  pub async fn create(client: &impl GenericClient, new_service: &NewService) -> Result<Service, CreateServiceError> {
    if !new_service.id.chars().all(|c| c.is_ascii_alphanumeric() || c == '_') {
      return Err(CreateServiceError::BadId);
    }
    let q = sql!(
      "INSERT INTO services ({:cols}) VALUES ({id}, {tenant_id}) RETURNING {:all_cols}",
      :cols = CREATE_COLUMNS, :all_cols = ALL_COLUMNS, id = new_service.id, tenant_id = new_service.tenant_id,);
    let result = client.query_one(q.query.as_str(), q.params.as_slice()).await;
    match result.as_ref().err().and_then(db_error_constraint) {
      Some("services_pkey") => return Err(CreateServiceError::AlreadyExists),
      Some("services_tenant_id_fkey") => return Err(CreateServiceError::NoTenant),
      _ => {}
    }
    Ok(Service::db_map(result?))
  }

  pub async fn index(
    client: &impl GenericClient,
    tenant_id: &str,
    pagination: &PaginationParams,
  ) -> Result<Paginated<Vec<Service>>, GenericPaginatedApiError> {
    let PaginatedQuery { fields, last_id } = pagination.clone().query()?;

    let (dir, cmp) = fields.sort_order();
    let col = fields.sort_col(&SORTABLE_COLUMNS)?;
    let page_size = fields.page_size()?;
    let items= match last_id {
      None => {
        let q = sql!(
          "SELECT {:cols} FROM services WHERE tenant_id = {tenant} ORDER BY {col} {:dir} LIMIT {page_size}",
          :cols = ALL_COLUMNS, dir, tenant = tenant_id, col, :dir, page_size);
        client.query(q.query.as_str(), q.params.as_slice())
            .await?
      },
      Some(last_id) => {
        let q = sql!(
          "SELECT {:cols} FROM services WHERE tenant_id = {tenant} AND id {:cmp} {service_id} ORDER BY {col} {:dir} LIMIT {page_size}",
          :cols = ALL_COLUMNS, :cmp, service_id = last_id, :dir, tenant = tenant_id, col, dir, page_size);
        client.query(q.query.as_str(), q.params.as_slice())
            .await?
      }
    }.into_iter()
      .map(DbMap::db_map)
      .collect::<Vec<Service>>();
    let cursor = items.last().map(|t| fields.into_cursor(&t.id));
    Ok(Paginated::new(cursor, items))
  }

  pub async fn scope(&self, client: &impl GenericClient) -> Result<Scope, tokio_postgres::Error> {
    Scope::get(client, ScopeDescriptor::Service {tenant_id: self.tenant_id.clone().try_into().unwrap(), service_id: self.id.clone().try_into().unwrap() }).await
  }
}

impl HasScope for Service {
  fn scope_descriptor(&self) -> ScopeDescriptor {
    ScopeDescriptor::Service {tenant_id: self.tenant_id.to_identifier(), service_id: self.id.to_identifier()}
  }
}

#[cfg(test)]
mod tests {
  use pretty_assertions::assert_eq;

  use crate::{
    admin::tenant::model::{NewTenant, Tenant},
    pagination::{PaginationFields, SortOrder::Ascending},
    util::test_transaction,
  };

  use super::*;

  #[actix_rt::test]
  async fn create_works() {
    let tx = test_transaction().await;

    Tenant::create(
      &tx,
      NewTenant {
        id: "test_tenant".to_identifier(),
        description: "this is a test tenant".into(),
      },
    )
    .await
    .unwrap();
    Service::create(
      &tx,
      &NewService {
        id: "test_service".into(),
        tenant_id: "test_tenant".into(),
      },
    )
    .await
    .unwrap();
  }

  #[actix_rt::test]
  async fn create_errors_on_bad_id() {
    let tx = test_transaction().await;

    Tenant::create(
      &tx,
      NewTenant {
        id: "test_tenant".to_identifier(),
        description: "this is a test tenant".into(),
      },
    )
    .await
    .unwrap();
    let e = Service::create(
      &tx,
      &NewService {
        id: "test service".into(),
        tenant_id: "test_tenant".into(),
      },
    )
    .await
    .unwrap_err();
    assert!(matches!(e, CreateServiceError::BadId));
  }

  #[actix_rt::test]
  async fn create_errors_on_missing_tenant() {
    let tx = test_transaction().await;
    let e = Service::create(
      &tx,
      &NewService {
        id: "test_service".into(),
        tenant_id: "test_tenant".into(),
      },
    )
    .await
    .unwrap_err();
    assert!(matches!(e, CreateServiceError::NoTenant));
  }

  #[actix_rt::test]
  async fn create_errors_on_dupe_id() {
    let tx = test_transaction().await;
    Tenant::create(
      &tx,
      NewTenant {
        id: "test_tenant".to_identifier(),
        description: "this is a test tenant".into(),
      },
    )
    .await
    .unwrap();
    Service::create(
      &tx,
      &NewService {
        id: "test_service".into(),
        tenant_id: "test_tenant".into(),
      },
    )
    .await
    .unwrap();
    let e = Service::create(
      &tx,
      &NewService {
        id: "test_service".into(),
        tenant_id: "test_tenant".into(),
      },
    )
    .await
    .unwrap_err();
    assert!(matches!(e, CreateServiceError::AlreadyExists));
  }

  #[actix_rt::test]
  async fn index_works() {
    let tx = test_transaction().await;
    Tenant::create(
      &tx,
      NewTenant {
        id: "test_tenant".to_identifier(),
        description: "this is a test tenant".into(),
      },
    )
    .await
    .unwrap();
    for i in 0..10 {
      Service::create(
        &tx,
        &NewService {
          tenant_id: "test_tenant".into(),
          id: format!("s{}", i),
        },
      )
      .await
      .unwrap();
    }

    let i_result = Service::index(
      &tx,
      "test_tenant",
      &PaginationParams::PaginationFields(PaginationFields {
        sort_by: "id".into(),
        sort_order: Ascending,
        page_size: 5,
      }),
    )
    .await
    .unwrap();

    assert_eq!(i_result.items().last().unwrap().id, "s4");
    assert_eq!(i_result.items().len(), 5);
    println!("first index");
    let res2 = Service::index(
      &tx,
      "test_tenant",
      &PaginationParams::Cursor(i_result.cursor().unwrap().to_owned()),
    )
    .await
    .unwrap();
    assert_eq!(res2.items().last().unwrap().id, "s9");
  }
}
