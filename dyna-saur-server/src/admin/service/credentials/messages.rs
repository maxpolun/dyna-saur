use std::ops::Deref;

use actix::{Handler, Message, ResponseFuture};
use crate::{
  actors::RealDbActor,
  pagination::{GenericPaginatedApiError, Paginated, PaginationParams},
};

use super::model::{CreateServiceCredentialError, NewCredential, ServiceCredential, DeleteCredentialParams};

pub struct CreateServiceCredential(pub NewCredential);

impl Message for CreateServiceCredential {
  type Result = Result<ServiceCredential, CreateServiceCredentialError>;
}

impl Handler<CreateServiceCredential> for RealDbActor {
  type Result = ResponseFuture<<CreateServiceCredential as Message>::Result>;
  fn handle(&mut self, msg: CreateServiceCredential, _ctx: &mut Self::Context) -> Self::Result {
    let pool = self.pool.clone();
    Box::pin(async move {
      let client = pool.get().await.unwrap();
      ServiceCredential::create(client.deref(), &msg.0).await
    })
  }
}

pub struct IndexServiceCredentials {
  pub tenant_id: String,
  pub service_id: String,
  pub pagination: PaginationParams,
}

impl Message for IndexServiceCredentials {
  type Result = Result<Paginated<Vec<ServiceCredential>>, GenericPaginatedApiError>;
}

impl Handler<IndexServiceCredentials> for RealDbActor {
  type Result = ResponseFuture<<IndexServiceCredentials as Message>::Result>;

  fn handle(&mut self, msg: IndexServiceCredentials, _ctx: &mut Self::Context) -> Self::Result {
    let pool = self.pool.clone();
    Box::pin(async move {
      let client = pool.get().await.unwrap();
      ServiceCredential::index(client.deref(), &msg.service_id, &msg.tenant_id, &msg.pagination).await
    })
  }
}


pub struct DeleteCredential(pub DeleteCredentialParams);

impl Message for DeleteCredential {
  type Result = Result<(), tokio_postgres::Error>;
}

impl Handler<DeleteCredential> for RealDbActor {
  type Result = ResponseFuture<<DeleteCredential as Message>::Result>;

  fn handle(&mut self, msg: DeleteCredential, _ctx: &mut Self::Context) -> Self::Result {
    let pool = self.pool.clone();
    Box::pin(async move {
      let client = pool.get().await.unwrap();
      ServiceCredential::delete_by_id(client.deref(), msg.0).await
    })
  }
}
