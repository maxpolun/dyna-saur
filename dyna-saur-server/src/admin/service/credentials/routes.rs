use actix_web::{
  web::{Data, Json, Path},
  HttpResponse, Result,
};
use dyna_saur_core::id::{UniqueId, Identifier};
use serde::{Deserialize, Serialize};
use serde_qs::actix::QsQuery;

use crate::{
  pagination::{PaginationFields, PaginationParams, SortOrder::Ascending},
  problem_json::ProblemJsonExt,
  DbAddr,
};

use super::{
  messages::{CreateServiceCredential, IndexServiceCredentials, DeleteCredential},
  model::{NewCredential, DeleteCredentialParams},
};

#[derive(Debug, Serialize, Deserialize)]
pub struct ServiceCredentialPath {
  pub tenant_id: String,
  pub service_id: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ServiceDetailCredentialPath {
  pub tenant_id: Identifier,
  pub service_id: Identifier,
  pub credential_id: UniqueId
}

#[derive(Debug, Serialize, Deserialize)]
pub struct CreateServiceCredentialRequest {
  pub comment: Option<String>,
}

pub async fn create_service_credential(
  db: Data<DbAddr>,
  path: Path<ServiceCredentialPath>,
  body: Json<CreateServiceCredentialRequest>,
) -> Result<HttpResponse> {
  let ServiceCredentialPath { service_id, tenant_id } = path.into_inner();
  let comment = body.0.comment;
  let cred = db
    .send(CreateServiceCredential(NewCredential {
      service_id,
      tenant_id,
      comment,
      algorithm: super::model::ServiceCredentialAlgorithm::HS256,
    }))
    .await
    .err_problem_json()?
    .err_problem_json()?;
  Ok(HttpResponse::Ok().json(cred))
}

#[derive(Debug, Serialize, Deserialize)]
pub struct IndexParams {
  pagination: Option<PaginationParams>,
}

pub async fn index_service_credentials(
  db: Data<DbAddr>,
  path: Path<ServiceCredentialPath>,
  params: QsQuery<IndexParams>,
) -> Result<HttpResponse> {
  let ServiceCredentialPath { tenant_id, service_id } = path.into_inner();
  let pagination = params
    .into_inner()
    .pagination
    .unwrap_or_else(|| PaginationParams::PaginationFields(PaginationFields {
      sort_by: "id".into(),
      sort_order: Ascending,
      page_size: 25,
    }));
  let creds = db
    .send(IndexServiceCredentials {
      tenant_id,
      service_id,
      pagination,
    })
    .await
    .err_problem_json()?
    .err_problem_json()?;
  Ok(HttpResponse::Ok().json(creds))
}

pub async fn delete_service_credential(
  db: Data<DbAddr>,
  path: Path<ServiceDetailCredentialPath>
) -> Result<HttpResponse> {
  let ServiceDetailCredentialPath { service_id, tenant_id, credential_id } = path.into_inner();
  db
    .send(DeleteCredential(DeleteCredentialParams {
      service_id, tenant_id, credential_id
    }))
    .await
    .err_problem_json()?
    .err_problem_json()?;
  Ok(HttpResponse::Ok().finish())
}

#[cfg(test)]
mod tests {
  use actix_web::{web::Path, http::StatusCode};
  use chrono::Utc;
  use dyna_saur_core::id::ToIdentifierExt;
  use pretty_assertions::assert_eq;

  use crate::{actors::RealDbActor, admin::service::credentials::{
      messages::{CreateServiceCredential, IndexServiceCredentials},
      model::{ServiceCredential, ServiceCredentialAlgorithm},
    }, pagination::{Cursor, Paginated}, util::{ident::UniqueId, parse_http_response}};

  use super::*;

  #[actix_rt::test]
  async fn create_can_create_cred() {
    let addr = actix_mock_helper::simple_mock_actor::<RealDbActor, CreateServiceCredential, _>(|msg| {
      Ok(ServiceCredential {
        id: UniqueId::new(),
        tenant_id: msg.0.tenant_id.to_identifier(),
        created_at: Utc::now(),
        service_id: msg.0.service_id.to_identifier(),
        comment: msg.0.comment.clone(),
        algorithm: ServiceCredentialAlgorithm::HS256,
        secret: ServiceCredentialAlgorithm::HS256.create_random_secret(),
      })
    });

    let response = create_service_credential(
      Data::new(addr),
      Path::from(ServiceCredentialPath {
        tenant_id: "t".into(),
        service_id: "s".into(),
      }),
      Json(CreateServiceCredentialRequest { comment: None }),
    )
    .await
    .unwrap();

    assert_eq!(response.status(), StatusCode::OK);
    assert_eq!(parse_http_response::<ServiceCredential>(response).service_id, "s".try_into().unwrap())
  }

  #[actix_rt::test]
  async fn index_gets_creds() {
    let addr = actix_mock_helper::simple_mock_actor::<RealDbActor, IndexServiceCredentials, _>(|msg| {
      let creds = (0..20)
        .map(|_i| ServiceCredential {
          id: UniqueId::new(),
          tenant_id: msg.tenant_id.to_identifier(),
          created_at: Utc::now(),
          service_id: msg.service_id.to_identifier(),
          comment: None,
          algorithm: ServiceCredentialAlgorithm::HS256,
          secret: ServiceCredentialAlgorithm::HS256.create_random_secret(),
        })
        .collect::<Vec<_>>();
      let cursor = creds.last().map(|c| Cursor {
        last_id: c.id.to_string(),
        fields: msg.pagination.clone().query().unwrap().fields,
      });
      Ok(Paginated::new(cursor, creds))
    });

    let response = index_service_credentials(
      Data::new(addr),
      Path::from(ServiceCredentialPath {
        tenant_id: "t".into(),
        service_id: "s".into(),
      }),
      QsQuery(IndexParams { pagination: None }),
    )
    .await
    .unwrap();

    assert_eq!(response.status(), StatusCode::OK);
    assert_eq!(
      parse_http_response::<Paginated<Vec<ServiceCredential>>>(response)
        .items()
        .len(),
      20
    );
  }
}
