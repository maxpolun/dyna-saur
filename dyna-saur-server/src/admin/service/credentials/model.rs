use crate::{pagination::{GenericPaginatedApiError, Paginated, PaginatedQuery, PaginationParams}, problem_json::{internal_server_error, not_found, ToProblemJson}, query::SqlCols, sql, util::{Timestamp, db_error_constraint, ident::UniqueId}};
use base64::STANDARD_NO_PAD;
use derive_pgmap::DbMap;
use dyna_db::DbMap;
use dyna_saur_core::id::Identifier;
use postgres_types::{FromSql, ToSql};
use serde::{Deserialize, Serialize};
use thiserror::Error;
use tokio_postgres::{GenericClient};

#[derive(Debug, ToSql, FromSql, Serialize, Deserialize, Clone)]
#[postgres(name = "service_credential_algorithm")]
pub enum ServiceCredentialAlgorithm {
  HS256, // Only one for now -- HMAC SHA256
}

impl ServiceCredentialAlgorithm {
  pub fn create_random_secret(&self) -> String {
    match self {
      ServiceCredentialAlgorithm::HS256 => base64::encode_config(rand::random::<[u8; 16]>(), STANDARD_NO_PAD),
    }
  }
}

#[derive(Debug, Serialize, Deserialize, Clone, DbMap)]
pub struct ServiceCredential {
  pub id: UniqueId,
  pub service_id: Identifier,
  pub tenant_id: Identifier,
  pub created_at: Timestamp,
  pub comment: Option<String>,
  pub algorithm: ServiceCredentialAlgorithm,
  // the shared secret for HMAC algorithms, pubkey for asymetric signing if we ever support that
  pub secret: String,
}

#[derive(Debug)]
pub struct NewCredential {
  pub service_id: String,
  pub tenant_id: String,
  pub comment: Option<String>,
  pub algorithm: ServiceCredentialAlgorithm,
}

#[derive(Debug, Error)]
pub enum CreateServiceCredentialError {
  #[error("service_id or tenant_id does not refer to a valid parent service for credential")]
  MissingParent,
  #[error("Database error: {0}")]
  DbError(#[from] tokio_postgres::Error),
}

impl ToProblemJson for CreateServiceCredentialError {
  fn to_problem_json(self) -> crate::problem_json::ProblemJson {
    match self {
      CreateServiceCredentialError::MissingParent => not_found(),
      CreateServiceCredentialError::DbError(_) => internal_server_error(),
    }
  }
}

const TABLE: &str = "service_credentials";
const SORTABLE_COLUMNS: [&str; 1] = ["id"];
const ALL_COLUMNS: SqlCols<7> = SqlCols([
  "id",
  "service_id",
  "tenant_id",
  "created_at",
  "comment",
  "algorithm",
  "secret",
]);
const CREATE_COLUMNS: SqlCols<6> = SqlCols(["id", "tenant_id", "service_id", "comment", "algorithm", "secret"]);

pub struct DeleteCredentialParams {
  pub tenant_id: Identifier,
  pub service_id: Identifier,
  pub credential_id: UniqueId
}

impl ServiceCredential {
  pub async fn create(
    client: &impl GenericClient,
    new_cred: &NewCredential,
  ) -> Result<ServiceCredential, CreateServiceCredentialError> {
    let id = UniqueId::new();

    let secret = new_cred.algorithm.create_random_secret();
    let q = sql!(
      "INSERT INTO {:table} ({:create_cols}) VALUES ({id}, {tenant_id}, {service_id}, {comment}, {algorithm}, {secret}) RETURNING {:all_cols}",
      :table = TABLE,
      :create_cols = CREATE_COLUMNS,
      :all_cols = ALL_COLUMNS,
      id, tenant_id = new_cred.tenant_id, service_id = new_cred.service_id, comment = new_cred.comment, algorithm = new_cred.algorithm, secret
    );
    let result = q.query_one(client).await;
    if let Some("service_credentials_tenant_id_service_id_fkey") = result.as_ref().err().and_then(db_error_constraint) {
       return Err(CreateServiceCredentialError::MissingParent)
    }
    Ok(ServiceCredential::db_map(result?))
  }

  pub async fn index(
    client: &impl GenericClient,
    service_id: &str,
    tenant_id: &str,
    pagination: &PaginationParams,
  ) -> Result<Paginated<Vec<ServiceCredential>>, GenericPaginatedApiError> {
    let PaginatedQuery { fields, last_id } = pagination.clone().query()?;

    let (dir, cmp) = fields.sort_order();
    let col = fields.sort_col(&SORTABLE_COLUMNS)?;
    let page_size = fields.page_size()?;
    sql!(
      "SELECT id FROM services WHERE id = {service_id} AND tenant_id = {tenant_id} LIMIT 1",
      service_id,
      tenant_id
    ).query_opt(client)
      .await?.ok_or_else(|| GenericPaginatedApiError::MissingParent("service_id, tenant_id".into()))?;

    let items = sql!(
      "SELECT
        {:cols}
      FROM
        service_credentials
      WHERE
        tenant_id = {t}
      AND
        service_id = {s}
      {?last_id}
        AND id {:cmp} {last_id}
      {/?last_id}
      ORDER BY {sort} {:dir} LIMIT {count}",
      :cols = ALL_COLUMNS, t = tenant_id, s = service_id, sort = col, :dir = dir, count = page_size, ?last_id, :cmp
    )
      .query(client)
      .await?
      .into_iter()
      .map(ServiceCredential::db_map)
      .collect::<Vec<ServiceCredential>>();
    let cursor = items.last().map(|t| fields.into_cursor(&t.id.to_string()));
    Ok(Paginated::new(cursor, items))
  }

  pub async fn get_by_secret(client: &impl GenericClient, secret: &str) -> Result<ServiceCredential, tokio_postgres::Error> {
    sql!("SELECT {:cols} FROM {:table} WHERE secret={secret}",
    :cols = ALL_COLUMNS, :table = TABLE, secret
  )
    .query_one(client).await
    .map(ServiceCredential::db_map)
  }

  pub async fn delete_by_id(client: &impl GenericClient, params: DeleteCredentialParams) -> Result<(), tokio_postgres::Error> {
    sql!("DELETE FROM {:table} WHERE tenant_id={tid} AND service_id={sid} AND id={cid}",
      :table = TABLE, tid = params.tenant_id, sid = params.service_id, cid = params.credential_id
    ).execute(client).await?;
    Ok(())
  }
}

#[cfg(test)]
mod tests {
use dyna_saur_core::id::ToIdentifierExt;

use crate::{
    admin::{
      service::model::{NewService, Service},
      tenant::model::{NewTenant, Tenant},
    },
    util::test_transaction,
  };

  use super::*;

  #[actix_rt::test]
  async fn create_works() {
    let tx = test_transaction().await;
    let t = Tenant::create(
      &tx,
      NewTenant {
        id: "test_tenant".to_identifier(),
        description: "test tenant".into(),
      },
    )
    .await
    .unwrap();
    let s = Service::create(
      &tx,
      &NewService {
        tenant_id: t.id.to_string(),
        id: "test_service".into(),
      },
    )
    .await
    .unwrap();

    let c = ServiceCredential::create(
      &tx,
      &NewCredential {
        service_id: s.id.clone(),
        tenant_id: t.id.to_string(),
        comment: None,
        algorithm: ServiceCredentialAlgorithm::HS256,
      },
    )
    .await
    .unwrap();

    pretty_assertions::assert_eq!(c.service_id, s.id.to_identifier());
  }

  #[actix_rt::test]
  async fn delete_works() {
    let tx = test_transaction().await;
    let t = Tenant::create(
      &tx,
      NewTenant {
        id: "test_tenant".to_identifier(),
        description: "test tenant".into(),
      },
    )
    .await
    .unwrap();
    let s = Service::create(
      &tx,
      &NewService {
        tenant_id: t.id.to_string(),
        id: "test_service".into(),
      },
    )
    .await
    .unwrap();

    let c = ServiceCredential::create(
      &tx,
      &NewCredential {
        service_id: s.id.clone(),
        tenant_id: t.id.to_string(),
        comment: None,
        algorithm: ServiceCredentialAlgorithm::HS256,
      },
    )
    .await
    .unwrap();

    ServiceCredential::delete_by_id(&tx, DeleteCredentialParams {
      tenant_id: t.id.clone(),
      service_id: Identifier::new(s.id.clone()).unwrap(),
      credential_id: c.id
    }).await.unwrap();

    assert!(tx.query_opt(
      "SELECT * FROM service_credentials WHERE id = $1", &[&c.id]
    ).await.unwrap().is_none())
  }
}
