use std::ops::Deref;

use actix::{Handler, Message, ResponseFuture};

use crate::{
  actors::RealDbActor,
  pagination::{GenericPaginatedApiError, Paginated, PaginationParams},
};

use super::model::{CreateServiceError, NewService, Service};

pub struct CreateService(pub NewService);

impl Message for CreateService {
  type Result = Result<Service, CreateServiceError>;
}

impl Handler<CreateService> for RealDbActor {
  type Result = ResponseFuture<<CreateService as Message>::Result>;
  fn handle(&mut self, msg: CreateService, _ctx: &mut Self::Context) -> Self::Result {
    let pool = self.pool.clone();
    Box::pin(async move {
      let client = pool.get().await.unwrap();
      Service::create(client.deref(), &msg.0).await
    })
  }
}

pub struct IndexServices {
  pub tenant_id: String,
  pub pagination: PaginationParams,
}

impl Message for IndexServices {
  type Result = Result<Paginated<Vec<Service>>, GenericPaginatedApiError>;
}

impl Handler<IndexServices> for RealDbActor {
  type Result = ResponseFuture<<IndexServices as Message>::Result>;

  fn handle(&mut self, msg: IndexServices, _ctx: &mut Self::Context) -> Self::Result {
    let pool = self.pool.clone();
    Box::pin(async move {
      let client = pool.get().await.unwrap();
      Service::index(client.deref(), &msg.tenant_id, &msg.pagination).await
    })
  }
}
