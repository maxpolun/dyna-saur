use std::str::FromStr;

use actix_web::{
  web::{Data, Json, Path},
  HttpResponse, Result,
};
use serde::{Deserialize, Serialize};
use serde_qs::actix::QsQuery;
use subject::Subject;

use crate::{DbAddr, admin::users::role_assignments::messages::Permission, pagination::{PaginationFields, PaginationParams, SortOrder::Ascending}, problem_json::ProblemJsonExt, shared::{scope::model::ScopeDescriptor}, util::{Identifier, permissions::permission_at_self_or_any_parent}};

use super::{
  messages::{CreateService, IndexServices},
  model::NewService,
};

#[derive(Debug, Serialize, Deserialize)]
pub struct NewServiceRequest {
  id: String,
}

pub async fn create_service(
  db: Data<DbAddr>,
  tenant_id: Path<String>,
  body: Json<NewServiceRequest>,
  subject: Subject
) -> Result<HttpResponse> {
  permission_at_self_or_any_parent(&*db, ScopeDescriptor::Tenant {
    tenant_id: Identifier::from_str(&tenant_id).unwrap()
  }, Permission::Write, &subject).await?;
  let s = db
    .send(CreateService(NewService {
      tenant_id: tenant_id.into_inner(),
      id: body.into_inner().id,
    }))
    .await
    .err_problem_json()?
    .err_problem_json()?;
  Ok(HttpResponse::Ok().json(s))
}

#[derive(Debug, Serialize, Deserialize)]
pub struct IndexParams {
  pagination: Option<PaginationParams>,
}

pub async fn index_services(
  db: Data<DbAddr>,
  tenant_id: Path<String>,
  params: QsQuery<IndexParams>,
  subject: Subject
) -> Result<HttpResponse> {
  permission_at_self_or_any_parent(&*db, ScopeDescriptor::Tenant {
    tenant_id: Identifier::from_str(&tenant_id).unwrap()
  }, Permission::Read, &subject).await?;
  let pagination = params
    .into_inner()
    .pagination
    .unwrap_or_else(|| PaginationParams::PaginationFields(PaginationFields {
      sort_by: "id".into(),
      sort_order: Ascending,
      page_size: 10,
    }));
  let services = db
    .send(IndexServices {
      tenant_id: tenant_id.into_inner(),
      pagination,
    })
    .await
    .err_problem_json()?
    .err_problem_json()?;
  Ok(HttpResponse::Ok().json(services))
}

#[cfg(test)]
mod tests {
  use actix_mock_helper::MockActorSequence;
use actix_web::web::Json;
  use chrono::Utc;

  use crate::{admin::{service::{
      messages::{CreateService, IndexServices},
      model::Service,
    }}, pagination::{Cursor, Paginated, PaginationFields, SortOrder::Ascending}, util::{ident::UniqueId, parse_http_response, permissions::PermissionAtSelfOrAnyParent}};

  use super::*;

  #[actix_rt::test]
  async fn create_works() {
    let addr = MockActorSequence::new()
    .msg(|_msg: &PermissionAtSelfOrAnyParent| true )
    .msg(|msg: &CreateService| {
      Ok(Service {
        id: msg.0.id.clone(),
        tenant_id: msg.0.tenant_id.clone(),
        created_at: Utc::now(),
      })
    })
        .build();

    let res = create_service(
      Data::new(addr),
      Path::from("test_tenant".to_owned()),
      Json(NewServiceRequest {
        id: "test_service".into(),
      }),
      Subject::User{ user_id: UniqueId::new() }
    )
    .await
    .unwrap();

    assert_eq!(parse_http_response::<Service>(res).id, "test_service");
  }

  #[actix_rt::test]
  async fn index_works() {
    let addr = MockActorSequence::new()
      .msg(|_msg: &PermissionAtSelfOrAnyParent| true )
      .msg(|msg: &IndexServices| {
        let tenant_id = msg.tenant_id.clone();
        let fields = match msg.pagination.clone() {
          PaginationParams::Cursor(_) => panic!("wrong match arm"),
          PaginationParams::PaginationFields(f) => f,
        };
        let items: Vec<Service> = (0..10)
          .map(|i| Service {
            id: format!("s{}", i),
            tenant_id: tenant_id.clone(),
            created_at: Utc::now(),
          })
          .collect();
        Ok(Paginated::new(
          items.last().map(|t| Cursor {
            last_id: t.id.clone(),
            fields,
          }),
          items,
        ))
    }).build();

    let res = index_services(
      Data::new(addr),
      Path::from("test_tenant".to_owned()),
      QsQuery(IndexParams {
        pagination: Some(PaginationParams::PaginationFields(PaginationFields {
          sort_by: "id".into(),
          sort_order: Ascending,
          page_size: 10,
        })),
      }),
      Subject::User{  user_id: UniqueId::new() }
    )
    .await
    .unwrap();

    assert_eq!(parse_http_response::<Paginated<Vec<Service>>>(res).items().len(), 10);
  }
}
