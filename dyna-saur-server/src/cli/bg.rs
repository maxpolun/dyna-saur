use std::{time::Duration, ops::Deref};

use dynalib::{admin::users::sessions::model::UserSession, util::db::make_db_pool};

#[actix_rt::main]
async fn main() {
  dotenv::dotenv().ok();
  let interval_seconds = std::env::var("BG_WORKER_INTERVAL_SECS").unwrap_or_else(|_err| "60".into()).parse::<u64>().expect("Invalid BG_WORKER_INTERVAL_SECS");
  let mut interval = tokio::time::interval(Duration::from_millis(interval_seconds * 1000));
  let pool = make_db_pool().await;

  loop {
    interval.tick().await;
    let client = match pool.get().await {
        Ok(client) => client,
        Err(e) => {
          println!("could not connect to db from worker: {}", e);
          continue
        },
    };

    // do all of the background tasks
    match UserSession::delete_expired(client.deref()).await {
        Ok(deleted_count) => println!("Successfully deleted {} old user sessions", deleted_count),
        Err(err) => println!("Unable to delete old user sessions: {}", err),
    }
  }
}
