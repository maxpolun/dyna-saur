use actix_web::cookie::Key;
use dynalib::{admin::{users::{model::User, role_assignments::model::{NewRoleAssignment, RoleAssignment}}, service::{model::{Service, NewService}, credentials::model::{ServiceCredential, NewCredential, ServiceCredentialAlgorithm}}}, shared::scope::model::{Scope, ScopeDescriptor}};
use rand::{Rng, distributions::Alphanumeric, thread_rng};
use tokio_postgres::{Client, NoTls};


#[actix_rt::main]
async fn main() {
  dotenv::dotenv().ok();

  let args_owned = std::env::args().collect::<Vec<_>>();
  let args = args_owned.iter().map(|s| &**s).collect::<Vec<_>>();
  match args[1..] {
    ["create-admin", email] => create_admin_user(email).await,
    ["gen-key"] => println!("Key: {}", base64::encode(Key::generate().master())),
    ["service", "create", tenant_id, service_name] => create_service(tenant_id, service_name).await,
    ["service", "gen-key", tenant_id, service_name] => create_service_key(tenant_id, service_name).await,
    _ => print_usage(),
  }
}

async fn create_service_key(tenant_id: &str, service_name: &str) {
  let client = connect().await;
  let cred = ServiceCredential::create(&client, &NewCredential{
    tenant_id: tenant_id.into(),
    service_id: service_name.into(),
    comment: None,
    algorithm: ServiceCredentialAlgorithm::HS256
  }).await.unwrap();
  println!("Service credential {} created for service {} in tenant {}", cred.id, service_name, tenant_id);
  println!("Secret for service credential is `{}`", cred.secret);
}

async fn create_service(tenant_id: &str, service_name: &str) {
  let client = connect().await;
  Service::create(&client, &NewService{ tenant_id: tenant_id.into(), id: service_name.into() }).await.unwrap();
  println!("Service {} created in tenant {}", service_name, tenant_id);
}

fn print_usage() {
  println!(
    "
    admin: COMMAND [optional args]

    COMMANDS:

    create-admin email-address: create the admin user with the given email address
    gen-key: generate a signing key used for authentication. Store it in the AUTH_COOKIE_KEY environment variable
    service create tenant_id service_name: create a service in the given tenant
    service gen-key tenant_id service_name: generate a key for the service in the given tenant
    "
  )
}
async fn create_admin_user(email: &str) {
  let mut client = connect().await;
  let tx = client.transaction().await.unwrap();
  let user = User::create(&tx, email, "admin").await.unwrap();
  let random_password = thread_rng()
    .sample_iter(&Alphanumeric)
    .take(30)
    .map(char::from)
    .collect::<String>();
  user.create_local_credential(&tx, &random_password, &random_password).await.unwrap();
  RoleAssignment::create(&tx, NewRoleAssignment {
    user_id: user.id,
    scope_id: Scope::get(&tx, ScopeDescriptor::Global).await.unwrap().id,
    role_id: RoleAssignment::WRITER_ROLE.to_owned(),
  }).await.unwrap();
  tx.commit().await.unwrap();
  println!("Admin user with name 'admin' and email '{}' created with password {}. SAVE THIS PASSWORD.", email, random_password)
}

async fn connect() -> Client {
  let db_url = std::env::var("DATABASE_URL").expect("DATABASE_URL");

  let (client, connection) = tokio_postgres::connect(&db_url, NoTls).await.unwrap();
  tokio::spawn(async move {
    if let Err(e) = connection.await {
      eprintln!("connection error: {}", e);
    }
  });
  client
}
