use std::{
  cmp::Ordering,
  fs::create_dir,
  path::Path,
  time::{SystemTime, UNIX_EPOCH},
};

use include_dir::{include_dir, Dir};
use tokio_postgres::{Client, NoTls};
// statically embed the migrations directory
const MIGRATIONS: Dir = include_dir!("$CARGO_MANIFEST_DIR/migrations");

#[actix_rt::main]
async fn main() {
  dotenv::dotenv().ok();

  let args_owned = std::env::args().collect::<Vec<_>>();
  let args = args_owned.iter().map(|s| &**s).collect::<Vec<_>>();
  match args[1..] {
    ["up"] => run_migrations().await,
    ["down"] => undo_last_migration().await,
    ["redo"] => redo_last_migration().await,
    ["reset"] => undo_all_migrations().await,
    ["list"] => print_migrations().await,
    ["create", migration_name] => create_new_migration(migration_name),
    _ => print_usage(),
  }
}

fn print_usage() {
  println!(
    "
  migrate: COMMAND [optional args]

  COMMANDS:

  up: run all pending migrations
  down: undo the last migration
  redo: undo and redo the last migration (good during development)
  reset: undo all migrations
  list: list all migrations and print if they have been executed or not
  create migration-name: creates a new migration with the given name
  "
  )
}
struct Migration {
  id: String,
  name: String,
  up: String,
  down: String,
  check: String,
}

impl Migration {
  fn has_been_executed(&self, last_executed: Option<&str>) -> bool {
    if let Some(version) = last_executed {
      matches!(version.cmp(&self.id), Ordering::Greater | Ordering::Equal)
    } else {
      false
    }
  }

  async fn run(&self, client: &mut Client) {
    let txn = client.transaction().await.unwrap();
    txn.batch_execute(&self.up).await.map_err(|e| {
      println!("Error executing migration {}", self.name);
      println!("Error {}", e);
      e
    }).unwrap();
    txn.batch_execute(&self.check).await.map_err(|e| {
      println!("Error checking migration {}", self.name);
      println!("Error {}", e);
      e
    }).unwrap();
    // insert into migrations last, since adding the table is a migration :-)
    txn
      .execute(
        "INSERT INTO dyna_migrations (id, name) VALUES ($1, $2)",
        &[&self.id, &self.name],
      )
      .await.map_err(|e| {
        println!("Error completing migration {}", self.name);
        println!("Error {}", e);
        e
      })
      .unwrap();
    txn.commit().await.unwrap();
    println!("{}\t{}\tEXECUTED", self.id, self.name);
  }

  async fn undo(&self, client: &mut Client) {
    let txn = client.transaction().await.unwrap();
    // delete from migrations first, since adding the table is a migration :-)
    txn
      .execute("DELETE FROM dyna_migrations WHERE id = $1", &[&self.id])
      .await
      .unwrap();
    txn.batch_execute(&self.down).await.map_err(|e| {
      println!("Error undoing migration {}", self.name);
      println!("Error {}", e);
      e
    }).unwrap();
    txn.commit().await.unwrap();
    println!("{}\t{}\tUNDO", self.id, self.name);
  }
}

fn get_migrations() -> Vec<Migration> {
  let mut m = MIGRATIONS
    .dirs()
    .map(|dir| {
      let dirname = dir.path().file_name().unwrap().to_string_lossy();
      let [id, name] = {
        let mut iter = dirname.split("__");
        [iter.next().unwrap().to_owned(), iter.next().unwrap().to_owned()]
      };

      let up = dir
        .get_file(dir.path().join("up.sql"))
        .unwrap()
        .contents_utf8()
        .unwrap()
        .to_owned();
      let down = dir
        .get_file(dir.path().join("down.sql"))
        .unwrap()
        .contents_utf8()
        .unwrap()
        .to_owned();
      let check = dir
        .get_file(dir.path().join("check.sql"))
        .unwrap()
        .contents_utf8()
        .unwrap()
        .to_owned();

      Migration {
        id,
        name,
        up,
        down,
        check,
      }
    })
    .collect::<Vec<_>>();
  m.sort_unstable_by(|a, b| a.id.cmp(&b.id));
  m
}

async fn print_migrations() {
  let migrations = get_migrations();

  let client = connect().await;
  let current = current_migration(&client).await;
  for m in migrations {
    println!(
      "{}\t{}:\t {}",
      m.id,
      m.name,
      current
        .as_ref()
        .map(|v| match v.cmp(&m.id) {
          std::cmp::Ordering::Greater | std::cmp::Ordering::Equal => "",
          std::cmp::Ordering::Less => "PENDING",
        })
        .unwrap_or("PENDING")
    )
  }
}

async fn connect() -> Client {
  let db_url = std::env::var("DATABASE_URL").expect("DATABASE_URL");

  let (client, connection) = tokio_postgres::connect(&db_url, NoTls).await.unwrap();
  tokio::spawn(async move {
    if let Err(e) = connection.await {
      eprintln!("connection error: {}", e);
    }
  });
  client
}

async fn current_migration(client: &Client) -> Option<String> {
  let row = client
    .query_one("SELECT id FROM dyna_migrations ORDER BY id DESC LIMIT 1", &[])
    .await
    .ok();
  row.map(|r| r.get(0))
}

fn create_new_migration(migration_name: &str) {
  let mut migrations_dir = std::env::current_dir().unwrap();
  migrations_dir.push("migrations");
  if !migrations_dir.exists() {
    panic!("migrations/ dir does not exist. Only call this executable from the root of the crate")
  }

  let migration_version = SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs();
  let dirname = format!("{}__{}", migration_version, migration_name);
  let migration_path = migrations_dir.join(Path::new(&dirname));

  create_dir(&migration_path).unwrap();
  println!("created migration dir {}", migration_path.to_string_lossy());

  std::fs::File::create(migration_path.join(Path::new("up.sql"))).unwrap();
  std::fs::File::create(migration_path.join(Path::new("down.sql"))).unwrap();
  std::fs::File::create(migration_path.join(Path::new("check.sql"))).unwrap();
}

async fn undo_all_migrations() {
  let mut migrations = get_migrations();

  let mut client = connect().await;
  let current = current_migration(&client).await;
  let version = if let Some(v) = current { v } else { return };
  let mut count: i32 = 0;

  migrations.reverse(); // start from the back instead of the front
  for m in migrations {
    let undo = m.has_been_executed(Some(&version));
    if undo {
      m.undo(&mut client).await;
      count += 1;
    }
  }
  println!("undid {} migrations", count);
}

async fn redo_last_migration() {
  let migrations = get_migrations();

  let mut client = connect().await;
  let current = current_migration(&client).await;
  let version = if let Some(v) = current { v } else { return };

  if let Some(m) = migrations.iter().find(|&m| m.id == version) {
    m.undo(&mut client).await;
    m.run(&mut client).await;
  }
}

async fn undo_last_migration() {
  let migrations = get_migrations();

  let mut client = connect().await;
  let current = current_migration(&client).await;
  let version = if let Some(v) = current { v } else { return };

  if let Some(m) = migrations.iter().find(|&m| m.id == version) {
    m.undo(&mut client).await;
  }
}

async fn run_migrations() {
  let migrations = get_migrations();

  let mut client = connect().await;
  let current = current_migration(&client).await;
  let current = current.as_deref();
  let mut count: i32 = 0;

  for m in migrations {
    let run = !m.has_been_executed(current);

    if run {
      m.run(&mut client).await;
      count += 1;
    }
  }
  println!("executed {} migrations", count);
}
