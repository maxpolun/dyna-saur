use actix::{Addr};
#[cfg(test)]
use actix::{actors::mocker::Mocker};
pub use dyna_db::actor::{RealDbActor};

#[cfg(not(test))]
pub type DbActor = RealDbActor;
#[cfg(test)]
pub type DbActor = Mocker<RealDbActor>;

pub type DbAddr = Addr<DbActor>;
