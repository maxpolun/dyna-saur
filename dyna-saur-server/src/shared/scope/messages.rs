use std::ops::Deref;

use actix::{Handler, Message, ResponseFuture};
use crate::{actors::RealDbActor, pagination::{GenericPaginatedApiError, Paginated, PaginationParams}};

use super::model::{Scope, ScopeDescriptor};

pub struct GetAllScopes(pub PaginationParams);

impl Message for GetAllScopes {
  type Result = Result<Paginated<Vec<Scope>>, GenericPaginatedApiError>;
}

impl Handler<GetAllScopes> for RealDbActor {
  type Result = ResponseFuture<<GetAllScopes as Message>::Result>;
  fn handle(&mut self, msg: GetAllScopes, _ctx: &mut Self::Context) -> Self::Result {
    let pool = self.pool.clone();
    Box::pin(async move {
      let client = pool.get().await.unwrap();
      Scope::get_all(client.deref(), &msg.0).await
    })
  }
}

pub struct GetScope(pub ScopeDescriptor);

impl Message for GetScope {
  type Result = Result<Option<Scope>, tokio_postgres::Error>;
}

impl Handler<GetScope> for RealDbActor {
  type Result = ResponseFuture<<GetScope as Message>::Result>;
  fn handle(&mut self, msg: GetScope, _ctx: &mut Self::Context) -> Self::Result {
    let pool = self.pool.clone();
    Box::pin(async move {
      let client = pool.get().await.unwrap();
      Scope::get_opt(client.deref(), &msg.0).await
    })
  }
}
