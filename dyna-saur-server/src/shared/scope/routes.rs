use actix_web::{HttpResponse, Result, web::Data};
use serde_qs::actix::QsQuery;
use serde::{Serialize, Deserialize};
use crate::pagination::{self, PaginationFields, PaginationParams};
use crate::problem_json::ProblemJsonExt;

use crate::{DbAddr};

use super::messages::GetAllScopes;

#[derive(Debug, Serialize, Deserialize)]
pub struct IndexParams {
  pagination: Option<PaginationParams>,
}


pub async fn get_all_scopes(db: Data<DbAddr>, params: QsQuery<IndexParams>) -> Result<HttpResponse> {
  let pagination = params.into_inner().pagination.unwrap_or_else(|| PaginationParams::PaginationFields(PaginationFields {
        sort_by: "id".into(),
        sort_order: pagination::SortOrder::Ascending,
        page_size: 25,
    }));
  let scopes = db.send(GetAllScopes(pagination)).await.err_problem_json()?.err_problem_json()?;
  Ok(HttpResponse::Ok().json(scopes))
}
