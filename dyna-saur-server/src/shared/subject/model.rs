use std::{future::{Ready, ready}, fmt::Display};

use actix_web::{FromRequest, error::ErrorUnauthorized, HttpMessage};
use subject::Subject;
use tracing::debug;

use crate::admin::{service::credentials::model::ServiceCredential, users::sessions::model::UserSession};


#[derive(Debug, Clone)]
pub enum SubjectCredential {
  User(UserSession),
  Service(ServiceCredential)
}

impl SubjectCredential {
    pub fn to_subject(&self) -> Subject {
      match self {
        SubjectCredential::User(session) => Subject::User { user_id: session.id },
        SubjectCredential::Service(cred) => Subject::Service {
          tenant_id: cred.tenant_id.clone(), service_id: cred.service_id.clone()
        },
      }
    }
}

impl FromRequest for SubjectCredential {
    type Error = actix_web::Error;

    type Future = Ready<Result<Self, Self::Error>>;

    fn from_request(req: &actix_web::HttpRequest, _payload: &mut actix_web::dev::Payload) -> Self::Future {
      ready(
        req
          .extensions()
          .get::<SubjectCredential>()
          .cloned()
          .ok_or_else(|| {
            debug!("404 because no subject found on request");
            ErrorUnauthorized("Not Authorized")
          }))

    }
}

impl Display for SubjectCredential {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            SubjectCredential::User(session) => {
              f.write_str("user_session:")?;
              session.id.fmt(f)?;
              f.write_str(":user_id:")?;
              session.user_id.fmt(f)
            },
            SubjectCredential::Service(cred) => {
              f.write_str("service_credential:")?;
              cred.id.fmt(f)?;
              f.write_str(":tenant_id:")?;
              cred.tenant_id.fmt(f)?;
              f.write_str(":service_id:")?;
              cred.service_id.fmt(f)
            },
        }
    }
}
