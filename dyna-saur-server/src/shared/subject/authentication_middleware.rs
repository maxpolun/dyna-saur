use std::{cell::RefCell, future::Ready, pin::Pin, rc::Rc, str::FromStr, task::{Context, Poll}};
use actix_web::{Error, HttpMessage, cookie::{CookieJar, Key}, dev::{Service, ServiceRequest, ServiceResponse, Transform}, http::header::AUTHORIZATION};
use dyna_db::Pool;
use futures::Future;
use subject::Subject;
use tracing::{Instrument, Level, debug, event, span, trace, info};

use crate::{admin::{service::credentials::model::ServiceCredential, users::sessions::model::UserSession}, util::ident::UniqueId};

use super::model::SubjectCredential;

pub struct Authenticate {
  pub cookie_name: String,
  pub signing_key: Key,
  pub db: Pool
}

pub struct AuthenticateMiddleware<S> {
  service: Rc<RefCell<S>>,
  cookie_name: String,
  signing_key: Key,
  db: Pool
}

impl<S, B> Transform<S, ServiceRequest> for Authenticate
where
  S: Service<ServiceRequest, Response = ServiceResponse<B>, Error = Error> + 'static,
  S::Future: 'static + std::future::Future,
  B: 'static,
{
  type Response = ServiceResponse<B>;
  type Error = Error;
  type InitError = ();
  type Transform = AuthenticateMiddleware<S>;
  type Future = Ready<Result<Self::Transform, Self::InitError>>;

  fn new_transform(&self, service: S) -> Self::Future {
    std::future::ready(Ok(AuthenticateMiddleware {
      cookie_name: self.cookie_name.clone(),
      signing_key: self.signing_key.clone(),
      db: self.db.clone(),
      service: Rc::new(RefCell::new(service)),
    }))
  }
}

impl<S, B> Service<ServiceRequest> for AuthenticateMiddleware<S>
where
  B: 'static,
  S: Service<ServiceRequest, Response = ServiceResponse<B>, Error = Error> + 'static,
  S::Future: 'static + std::future::Future,
{
  type Response = ServiceResponse<B>;
  type Error = Error;
  #[allow(clippy::type_complexity)]
  type Future = Pin<Box<dyn Future<Output = Result<Self::Response, Self::Error>>>>;

  fn poll_ready(&self, ctx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
    self.service.borrow().poll_ready(ctx)
  }

  fn call(&self, req: ServiceRequest) -> Self::Future {
    let srv = self.service.clone();
    let cookie_name = self.cookie_name.clone();
    let signing_key = self.signing_key.clone();
    let db = self.db.clone();

    Box::pin(async move {
      let mut authn = false;
      let client = db.get().await.unwrap();
      // check for cookie first, then for header
      if let Some(cookie) = req.cookie(&cookie_name) {
        debug!("got cookie");
        let mut cj = CookieJar::new();
        cj.add_original(cookie);
        if let Some(cookie) = cj.signed(&signing_key).get(&cookie_name) {
          let session_id = UniqueId::from_str(cookie.value()).unwrap();
          if let Ok(session) = UserSession::get(&*client, session_id).await {
            trace!("got session from cookie");
            if session.valid() {
              event!(Level::INFO, user_id = %session.user_id, "user is logged in");
              req.extensions_mut().insert(Subject::User { user_id: session.user_id });
              req.extensions_mut().insert(SubjectCredential::User(session));
              authn = true
            } else {
              debug!("found session, but it's expired")
            }
          } else {
            debug!("no user session found with id {}", session_id)
          }
        } else {
          info!("found cookie, but it was not signed!")
        }
      } else if let Some(auth_header) = req.headers().get(AUTHORIZATION)  {
        debug!("got auth header");
        let (bearer, secret) = {
          let mut iter = auth_header.to_str().unwrap_or_default().split_whitespace();
          (iter.next().unwrap_or_default(), iter.next().unwrap_or_default())
        };
        if bearer.eq_ignore_ascii_case("Bearer") {
          let cred_result =  ServiceCredential::get_by_secret(&*client, secret).await;
          if let Ok(cred) = cred_result {
            event!(Level::INFO, tenant_id = %cred.tenant_id, service_id = %cred.service_id, credential_id = %cred.id, "service authenticated");
            req.extensions_mut().insert(Subject::Service{
              tenant_id: cred.tenant_id.clone(),
              service_id: cred.service_id.clone()
            });
            req.extensions_mut().insert(SubjectCredential::Service(cred));
            authn = true
          } else {
            debug!("error loading credential: {:?}", cred_result);
          }
        }
      }

      if !authn {
        debug!("Request not authenticated")
      }

      // split these two lines up so that the refcell is dropped before calling await
      let f = srv.borrow().call(req);
      f.await
    }.instrument(span!(Level::INFO, "trying to authenticate")))
  }
}
