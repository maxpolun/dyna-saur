
use actix_web::{App, HttpServer};
use dynalib::{config_api, StaticConfig, util::db::make_db_pool, setup_tracing};
use tracing_actix_web::TracingLogger;

#[actix_rt::main]
async fn main() {
  dotenv::dotenv().ok();
  setup_tracing();

  let pool = make_db_pool().await;
  let cookie_name = std::env::var("AUTH_COOKIE_NAME").unwrap_or_else(|_| "dyna-saur-session".into());
  let cookie_signing_key = std::env::var("AUTH_COOKIE_KEY").expect("AUTH_COOKIE_KEY is required");

  HttpServer::new(move || {
    App::new()
      .wrap(TracingLogger::default())
      .configure(|cfg| config_api(cfg, StaticConfig { db_pool: pool.clone(), cookie_name: cookie_name.clone(), cookie_signing_key: cookie_signing_key.clone() }))
  })
  .bind((
    "0.0.0.0",
    std::env::var("PORT")
      .and_then(|port_str| port_str.parse::<u16>().map_err(|_| std::env::VarError::NotPresent))
      .unwrap_or(3500),
  ))
  .unwrap()
  .run()
  .await
  .unwrap();
}
