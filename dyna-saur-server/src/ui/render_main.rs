use std::fs;

use actix_web::{HttpResponse, http::header};
use once_cell::sync::OnceCell;
use tracing::debug;

use crate::{problem_json::forbidden, shared::subject::model::SubjectCredential, util::server_environment};


static UI_FILE_MEMO: OnceCell<String> = OnceCell::new();

fn read_ui_file() -> String {
  String::from_utf8(fs::read("ui/dist/main.html").expect("Main UI Page does not exist. Did you run webpack?")).unwrap()
}

fn get_ui_file() -> String {
  if server_environment().is_production() {
    UI_FILE_MEMO.get_or_init(read_ui_file).clone()
  } else {
    read_ui_file()
  }
}

pub async fn get_ui(subject: Option<SubjectCredential>) -> actix_web::Result<HttpResponse> {
  match subject {
    Some(SubjectCredential::User(_session)) => {
      Ok(HttpResponse::Ok().content_type("text/html").body(get_ui_file()))
    },
    Some(SubjectCredential::Service(_cred)) => {
      debug!("service tried to access UI route");
      Err(forbidden().into())
    }
    None => {
      debug!("user not logged in, redirecting to /login");
      Ok(HttpResponse::TemporaryRedirect().append_header((header::LOCATION, "/login")).finish())
    }
  }

}
