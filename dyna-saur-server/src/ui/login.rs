use std::fs::{self};

use actix_web::{HttpResponse, Result, cookie::{Cookie, CookieJar, SameSite}, http::{StatusCode, header}, web::{Data, Form}};
use once_cell::sync::OnceCell;
use serde::{Deserialize, Serialize};
use tracing::{debug, info};

use crate::{CookieConfig, DbAddr, problem_json::{ProblemJson, ProblemJsonExt}, shared::subject::model::SubjectCredential, util::server_environment, admin::users::sessions::messages::LoginUserLocalCredential};

static LOGIN_FILE_MEMO: OnceCell<String> = OnceCell::new();

fn read_login_file() -> String {
  String::from_utf8(fs::read("ui/dist/login.html").expect("Login Page does not exist. Did you run webpack?")).unwrap()
}

fn get_login_file() -> String {
  if server_environment().is_production() {
    LOGIN_FILE_MEMO.get_or_init(read_login_file).clone()
  } else {
    read_login_file()
  }
}

pub async fn get_login(subject: Option<SubjectCredential>) -> Result<HttpResponse> {
  if subject.is_some() {
    debug!("user is already logged in, redirecting to /");
    Ok(HttpResponse::TemporaryRedirect().append_header((header::LOCATION, "/")).finish())
  } else {
    Ok(HttpResponse::Ok().content_type("text/html").body(get_login_file()))
  }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct LoginPostBody {
  email: String,
  password: String
}

pub async fn post_login(db: Data<DbAddr>, body: Form<LoginPostBody>, subject: Option<SubjectCredential>, cookie_config: Data<CookieConfig>) -> Result<HttpResponse> {
  if subject.is_some() {
    debug!("user is already logged in, redirecting to /");
    return Ok(HttpResponse::TemporaryRedirect().append_header((header::LOCATION, "/")).finish())
  }
  let body = body.into_inner();
  if let Some(session) = db.send(LoginUserLocalCredential {
    email: body.email,
    password: body.password
  }).await.err_problem_json()?.err_problem_json()? {
    info!("created user session {} for user {}", session.id, session.user_id);
    let mut jar = CookieJar::new();
    let mut cookies = jar.signed_mut(&cookie_config.key);
    cookies.add(Cookie::build(cookie_config.name.clone(), session.id.to_string())
        // .domain(hostname())
        .max_age(session.remaining_duration().to_std().unwrap().try_into().unwrap())
        .same_site(SameSite::Lax)
        .http_only(true)
        .secure(server_environment().is_production())
        .finish()
    );
    debug!("created session cookie for session {}", session.id);
    let mut response = HttpResponse::SeeOther();
    response.append_header(("Location", "/"));
    for c in jar.delta() {
      response.append_header((header::SET_COOKIE, c.encoded().to_string()));
    }
    debug!("successfully logged in, redirecting");
    Ok(response.finish())
  } else {
    Err(ProblemJson::new_custom(StatusCode::UNAUTHORIZED, "bad_login", "Unable to login with that email and password combination".into()).into())
  }
}
