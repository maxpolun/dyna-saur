use std::{env};

use actix_web::{HttpResponse, body::MessageBody};
use serde::{de::DeserializeOwned};
use serde_json::Value;
use tokio_postgres::{Client};
use tracing_subscriber::{fmt, EnvFilter};
use url::Url;

pub mod permissions;
pub mod ident;
pub mod db;
pub mod msg;

pub use crate::util::ident::Identifier;
pub use dyna_saur_core::Timestamp;
pub use dyna_db::{PgMapper, PgMappingError, DbMap, establish_connection, test_client, test_transaction, db_error_constraint};

pub enum ServerEnvironment {
  Production,
  Development,
  Testing
}

impl ServerEnvironment {
    /// Returns `true` if the server environment is [`Production`].
    ///
    /// [`Production`]: ServerEnvironment::Production
    pub fn is_production(&self) -> bool {
        matches!(self, Self::Production)
    }

    /// Returns `true` if the server environment is [`Development`].
    ///
    /// [`Development`]: ServerEnvironment::Development
    pub fn is_development(&self) -> bool {
        matches!(self, Self::Development)
    }
}

pub fn server_environment() -> ServerEnvironment {
  match std::env::var("DYNA_SAUR_ENV").unwrap_or_else(|_| "development".into()).as_str() {
    "production" => ServerEnvironment::Production,
    "development" => ServerEnvironment::Development,
    "test" => ServerEnvironment::Testing,
    env => panic!("unknown server env {}", env)
  }
}

fn db_connstring(dbname: &str, template: &str) -> String {
  let mut parsed = Url::parse(template).unwrap();
  parsed.set_path(dbname);
  parsed.into()
}

fn template_db(template: &str) -> String {
  let parsed = Url::parse(template).unwrap();
  parsed.path_segments().unwrap().next().unwrap().to_owned()
}

fn db_name() -> String {
  let uuid = uuid::Uuid::new_v4();
  format!("test_{}", uuid.to_simple())
}

pub struct TestDb {
  template_db: String,
  pub connstring: String,
  db_name: String,
}

impl TestDb {
  async fn from_template(template: String) -> Self {
    let conn = establish_connection(&template).await;
    let name = db_name();
    let create_command = format!("CREATE DATABASE {} TEMPLATE {}", name, template_db(&template));
    println!("executing command \"{}\"", create_command);
    conn.execute(create_command.as_str(), &[]).await.unwrap();
    drop(conn);
    let connstring = db_connstring(&name, &template);
    Self {
      template_db: template,
      db_name: name,
      connstring,
    }
  }

  pub async fn conn(&self) -> Client {
    establish_connection(self.connstring.as_str()).await
  }
}

impl Drop for TestDb {
  fn drop(&mut self) {
    let template_db = self.template_db.clone();
    let db_name = self.db_name.clone();
    let res = std::thread::spawn(move || {
      tokio::runtime::Builder::new_current_thread()
        .enable_io()
        .build()
        .unwrap()
        .block_on(async move {
          let client = establish_connection(&template_db).await;
          let kill_other_connections = format!(
            "SELECT pg_terminate_backend(pg_stat_activity.pid)
            FROM pg_stat_activity
            WHERE pg_stat_activity.datname = '{}'
              AND pid <> pg_backend_pid();",
            &db_name
          );
          client.execute(kill_other_connections.as_str(), &[]).await.unwrap();
          let drop_cmd = format!("DROP DATABASE {}", db_name);
          println!("executing command \"{}\"", drop_cmd);
          client.execute(drop_cmd.as_str(), &[]).await.unwrap();
        });
    })
    .join();
    if res.is_err() {
      println!("got error while waiting for db drop")
    }
  }
}

pub async fn test_db() -> TestDb {
  dotenv::dotenv().ok();
  let subscriber = fmt::Subscriber::builder()
    .with_env_filter(EnvFilter::from_default_env())
    .finish();
  tracing::subscriber::set_global_default(subscriber).ok();
  let template_db = env::var("TEST_DB_TEMPLATE").expect("TEST_DB_TEMPLATE must be set");
  TestDb::from_template(template_db).await
}

pub fn parse_http_response<T: DeserializeOwned>(response: HttpResponse) -> T {
  let body =  response.into_body();

  let bytes = body
    .try_into_bytes()
    .expect("expected a body response");
  serde_json::from_slice(&bytes).unwrap()
}

pub fn parse_http_error_json(res: HttpResponse) -> Value {
  serde_json::from_slice::<Value>(
    &res.into_body().try_into_bytes().unwrap()

  )
  .unwrap()
}
