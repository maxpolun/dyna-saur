use std::str::FromStr;

use bb8_postgres::PostgresConnectionManager;
use dyna_db::Pool;

pub async fn make_db_pool() -> Pool {
  let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL");
  let db_config = tokio_postgres::config::Config::from_str(&database_url).unwrap();

  let manager = PostgresConnectionManager::new(db_config, tokio_postgres::NoTls);
  bb8::Pool::builder()
    .build(manager)
    .await
    .expect("Failed to create pool.")
}
