use subject::Subject;
use tracing::Span;

pub struct Msg<T> {
  pub payload: T,
  pub subject: Option<Subject>,
  pub span: Span
}


impl<T> Msg<T> {
  pub fn new(payload: T, subject: Option<Subject>) -> Self {
    let span = tracing::Span::current();
    Msg { payload, subject, span }
  }

  pub fn subject_id(&self) -> String {
    self.subject.as_ref().map(|s| s.to_string()).unwrap_or_else(|| "system".into())
  }
}
