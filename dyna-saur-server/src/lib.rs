use actix::Actor;
use actix_files::Files;
use actix_web::{
  cookie::Key,
  web::{self, Data},
};
use actors::RealDbActor;
use admin::{
  field::routes::{create_field, index_fields},
  service::{
    credentials::routes::{create_service_credential, delete_service_credential, index_service_credentials},
    routes::{create_service, index_services},
  },
  service_group::routes::{add_membership_to_service_group, create_service_group},
  tenant::routes::{create_tenant, delete_tenant, get_tenant_by_id, index_tenants, tenant_report, update_tenant},
  users::{
    sessions::{csrf_middleware::RequireCsrf, routes::get_csrf_token},
  },
};
use client::get_configs::get_all_configs_for_client;
use dyna_db::Pool;
use dyna_saur_server_users::routes::{index_users, create_user, get_user_role_assignments};
use serde_qs::actix::QsQueryConfig;
use serde_qs::Config as QsConfig;
use shared::{scope::routes::get_all_scopes, subject::authentication_middleware::Authenticate};
use tracing_subscriber::{EnvFilter, fmt, prelude::__tracing_subscriber_SubscriberExt, util::SubscriberInitExt};
use ui::{
  login::{get_login, post_login},
  render_main::get_ui,
};

pub use sqlquery::sql;

mod actors;
pub mod admin;
mod client;
mod pagination;
mod problem_json;
mod query;
pub mod shared;
mod ui;
pub mod util;

pub struct StaticConfig {
  pub db_pool: Pool,
  pub cookie_name: String,
  pub cookie_signing_key: String,
}

pub use actors::DbAddr;

pub struct CookieConfig {
  pub key: Key,
  pub name: String,
}

pub fn setup_tracing() {
  let env_filter = EnvFilter::from_default_env();
  let fmt_layer = fmt::layer().pretty();
  let console_layer = console_subscriber::spawn();
  #[cfg(not(debug_assertions))]
  let fmt_layer = fmt_layer.json();
  tracing_subscriber::registry()
    .with(console_layer)
    .with(env_filter)
    .with(fmt_layer)
    .init()
}

pub fn config_api(cfg: &mut web::ServiceConfig, static_config: StaticConfig) {
  let db_actor = RealDbActor::new(static_config.db_pool.clone());
  let addr = db_actor.start();
  let cookie_key = Key::from(&base64::decode(static_config.cookie_signing_key).unwrap());
  let cookie_config = CookieConfig {
    key: cookie_key.clone(),
    name: static_config.cookie_name.clone(),
  };

  cfg.service(
    web::scope("")
      .app_data(Data::new(addr))
      .app_data(QsQueryConfig::default().qs_config(QsConfig::new(5, false)))
      .app_data(Data::new(cookie_config))
      .wrap(Authenticate {
        cookie_name: static_config.cookie_name,
        signing_key: cookie_key,
        db: static_config.db_pool,
      })
      .service(web::scope("/client").route("/configs", web::get().to(get_all_configs_for_client)))
      .service(web::scope("/internal").route("/csrf_tokens", web::get().to(get_csrf_token)))
      .service(
        web::scope("/admin")
          .wrap(RequireCsrf)
          .service(
            web::scope("/tenants/{tenant_id}/services")
              .route("", web::post().to(create_service))
              .route("", web::get().to(index_services))
              .route("/{service_id}/credentials", web::post().to(create_service_credential))
              .route("/{service_id}/credentials", web::get().to(index_service_credentials))
              .route(
                "/{service_id}/credentials/{credential_id}",
                web::delete().to(delete_service_credential),
              ),
          )
          .service(
            web::scope("/tenants/{tenant_id}/service_groups")
              .route("", web::post().to(create_service_group))
              .route(
                "/{service_group_id}/members",
                web::post().to(add_membership_to_service_group),
              ),
          )
          .service(
            web::scope("/tenants")
              .route("", web::post().to(create_tenant))
              .route("", web::get().to(index_tenants))
              .route("/report", web::post().to(tenant_report))
              .route("/{tenant_id}", web::get().to(get_tenant_by_id))
              .route("/{tenant_id}", web::put().to(update_tenant))
              .route("/{tenant_id}", web::delete().to(delete_tenant)),
          )
          .service(
            web::scope("/fields")
              .route("", web::post().to(create_field))
              .route("", web::get().to(index_fields)),
          )
          .service(web::scope("/scopes").route("", web::get().to(get_all_scopes)))
          .service(
            web::scope("/users")
              .route("", web::get().to(index_users))
              .route("", web::post().to(create_user))
              .route("/{user_id}/role_assignments", web::get().to(get_user_role_assignments)),
          ),
      )
      .service(
        web::scope("/login")
          .route("", web::get().to(get_login))
          .route("", web::post().to(post_login)),
      )
      .route("/", web::get().to(get_ui))
      .route("/ui/{rest:.*}", web::get().to(get_ui))
      .service(Files::new("/assets", "ui/dist").prefer_utf8(true)),
  );
}
