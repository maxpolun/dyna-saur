use actix_web::{HttpResponse, Result, web::Data};

use crate::{shared::subject::model::SubjectCredential, DbAddr, problem_json::{unauthorized, ProblemJsonExt}};

use super::messages::GetAllConfigs;

pub async fn get_all_configs_for_client(db: Data<DbAddr>, subject: SubjectCredential) -> Result<HttpResponse> {
  let cred = match subject {
    SubjectCredential::User(_) => return Err(unauthorized().into()),
    SubjectCredential::Service(sc) => sc,
  };

  let data = db.send(GetAllConfigs(cred)).await.err_problem_json()?.err_problem_json()?;
  Ok(HttpResponse::Ok().json(data))
}
