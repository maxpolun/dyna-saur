use std::ops::Deref;

use actix::{Message, Handler, ResponseFuture};
use dyna_saur_core::{ClientData, ClientScope};

use crate::{admin::{service::credentials::model::ServiceCredential, field::model::{FieldDefinition, Field}}, actors::RealDbActor, shared::scope::model::Scope};


pub struct GetAllConfigs (pub ServiceCredential);

impl Message for GetAllConfigs {
    type Result = Result<ClientData, tokio_postgres::Error>;
}

impl Handler<GetAllConfigs> for RealDbActor {
    type Result = ResponseFuture<<GetAllConfigs as Message>::Result>;

    fn handle(&mut self, msg: GetAllConfigs, _ctx: &mut Self::Context) -> Self::Result {
      let pool = self.pool.clone();
      Box::pin(async move {
        let client = pool.get().await.unwrap();
        let scopes = Scope::all_parents_for_service(
          client.deref(),
          &msg.0.service_id,
          &msg.0.tenant_id
        ).await?.into_iter().map(|s| s.to_client()).collect::<Vec<ClientScope>>();
        let field_defs = FieldDefinition::get_all_in_scopes(client.deref(), &scopes.iter().map(|s| s.id).collect::<Vec<_>>()).await?;
        let fields = Field::get_all_by_ids(
          client.deref(),
          field_defs.iter().map(|fd| fd.id).collect::<Vec<_>>()
        ).await?
            .into_iter()
            .map(|f| f.to_client())
            .collect();
        Ok(ClientData {
            scopes,
            fields,
        })
      })
    }
}
