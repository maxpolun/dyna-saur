-- the default is just here to make migration easier. The seed should always be set from the application code via secure random
ALTER TABLE user_sessions ADD COLUMN csrf_seed bigint NOT NULL DEFAULT (random() * 65535)::bigint;
