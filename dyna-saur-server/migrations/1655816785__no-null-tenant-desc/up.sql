UPDATE tenants SET description = '' WHERE description IS NULL;

ALTER TABLE
  tenants
ALTER COLUMN
  description SET NOT NULL,
ALTER COLUMN
  description SET DEFAULT '';
