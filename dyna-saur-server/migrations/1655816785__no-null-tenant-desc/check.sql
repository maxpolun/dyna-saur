SAVEPOINT check_tenants;

INSERT INTO tenants (id) VALUES ('__migration_check');
do $$
  declare
    desc_test text;
  BEGIN
    SELECT description FROM tenants INTO desc_test WHERE id = '__migration_check';
    if desc_test IS NULL THEN
      raise exception 'desc should only be allowed to be a string, got %', desc_test;
    end if;
  end
$$;

ROLLBACK TO SAVEPOINT check_tenants;
