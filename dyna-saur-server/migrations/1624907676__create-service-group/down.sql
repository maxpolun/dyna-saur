DROP TABLE IF EXISTS service_group_memberships;
DROP TRIGGER IF EXISTS set_service_groups_timestamp ON service_groups;
DROP TABLE IF EXISTS service_groups;
