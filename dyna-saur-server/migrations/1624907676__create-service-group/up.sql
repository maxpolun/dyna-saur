CREATE TABLE service_groups (
  id TEXT PRIMARY KEY,
  description TEXT NOT NULL,
  tenant_id TEXT NOT NULL REFERENCES tenants (id),
  created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TRIGGER set_service_groups_timestamp
BEFORE UPDATE ON service_groups
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();

CREATE TABLE service_group_memberships (
  tenant_id TEXT NOT NULL REFERENCES tenants (id),
  service_group_id TEXT NOT NULL REFERENCES service_groups (id),
  service_id TEXT NOT NULL,
  created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (tenant_id, service_id) REFERENCES services (tenant_id, id),
  PRIMARY KEY (tenant_id, service_group_id, service_id)
);
