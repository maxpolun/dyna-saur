CREATE TABLE services (
  tenant_id TEXT REFERENCES tenants(id),
  id TEXT,
  created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (tenant_id, id)
);
