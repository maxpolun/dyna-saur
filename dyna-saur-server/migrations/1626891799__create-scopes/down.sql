DROP TABLE IF EXISTS scopes;
DROP TRIGGER IF EXISTS create_scope_for_service ON services;
DROP TRIGGER IF EXISTS create_scope_for_service_group ON service_groups;
DROP TRIGGER IF EXISTS create_scope_for_tenant ON tenants;

DROP FUNCTION IF EXISTS insert_tenant_scope;
DROP FUNCTION IF EXISTS insert_service_scope;
DROP FUNCTION IF EXISTS insert_service_group_scope;
