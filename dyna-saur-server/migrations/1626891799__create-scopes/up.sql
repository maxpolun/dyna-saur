CREATE TABLE scopes (
  id TEXT PRIMARY KEY,
  name TEXT NOT NULL,
  tenant_id TEXT NULL,
  service_group_id TEXT NULL,
  service_id TEXT NULL,
  UNIQUE (id, tenant_id, service_group_id, service_id)
);


-- trigger for tenants
CREATE OR REPLACE FUNCTION insert_tenant_scope() RETURNS TRIGGER
  AS $$
  BEGIN
    INSERT INTO scopes (id, name, tenant_id) VALUES (generate_ulid(), 'tenant_'||NEW.id, NEW.id);
    return NEW;
  END
  $$ LANGUAGE plpgsql;

CREATE TRIGGER create_scope_for_tenant
AFTER INSERT ON tenants
FOR EACH ROW
EXECUTE FUNCTION insert_tenant_scope();

-- trigger for services
CREATE OR REPLACE FUNCTION insert_service_scope() RETURNS TRIGGER
  AS $$
  BEGIN
    INSERT INTO scopes
      (id, name, tenant_id, service_id)
    VALUES
      (generate_ulid(), 'service_'||NEW.tenant_id||'_'||NEW.id, NEW.tenant_id, NEW.id);
    return NEW;
  END
  $$ LANGUAGE plpgsql;

CREATE TRIGGER create_scope_for_service
AFTER INSERT ON services
FOR EACH ROW
EXECUTE FUNCTION insert_service_scope();

-- trigger for service groups
CREATE OR REPLACE FUNCTION insert_service_group_scope() RETURNS TRIGGER
  AS $$
  BEGIN
    INSERT INTO scopes
      (id, name, tenant_id, service_group_id)
    VALUES
      (generate_ulid(), 'servicegroup_'||NEW.tenant_id||'_'||NEW.id, NEW.tenant_id, NEW.id);
    return NEW;
  END
  $$ LANGUAGE plpgsql;

CREATE TRIGGER create_scope_for_service_group
AFTER INSERT ON service_groups
FOR EACH ROW
EXECUTE FUNCTION insert_service_group_scope();

-- insert global scope
INSERT INTO scopes (id, name) VALUES (generate_ulid(), 'global');

-- insert existing tenants
INSERT INTO scopes (id, name, tenant_id)
  SELECT generate_ulid(), 'tenant_'||id, id FROM tenants;

  -- insert existing services
INSERT INTO scopes (id, name, tenant_id, service_id)
  SELECT generate_ulid(), 'service_'||tenant_id||'_'||id, tenant_id, id FROM services;

-- insert existing service_groups
INSERT INTO scopes (id, name, tenant_id, service_group_id)
  SELECT generate_ulid(), 'servicegroup_'||tenant_id||'_'||id, tenant_id, id FROM service_groups;
