DROP TRIGGER set_field_value_constraints_timestamp ON field_value_constraints;
DROP TRIGGER set_field_values_timestamp ON field_values;

DROP TABLE field_value_constraints;
DROP TYPE constraint_operation;
DROP TABLE field_values;
