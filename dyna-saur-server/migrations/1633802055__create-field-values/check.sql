SELECT id, field_id, payload, ordinal FROM field_values;
SELECT id,
  field_value_id,
  context_field,
  operation,
  constrained_to_value,
  ordinal,
  created_at,
  updated_at
FROM field_value_constraints;
