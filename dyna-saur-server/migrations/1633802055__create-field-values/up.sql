CREATE TABLE field_values (
  id TEXT PRIMARY KEY,
  field_id TEXT NOT NULL REFERENCES fields(id),
  payload JSONB NOT NULL,
  ordinal INT NOT NULL,
  created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  constraint field_values_unique_ordinal UNIQUE (ordinal, field_id) DEFERRABLE INITIALLY DEFERRED
);

CREATE TYPE constraint_operation AS ENUM (
  'EQUALS',
  'NOT_EQUALS',
  'IN_ARRAY'
);

CREATE TABLE field_value_constraints (
  id TEXT PRIMARY KEY,
  field_value_id TEXT NOT NULL REFERENCES field_values(id),
  context_field TEXT NOT NULL,
  operation constraint_operation NOT NULL,
  constrained_to_value JSONB NOT NULL,
  ordinal INT NOT NULL ,
  created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  constraint field_value_constraints_unique_ordinal UNIQUE (ordinal, field_value_id) DEFERRABLE INITIALLY DEFERRED
);

CREATE TRIGGER set_field_values_timestamp
BEFORE UPDATE ON field_values
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();

CREATE TRIGGER set_field_value_constraints_timestamp
BEFORE UPDATE ON field_value_constraints
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();
