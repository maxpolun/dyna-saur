DROP TRIGGER set_local_user_credentials_timestamp ON local_user_credentials;
DROP TRIGGER set_users_timestamp ON users;

DROP TABLE local_user_credentials;
DROP TABLE users;
