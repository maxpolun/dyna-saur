SELECT id,
  email,
  display_name,
  created_at,
  updated_at
FROM users;

SELECT id,
  user_id,
  crypted_password,
  created_at,
  updated_at
FROM local_user_credentials;
