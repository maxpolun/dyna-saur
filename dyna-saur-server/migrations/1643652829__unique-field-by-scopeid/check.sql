DO $$DECLARE scope text;
BEGIN
  select id from scopes where tenant_id is null INTO scope;
  INSERT INTO fields (id, name, scope_id, salt) VALUES (generate_ulid(), 'unique_test', scope, 1);
  INSERT INTO fields (id, name, scope_id, salt) VALUES (generate_ulid(), 'unique_test', scope, 1);

  -- no exception thrown, constraint isn't working
  DELETE FROM fields WHERE scope_id = scope;
  RAISE  'fields should be unique by name/scope';
EXCEPTION
  WHEN unique_violation THEN
    DELETE FROM fields WHERE scope_id = scope;
END$$;
