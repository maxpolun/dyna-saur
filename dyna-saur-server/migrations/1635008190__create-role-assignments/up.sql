-- Right now, the roles table is pretty much just a placeholder
-- at some point there will be more sophisticated roles
-- for now, just reader and writer
CREATE TABLE roles (
  id TEXT PRIMARY KEY
);

INSERT INTO roles (id) VALUES ('reader'), ('writer');

CREATE TABLE role_assignments (
  user_id TEXT NOT NULL REFERENCES users (id),
  role_id TEXT NOT NULL REFERENCES roles (id),
  scope_id TEXT NOT NULL references scopes (id),
  created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (user_id, role_id, scope_id)
);


CREATE TRIGGER set_role_assignments_timestamp
BEFORE UPDATE ON role_assignments
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();
