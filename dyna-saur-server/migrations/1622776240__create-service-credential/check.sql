SELECT
  id,
  service_id,
  created_at,
  comment,
  algorithm,
  secret
FROM service_credentials;
