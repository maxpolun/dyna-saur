CREATE TYPE service_credential_algorithm AS ENUM (
  'HS256'
);

CREATE TABLE service_credentials (
  id TEXT PRIMARY KEY,
  service_id TEXT NOT NULL,
  tenant_id TEXT NOT NULL,
  created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  comment TEXT,
  algorithm service_credential_algorithm NOT NULL,
  secret TEXT NOT NULL,

  FOREIGN KEY (tenant_id, service_id) REFERENCES services (tenant_id, id)
)
