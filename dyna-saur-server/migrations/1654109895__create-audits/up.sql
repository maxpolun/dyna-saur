CREATE TABLE audits (
  id TEXT PRIMARY KEY,
  table_name TEXT NOT NULL,
  record_id TEXT NOT NULL,
  operation TEXT NOT NULL,
  who_changed_id TEXT NOT NULL,
  record JSONB,
  record_before JSONB,
  audited_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);
