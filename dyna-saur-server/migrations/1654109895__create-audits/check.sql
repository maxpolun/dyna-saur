SELECT id,
  table_name,
  record_id,
  operation,
  who_changed_id,
  record,
  record_before,
  audited_at
FROM audits;
