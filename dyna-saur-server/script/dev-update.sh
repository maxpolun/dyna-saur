#!/usr/bin/env bash
set -euo pipefail

source ./.env

cargo run --bin migrate up
DATABASE_URL="${TEST_DB_TEMPLATE}" cargo run --bin migrate up
