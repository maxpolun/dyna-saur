#!/usr/bin/env bash
set -euo pipefail

if [[ $BASH_SOURCE = */* ]]; then
  current_dir=${BASH_SOURCE%/*}/
else
  current_dir=./
fi

function db_query {
  psql -qt --csv -c "$1"
}

function user_exists {
  result=$(db_query "SELECT COUNT(*) FROM pg_catalog.pg_roles WHERE rolname = '$1';")
  [ "$result" = "1" ]
}

function db_exists {
  result=$(db_query "SELECT COUNT(*) FROM pg_catalog.pg_database WHERE datname = '$1';")
  [ "$result" = "1" ]
}

# test user's password is dynasaur
user_exists "dyna-saur" || createuser -s -P dyna-saur

db_exists "dynaserver_dev" || createdb -O dyna-saur dynaserver_dev
db_exists "dynaserver_test" || createdb -O dyna-saur dynaserver_test

bash "${current_dir}/../../.githooks/install.sh"
