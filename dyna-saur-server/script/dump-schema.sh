#!/bin/bash

URL_REGEX='postgres://([a-zA-Z0-9_-]+):?([a-zA-Z0-9_-]*)@([a-zA-Z0-9_-]+):?([a-zA-Z0-9_-]*)/([a-zA-Z0-9_-]+)'
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
if [[ $DATABASE_URL =~ $URL_REGEX ]]
then
  username="${BASH_REMATCH[1]}"
  password="${BASH_REMATCH[2]}"
  host="${BASH_REMATCH[3]}"
  port="${BASH_REMATCH[4]:-5432}"
  dbname="${BASH_REMATCH[5]}"
  echo $password | pg_dump -s -U $username -h $host -p $port $dbname > $SCRIPT_DIR/db_schema.sql
else
  echo DATABASE_URL did not match
fi
# postgres://dyna-saur:dynasaur@localhost/dynaserver_dev
