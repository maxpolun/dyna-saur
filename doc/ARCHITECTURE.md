# Dyna-saur architecture

This describes the concepts and archiutecture of the dyna-saur dynamic configuration service.

## What is dynamic configuration

Configuration in this context is a set of data that an application or service depends on to perform it's actions. Traditionally configuration was static -- the program would need to restart, or a server redeploy in order for new configuration to be used. However modern applications and services need to update configuration faster than that.

Dynamic configuration is a technology that can be used for

* feature flags
* a/b testing
* canary rollout (of features)
* a lightweight datastore with a built-in admin UI

Many of these cases can be solved using a database, however

1. The configuration may be stored at a different granularity than the database (e.g. configuration is shared between multiple different apps, each with their own database)
2. The configuration may be short-lived (e.g. a feature flag will only exist for a short time)
3. The configuration may only apply to specific users, or to only a specific percent of users

In these cases dynamic configuration is the clear ideal solution compared to either static configuration or an application-managed database. Additionally a dynamic configuration system should have a nice UI for users to update configuration.

## Goal

Dynamic configurations should be flexible in what data can be stored, lightweight to use, and fast to update.

Modifying the dynamic configuration can be sensitive, so security controls need to be strict, but also flexible enough to allow developers using dynamic configuration to not be blocked.

The users of dyna-saur should have the flexibility to set up the system in a variety of ways according to their needs.

## Glossary

Field: a dynamic configuration value. The data model is json.
Scope: A pointer to any part of the hierarchy of dynasaur -- can be global, or to a tenant, a service, or service group. Used by both fields and role assignments
Service: A single client of dyna-saur. Each one has it's own unique set of access keys. Lives inside of a single tenant, but can be connect to equivalent services in a different tenant.
ServiceGroup: A collection of services inside of a single tenant. Can be used for whatever grouping is desired.
Tenant: The toplevel collection of services in dyna-saur. Can represent a variety of different things, thus the rather generic name. See recommended configurations for more details.

## Recommended configurations

### A collection SIMT applications and services

* A tenant represents a "stage" -- e.g. development, staging and production
* Each service is an individual application or service
* Service groups are used for organizational or functional groups (possibly both)
  * e.g. for organizational groups: all services in the core services group could belong to the service group "core", and all supporting mobile applications could be in the group "mobile"
  * e.g. for functional groups: all services related to authorization belong to one group, all those that do machine learning in another group.

### A set of MIST application instances, one per customer

* A tenant represents a customer
* Each service in a tenant is a stage
  * service groups may not be needed in this case.
* alternatively (especially if there are multiple applications or services for each customer) each service group represents a stage, and each service is a staged instance.
