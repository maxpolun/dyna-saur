use std::{fmt::Display, future::Future};

use async_trait::async_trait;
use dyna_db::{DbError};
use dyna_saur_core::id::UniqueId;
use serde::Serialize;
use sqlquery::{sql, SqlCols};
use tokio_postgres::{GenericClient, Transaction};
use tracing::{info, log::error};

#[derive(Debug)]
pub struct NewAudit<'a> {
  pub table_name: &'a str,
  pub record_id: &'a str,
  pub operation: &'a str,
  pub who_changed_id: &'a str,
  pub record: serde_json::Value,
  pub record_before: serde_json::Value,
}

pub struct Audit;

impl<'a> Audit {
  const TABLE: &'static str = "audits";
  const INSERT_COLS: SqlCols<7> = SqlCols([
    "id",
    "table_name",
    "record_id",
    "operation",
    "who_changed_id",
    "record",
    "record_before",
  ]);

  pub async fn create(conn: &impl GenericClient, audit: NewAudit<'a>) -> Result<(), DbError> {
    let NewAudit { table_name, record_id, operation, who_changed_id, record, record_before } = audit;
    let id = UniqueId::new();
    sql!(
      "INSERT INTO {:table} ({:cols})
      VALUES (
        {id},
        {table_name},
        {record_id},
        {operation},
        {who_changed_id},
        {record},
        {record_before}
      )",
    :table = Self::TABLE, :cols = Self::INSERT_COLS,
    id, table_name, record_id, operation, who_changed_id, record, record_before
    ).execute(conn).await?;
    info!("audit {id} created");
    Ok(())
  }

  pub async fn created<T: Auditable>(conn: &Transaction<'a>, new_record: T, who_changed_id: &str) -> Result<(), DbError> {
    Audit::create(
      conn,
      NewAudit {
        table_name: T::table_name(),
        record_id: &new_record.id().to_string(),
        operation: "CREATE",
        who_changed_id,
        record: serde_json::to_value(new_record).unwrap(),
        record_before: serde_json::Value::Null,
      },
    ).await?;
    Ok(())
  }

  pub async fn updated<T: Auditable, E: From<DbError>, F: Future<Output = Result<T, E>>>(conn: &Transaction<'a>, update_fut: F, id: T::Id, who_changed_id: &str) -> Result<T, E> {
    match T::get_by_id(conn, &id).await {
        Ok(Some(old)) => {
        let new = update_fut.await?;
        Audit::create(
          conn,
          NewAudit {
            table_name: T::table_name(),
            record_id: &new.id().to_string(),
            operation: "UPDATE",
            who_changed_id,
            record: serde_json::to_value(&new).unwrap(),
            record_before: serde_json::to_value(old).unwrap(),
          },
        ).await?;
        Ok(new)
      },
      err => {
        // wasn't able to get the old record for some reason.
        error!("Unable to get old record for audit. table: {}, id: {}", T::table_name(), id);
        err?;
        panic!("record missing")
      }
    }
  }

  pub async fn deleted<T: Auditable, E: From<DbError>, F: Future<Output = Result<(), E>>>(conn: &Transaction<'a>, delete_fut: F, id: T::Id, who_changed_id: &str) -> Result<(), E> {
    match T::get_by_id(conn, &id).await {
      Ok(Some(old)) => {
        delete_fut.await?;
        Audit::create(
          conn,
          NewAudit {
            table_name: T::table_name(),
            record_id: &id.to_string(),
            operation: "DELETE",
            who_changed_id,
            record: serde_json::Value::Null,
            record_before: serde_json::to_value(old).unwrap(),
          },
        ).await?;
        Ok(())
      },
      err => {
        // wasn't able to get the old record for some reason.
        error!("Unable to get old record for audit. table: {}, id: {}", T::table_name(), id);
        err?;
        Ok(())
      }
    }
  }
}

#[async_trait(?Send)]
pub trait Auditable: Serialize + Sized {
  type Id: Display;
  fn table_name() -> &'static str;

  fn id(&self) -> Self::Id;
  async fn get_by_id(conn: &impl GenericClient, id: &Self::Id) -> Result<Option<Self>, DbError>;
}

#[cfg(test)]
mod tests {
    use dyna_db::test_transaction;
    use serde_json::{json};

    use super::*;

  #[actix_rt::test]
  async fn it_creates_an_audit() {
    let tx = test_transaction().await;
    Audit::create(&tx, NewAudit {
      table_name: "test",
      record_id: "test12345",
      operation: "CREATE",
      who_changed_id: "system",
      record: json!(5),
      record_before: serde_json::Value::Null
    }).await.unwrap();
  }
}
