use dyna_db::{PgMappingError, PgMapper};
use dyna_saur_core::{ClientScope, id::{UniqueId, Identifier}};
use futures::{try_join};
use pagination::{PaginatedQuery, PaginationParams, Paginated, GenericPaginatedApiError};
use sqlquery::{SqlCols, sql};
use tokio_postgres::{GenericClient, Row};
use serde::{self, Serialize, Deserialize};

pub use dyna_saur_core::{ScopeDescriptor};

#[derive(Debug, Serialize, Deserialize)]
pub struct Scope {
  pub id: UniqueId,
  pub name: String,
  pub descriptor: ScopeDescriptor
}

impl TryFrom<Row> for Scope {
    type Error = PgMappingError;

    fn try_from(row: Row) -> Result<Self, Self::Error> {
        let mapper = PgMapper::new("Scope", row);
        let idents = (
          mapper.map::<Option<Identifier>>("tenant_id")?,
          mapper.map::<Option<Identifier>>("service_id")?,
          mapper.map::<Option<Identifier>>("service_group_id")?
        );
        let kind = match idents {
            (None, None, None) => ScopeDescriptor::Global,
            (Some(tenant_id), None, None) => ScopeDescriptor::Tenant{ tenant_id },
            (Some(tenant_id), Some(service_id), None) => ScopeDescriptor::Service { tenant_id, service_id},
            (Some(tenant_id), None, Some(service_group_id)) => ScopeDescriptor::ServiceGroup { tenant_id, service_group_id },
            _ => panic!("invalid scope")
        };
        Ok(
          Scope {
            id: mapper.map("id")?,
            name: mapper.map("name")?,
            descriptor: kind,
        }
        )
    }
}

const ALL_COLUMNS: SqlCols<5> = SqlCols([
  "id",
  "name",
  "tenant_id",
  "service_id",
  "service_group_id",
]);

impl Scope {
  pub async fn all_parents_for_service(client: &impl GenericClient, service_id: &Identifier, tenant_id: &Identifier) -> Result<Vec<Scope>, tokio_postgres::Error> {
    let query = sql!("
    WITH svc AS (
      SELECT {:cols}
      FROM scopes
      WHERE tenant_id = {tenant_id} AND service_id = {service_id} AND service_group_id IS NULL
    ), tnt AS (
      SELECT {:cols}
      FROM scopes
      WHERE tenant_id = {tenant_id} AND service_id IS NULL AND service_group_id IS NULL
    ), svc_groups AS (
      SELECT {:cols}
      FROM scopes
      WHERE tenant_id = {tenant_id} AND service_id IS NULL AND service_group_id IN (
        SELECT service_group_id FROM service_group_memberships WHERE tenant_id = {tenant_id} AND service_id = {service_id}
      )
    )

    (SELECT * FROM svc) UNION (SELECT * FROM tnt) UNION (SELECT * FROM svc_groups)
    ", :cols = ALL_COLUMNS, tenant_id, service_id);
    let rows = query.query(client).await?;
    Ok(rows.into_iter().map(|r| Scope::try_from(r).unwrap()).collect())
  }

  pub async fn get_opt(client: &impl GenericClient, scope_kind: &ScopeDescriptor) -> Result<Option<Scope>, tokio_postgres::Error> {
    let opt = match scope_kind {
        ScopeDescriptor::Global => {
          let sql = sql!("SELECT {:cols} FROM scopes WHERE tenant_id IS NULL AND service_id IS NULL AND service_group_id IS NULL", :cols = ALL_COLUMNS);
          sql.query_opt(client).await?
        },
        ScopeDescriptor::Tenant { tenant_id } => {
          let sql = sql!("
            SELECT {:cols}
            FROM scopes
            WHERE
              tenant_id = {tenant_id} AND service_id IS NULL AND service_group_id IS NULL",
              :cols = ALL_COLUMNS, tenant_id);
          sql.query_opt(client).await?
        },
        ScopeDescriptor::Service { tenant_id, service_id } => {
          let sql = sql!("
          SELECT {:cols}
          FROM scopes
          WHERE
            tenant_id = {tenant_id} AND service_id = {service_id} AND service_group_id IS NULL",
            :cols = ALL_COLUMNS, tenant_id, service_id);
          sql.query_opt(client).await?
        },
        ScopeDescriptor::ServiceGroup { tenant_id, service_group_id } => {
          let sql = sql!("
            SELECT {:cols}
            FROM scopes
            WHERE
              tenant_id = {tenant_id} AND service_id IS NULL AND service_group_id = {service_group_id}",
              :cols = ALL_COLUMNS, tenant_id, service_group_id);
          sql.query_opt(client).await?
        },
    };

    Ok(opt.map(|row| Scope::try_from(row).unwrap()))
  }

  pub async fn get(client: &impl GenericClient, scope_kind: ScopeDescriptor) -> Result<Scope, tokio_postgres::Error> {
    Self::get_opt(client, &scope_kind).await.map(|opt| match opt {
        Some(s) => s,
        None => panic!("scope not found: {:?}", scope_kind),
    })
  }

  pub async fn get_by_id_opt(client: &impl GenericClient, id: UniqueId) -> Result<Option<Self>, tokio_postgres::Error> {
    let id = id.to_string();
    sql!(
      "SELECT {:cols} FROM scopes WHERE id = {id}",
      :cols = ALL_COLUMNS, id
    ).query_opt(client).await.map(|opt| opt.map(|r| Scope::try_from(r).unwrap()))
  }

  pub async fn get_by_id(client: &impl GenericClient, id: UniqueId) -> Result<Self, tokio_postgres::Error> {
    Self::get_by_id_opt(client, id).await.map(|opt| opt.unwrap())
  }

  pub async fn get_all(client: &impl GenericClient, pagination: &PaginationParams) -> Result<Paginated<Vec<Scope>>, GenericPaginatedApiError> {
    let PaginatedQuery {fields, last_id} = pagination.clone().query()?;
    let (dir, cmp) = fields.sort_order();
    let col = fields.sort_col(&["id"])?;
    let page_size = fields.page_size()?;
    let items = match last_id {
        Some(id) => {
          let query = sql!(
            "SELECT {:cols} FROM scopes WHERE {col} {:cmp} {id} ORDER BY {col} {:dir} LIMIT {page_size}",
            :cols = ALL_COLUMNS, col, :dir, page_size, :cmp, id
          );
          query.query(client).await?
        },
        None => {
          let query = sql!(
            "SELECT {:cols} FROM scopes ORDER BY {col} {:dir} LIMIT {page_size}",
            :cols = ALL_COLUMNS, col, :dir, page_size
          );
          query.query(client).await?
        },
    }.into_iter().map(|row| Scope::try_from(row).unwrap()).collect::<Vec<_>>();

    let cursor = items.last().map(|t| fields.into_cursor(&t.id.to_string()));
    Ok(Paginated::new(cursor, items))
  }

  pub async fn parent(&self, client: &impl GenericClient) -> Result<Option<Self>, tokio_postgres::Error> {
    let tenant_id = match &self.descriptor {
        ScopeDescriptor::Global => return Ok(None),
        ScopeDescriptor::Tenant { tenant_id: _ } => return Self::get_opt(client, &ScopeDescriptor::Global).await,
        ScopeDescriptor::Service { tenant_id, service_id: _ } => tenant_id.clone(),
        ScopeDescriptor::ServiceGroup { tenant_id, service_group_id: _ } => tenant_id.clone(),
    };

    Self::get_opt(client, &ScopeDescriptor::Tenant{ tenant_id }).await
  }

  pub async fn parents(&self, client: &impl GenericClient) -> Result<Vec<Self>, tokio_postgres::Error> {
    let tenant_id = match &self.descriptor {
      ScopeDescriptor::Global => return Ok(Vec::new()),
      ScopeDescriptor::Tenant { tenant_id: _ } => return Self::get_opt(client, &ScopeDescriptor::Global).await.map(|opt| opt.into_iter().collect()),
      ScopeDescriptor::Service { tenant_id, service_id: _ } => tenant_id.clone(),
      ScopeDescriptor::ServiceGroup { tenant_id, service_group_id: _ } => tenant_id.clone(),
    };
    let (global, tenant) = try_join!(Self::get(client, ScopeDescriptor::Global), Self::get(client, ScopeDescriptor::Tenant {tenant_id} ))?;
    Ok(vec![global, tenant])
  }

  pub fn to_client(self) -> ClientScope {
    ClientScope {
        id:self.id,
        descriptor: self.descriptor,
    }
  }
}

pub trait HasScope {
  fn scope_descriptor(&self) -> ScopeDescriptor;
}

#[cfg(test)]
mod tests {
  use std::{convert::TryInto};
  use dyna_db::test_transaction;
use dyna_saur_core::id::ToIdentifierExt;
use pagination::{PaginationFields, SortOrder};
use tokio_postgres::Client;

  use super::*;

  // duplicate creation logic here to avoid a dependency
  async fn create_tenant(client: &Client, id: &str) {
    sql!("INSERT INTO tenants (id, description) VALUES ({id}, {description}) ", id, description = id).execute(client).await.unwrap();
  }

  async fn create_service(client: &Client, tenant_id: &str, id: &str) {
    sql!("INSERT INTO services (tenant_id, id) VALUES ({tenant_id}, {id})", tenant_id, id).execute(client).await.unwrap();
  }

  async fn create_service_group(client: &Client, tenant_id: &str, id: &str) {
    sql!("INSERT INTO service_groups (tenant_id, id, description) VALUES ({tenant_id}, {id}, '')", tenant_id, id).execute(client).await.unwrap();
  }

  async fn add_service_to_group(client: &Client, tenant_id: &str, service_id: &str, group_id: &str) {
    sql!("
    INSERT INTO service_group_memberships
      (tenant_id, service_id, service_group_id)
    VALUES
      ({tenant_id}, {service_id}, {group_id})",
      tenant_id, service_id, group_id).execute(client).await.unwrap();
  }

  #[actix_rt::test]
  async fn gets_all_parents() {
    let client = test_transaction().await;

    create_tenant(&client, "t1").await;
    create_tenant(&client, "t2").await;

    create_service(&client, "t1", "s1").await;
    create_service(&client, "t1", "s2").await;

    create_service_group(&client, "t1", "parent_sg1").await;
    create_service_group(&client, "t1", "parent_sg2").await;
    create_service_group(&client, "t1", "nonparent_sg").await;

    add_service_to_group(&client, "t1", "s1", "parent_sg1").await;
    add_service_to_group(&client, "t1", "s2", "parent_sg1").await;
    add_service_to_group(&client, "t1", "s1", "parent_sg2").await;
    add_service_to_group(&client, "t1", "s2", "nonparent_sg").await;

    let scopes = Scope::all_parents_for_service(&client, &"s1".try_into().unwrap(), &"t1".try_into().unwrap()).await.unwrap();

    // we get the overall tenant
    assert!(scopes.iter().any(|s| s.descriptor == ScopeDescriptor::Tenant {tenant_id: "t1".try_into().unwrap()}));
    // and the specific service
    assert!(scopes.iter().any(|s| s.descriptor == ScopeDescriptor::Service {tenant_id: "t1".try_into().unwrap(), service_id: "s1".try_into().unwrap()}));
    // and all service groups containing the service
    assert!(scopes.iter().any(|s| s.descriptor == ScopeDescriptor::ServiceGroup {tenant_id: "t1".try_into().unwrap(), service_group_id: "parent_sg1".try_into().unwrap()}));
    assert!(scopes.iter().any(|s| s.descriptor == ScopeDescriptor::ServiceGroup {tenant_id: "t1".try_into().unwrap(), service_group_id: "parent_sg1".try_into().unwrap()}));

    // and *don't* get any of the other stuff
    assert!(!scopes.iter().any(|s| s.descriptor == ScopeDescriptor::Tenant {tenant_id: "t2".try_into().unwrap()}));
    assert!(!scopes.iter().any(|s| s.descriptor == ScopeDescriptor::Service {tenant_id: "t1".try_into().unwrap(), service_id: "s2".try_into().unwrap()}));
    assert!(!scopes.iter().any(|s| s.descriptor == ScopeDescriptor::ServiceGroup {tenant_id: "t1".try_into().unwrap(), service_group_id: "nonparent_sg".try_into().unwrap()}));
  }

  #[actix_rt::test]
  async fn gets_all() {
    let client = test_transaction().await;
    create_tenant(&client, "t1").await;
    create_tenant(&client, "t2").await;

    create_service(&client, "t1", "s1").await;
    create_service(&client, "t1", "s2").await;

    create_service_group(&client, "t1", "parent_sg1").await;
    create_service_group(&client, "t1", "parent_sg2").await;
    create_service_group(&client, "t1", "nonparent_sg").await;

    add_service_to_group(&client, "t1", "s1", "parent_sg1").await;
    add_service_to_group(&client, "t1", "s2", "parent_sg1").await;
    add_service_to_group(&client, "t1", "s1", "parent_sg2").await;
    add_service_to_group(&client, "t1", "s2", "nonparent_sg").await;

    let scopes = Scope::get_all(&client,
      &PaginationParams::PaginationFields(PaginationFields{
        sort_by: "id".into(),
        sort_order: SortOrder::Ascending,
        page_size: 5
    })).await.unwrap();

    assert_eq!(scopes.items().len(), 5);

    let scopes = Scope::get_all(&client,
      &PaginationParams::Cursor(scopes.cursor().unwrap().into())).await.unwrap();

    assert_eq!(scopes.items().len(), 5);

    let scopes = Scope::get_all(&client,
      &PaginationParams::PaginationFields(PaginationFields{
        sort_by: "id".into(),
        sort_order: SortOrder::Ascending,
        page_size: 99
    })).await.unwrap();

    assert_eq!(scopes.items().len(), 8); // setup creates 7 scopes + 1 global already existing

    let tenant_id = "t1".to_identifier();
    assert!(vec![
      ScopeDescriptor::Global,
      ScopeDescriptor::Tenant {tenant_id: tenant_id.clone()},
      ScopeDescriptor::Tenant {tenant_id: "t2".to_identifier()},
      ScopeDescriptor::Service {tenant_id: tenant_id.clone(), service_id: "s1".to_identifier()},
      ScopeDescriptor::Service {tenant_id: tenant_id.clone(), service_id: "s2".to_identifier()},
      ScopeDescriptor::ServiceGroup {tenant_id: tenant_id.clone(), service_group_id: "parent_sg1".to_identifier()},
      ScopeDescriptor::ServiceGroup {tenant_id: tenant_id.clone(), service_group_id: "parent_sg2".to_identifier()},
      ScopeDescriptor::ServiceGroup {tenant_id, service_group_id: "nonparent_sg".to_identifier()},
    ].iter().all(|kind| scopes.items().iter().any(|scope| kind == &scope.descriptor)),
      "expect all scopes to exist"
    )
  }
}
