use std::{future::{Ready, ready}, fmt::Display};

use actix_web::{FromRequest, HttpMessage, error::ErrorUnauthorized};
use dyna_saur_core::id::{UniqueId, Identifier};
use tracing::debug;

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Subject {
  User {
    user_id: UniqueId
  },
  Service {
    tenant_id: Identifier,
    service_id: Identifier
  }
}


impl FromRequest for Subject {
  type Error = actix_web::Error;

  type Future = Ready<Result<Self, Self::Error>>;

  fn from_request(req: &actix_web::HttpRequest, _payload: &mut actix_web::dev::Payload) -> Self::Future {
    ready(
      req
        .extensions()
        .get::<Subject>()
        .cloned()
        .ok_or_else(|| {
          debug!("404 because no subject found on request");
          ErrorUnauthorized("Not Authorized")
        }))
  }
}

impl Display for Subject {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
      f.write_str("subject:")?;
      match self {
        Subject::User { user_id } => {
          f.write_str("user:")?;
          user_id.fmt(f)
        },
        Subject::Service { tenant_id, service_id} => {
          f.write_str("service:tenant:")?;
          tenant_id.fmt(f)?;
          f.write_str(":service_id:")?;
          service_id.fmt(f)
        },
    }
    }
}
