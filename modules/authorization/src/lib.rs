use std::{ops::Deref, fmt::Display};

use actix::{Handler, Message, ResponseFuture, Addr, Actor};
use actix_web::Result;
use dyna_db::{actor::{RealDbActor}, DbError};
use dyna_saur_core::{ScopeDescriptor, id::UniqueId};
use dyna_saur_server_scopes::Scope;
use problem_json::{unauthorized, ProblemJsonExt};
use sqlquery::sql;
use subject::Subject;
use tokio_postgres::GenericClient;
use tracing::{span, Level, info, debug, trace};

#[derive(Debug)]
pub enum Permission {
    Read,
    Write
}

impl Permission {
  pub fn role_name(&self) -> &'static str {
    match self {
        Permission::Read => "reader",
        Permission::Write => "writer",
    }
  }
}

impl Display for Permission {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("permission:")?;
        f.write_str(match self {
            Permission::Read => "read",
            Permission::Write => "write",
        })
    }
}

pub async fn permission_at_self_or_any_parent<Db: Actor + Handler<PermissionAtSelfOrAnyParent>>(db: &Addr<Db>, current_scope: ScopeDescriptor, permission: Permission, subject: &Subject) -> Result<(), actix_web::Error>
  where <Db as actix::Actor>::Context: actix::dev::ToEnvelope<Db, PermissionAtSelfOrAnyParent> {
  let check = {
    if let Subject::User {user_id} = subject {
      db.send(PermissionAtSelfOrAnyParent {
        current_scope,
        permission,
        user_id: *user_id
      }).await.err_problem_json()?
    } else {
      false
    }
  };

  if check {
    Ok(())
  } else {
    Err(unauthorized().into())
  }
}

pub struct PermissionAtSelfOrAnyParent {
  pub current_scope: ScopeDescriptor,
  pub permission: Permission,
  pub user_id: UniqueId
}

impl Message for PermissionAtSelfOrAnyParent {
  type Result = bool;
}

impl Handler<PermissionAtSelfOrAnyParent> for RealDbActor {
  type Result = ResponseFuture<<PermissionAtSelfOrAnyParent as Message>::Result>;
  fn handle(&mut self, msg: PermissionAtSelfOrAnyParent, _ctx: &mut Self::Context) -> Self::Result {
    let pool = self.pool.clone();
    Box::pin(async move {
      let client = pool.get().await.unwrap();
      check_permission_at_any_parent(client.deref(), msg).await
    })
  }
}

async fn check_permission_at_any_parent(client: &impl GenericClient, msg: PermissionAtSelfOrAnyParent) -> bool {
  let s = if let Ok(s) = Scope::get(client, msg.current_scope).await {s} else {return false};
  debug!("checking for {} at {} or any parent for {}", msg.permission, s.id, msg.user_id);
  let parents = if let Ok(scopes) = s.parents(client).await {scopes} else {return false};
  let mut scope_ids = parents.into_iter().map(|s| s.id).collect::<Vec<_>>();
  scope_ids.push(s.id);
  let ra_result = get_all_roles_by_scope_ids(client, msg.user_id, scope_ids.as_slice()).await;
  debug!("got role assignments: {:?}", ra_result);
  let ra = if let Ok(ras) = ra_result {ras} else {return false};
  debug!("got {} role assignments", ra.len());
  ra.iter().any(|r| match msg.permission {
    // write permission implies read, so any RA will mean that there is permission
    Permission::Read => true,
    Permission::Write => r == "writer",
})
}


pub async fn user_has_global_permission<Db: Actor + Handler<HasGlobalPermission>>(db: &Addr<Db>, subject: Subject, permission: Permission) -> Result<()>
  where <Db as actix::Actor>::Context: actix::dev::ToEnvelope<Db, HasGlobalPermission> {
  let _span = span!(Level::INFO, "checking user for global access", %subject, %permission);
  if let Subject::User { user_id } = subject {
    if !db.send(HasGlobalPermission {user_id, permission}).await.err_problem_json()? {
      info!("user does not have permission");
      return Err(unauthorized().into())
    }
  } else {
    info!("subject is not a user");
    return Err(unauthorized().into())
  }
  Ok(())
}




async fn get_all_roles_by_scope_ids(client: &impl GenericClient, user_id: UniqueId, scope_ids: &[UniqueId]) -> Result<Vec<String>, DbError> {
  Ok(
    sql!(
      "SELECT role_id FROM role_assignments WHERE user_id = {user_id} AND scope_id = ANY({scope_ids})",
    user_id, scope_ids)
      .query(client)
      .await?
      .into_iter()
      .map(|row| row.get(0))
      .collect())
}


pub struct HasGlobalPermission {
  pub user_id: UniqueId,
  pub permission: Permission
}

impl Message for HasGlobalPermission {
  type Result = bool;
}

impl Handler<HasGlobalPermission> for RealDbActor {
  type Result = ResponseFuture<<HasGlobalPermission as Message>::Result>;
  fn handle(&mut self, msg: HasGlobalPermission, _ctx: &mut Self::Context) -> Self::Result {
    let pool = self.pool.clone();
    Box::pin(async move {
      let client = pool.get().await.unwrap();
      let s = match Scope::get(client.deref(), ScopeDescriptor::Global).await {
        Ok(s) => s,
        Err(_) => return false,
      };
      trace!("got global scope");
      match get_role_assignment_at_scope(client.deref(), msg.user_id, s.id).await {
        Ok(role_id) => {
          debug!("got role assignment: {:?}", role_id);
          match msg.permission {
            Permission::Read => role_id.is_some(),
            Permission::Write => role_id.map(|role_id| role_id == msg.permission.role_name()).unwrap_or(false),
        }
        },
        Err(_) => false,
    }
    })
  }
}

pub async fn get_role_assignment_at_scope(client: &impl GenericClient, user_id: UniqueId, scope_id: UniqueId) -> Result<Option<String>, DbError> {
  sql!(
    "SELECT role_id FROM role_assignments WHERE user_id = {user_id} AND scope_id = {scope_id}",
    user_id, scope_id
  ).query_opt(client).await.map(|opt| opt.map(|r| r.get(0)))
}
