use std::{str::FromStr, fmt::Display};
#[cfg(feature = "db")]
use std::{error::Error};


use serde::{Serialize, Deserialize};
use ulid::Ulid;

#[cfg(feature = "db")]
use postgres_types::{FromSql, ToSql, IsNull, accepts, to_sql_checked};
#[cfg(feature = "db")]
use bytes::{BytesMut, BufMut};

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[serde(try_from = "String")]
#[serde(into = "String")]
pub struct Identifier(String);

#[derive(Debug, thiserror::Error)]
pub enum IdentifierError {
  #[error("identifiers may only contain ascii alphanumeric characters and _")]
  InvalidChar,
  #[error("identifiers may range from 1-255 characters")]
  BadLength
}

impl Identifier {
  const MAX_ASCII_CHARS: usize = 255;
  pub fn new(id: String) -> Result<Self, IdentifierError>  {
    if !id.chars().all(|c| c.is_ascii_alphanumeric() || c == '_') {
      return Err(IdentifierError::InvalidChar);
    }

    if !(1..=Self::MAX_ASCII_CHARS).contains(&id.len()) {
      return Err(IdentifierError::BadLength)
    }

    Ok(Self(id))
  }

  pub fn as_str(&self) -> &str {
    self.0.as_str()
  }
}

impl FromStr for Identifier {
    type Err = IdentifierError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Identifier::new(s.to_owned())
    }
}

impl TryFrom<String> for Identifier {
    type Error = IdentifierError;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        Identifier::new(value)
    }
}

impl TryFrom<&str> for Identifier {
  type Error = IdentifierError;

  fn try_from(value: &str) -> Result<Self, Self::Error> {
      Identifier::new(value.to_owned())
  }
}

impl From<Identifier> for String {
    fn from(id: Identifier) -> Self {
        id.0
    }
}

impl Display for Identifier {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

pub trait ToIdentifierExt {
  fn to_identifier(&self) -> Identifier;
}

impl ToIdentifierExt for str {
    fn to_identifier(&self) -> Identifier {
        Identifier::new(self.to_owned()).unwrap()
    }
}

impl ToIdentifierExt for String {
    fn to_identifier(&self) -> Identifier {
        Identifier::new(self.clone()).unwrap()
    }
}

#[cfg(feature = "db")]
impl ToSql for Identifier {
    fn to_sql(&self, ty: &postgres_types::Type, out: &mut BytesMut) -> Result<postgres_types::IsNull, Box<dyn Error + Sync + Send>>
    where
        Self: Sized {
        self.0.to_sql(ty, out)
    }

    fn accepts(ty: &postgres_types::Type) -> bool
    where
        Self: Sized {
        <String as ToSql>::accepts(ty)
    }

    fn to_sql_checked(
        &self,
        ty: &postgres_types::Type,
        out: &mut BytesMut,
    ) -> Result<postgres_types::IsNull, Box<dyn Error + Sync + Send>> {
        self.0.to_sql_checked(ty, out)
    }
}

#[cfg(feature = "db")]
impl<'a> FromSql<'a> for Identifier {
    fn from_sql(ty: &postgres_types::Type, raw: &'a [u8]) -> Result<Self, Box<dyn Error + Sync + Send>> {
        let s = <String as FromSql>::from_sql(ty, raw)?;
        Ok(Identifier::new(s)?)
    }

    fn accepts(ty: &postgres_types::Type) -> bool {
      <String as FromSql>::accepts(ty)
    }
}


#[derive(Debug, Serialize, Deserialize, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Default, Hash)]
#[serde(from = "Ulid")]
#[serde(into = "Ulid")]
pub struct UniqueId(Ulid);

impl From<Ulid> for UniqueId {
    fn from(u: Ulid) -> Self {
        Self(u)
    }
}

impl From<UniqueId> for Ulid {
    fn from(u: UniqueId) -> Self {
        u.0
    }
}

impl Display for UniqueId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

#[cfg(feature = "db")]
impl ToSql for UniqueId {
  fn to_sql(&self, _ty: &postgres_types::Type, out: &mut BytesMut) -> Result<postgres_types::IsNull, Box<dyn Error + Sync + Send>>
  where
      Self: Sized {
        let mut slicebuf = [0_u8; 26];
        self.0.to_str(&mut slicebuf)?;
        out.put_slice(&slicebuf);
        Ok(IsNull::No)
  }

  accepts!(TEXT, CHAR);

  to_sql_checked!();
}

#[cfg(feature = "db")]
impl<'a> FromSql<'a> for UniqueId {
  fn from_sql(_ty: &postgres_types::Type, raw: &'a [u8]) -> Result<Self, Box<dyn Error + Sync + Send>> {
      Ok(Self(
        Ulid::from_str(std::str::from_utf8(raw)?)?
      ))
  }

  accepts!(TEXT, CHAR);
}

impl FromStr for UniqueId {
    type Err = ulid::DecodeError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self(Ulid::from_str(s)?))
    }
}

impl UniqueId {
  pub fn new() -> Self {
    Self (Ulid::new())
  }
}
