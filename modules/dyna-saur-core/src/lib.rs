use id::{UniqueId, Identifier};
use serde::{Deserialize, Serialize};
#[cfg(feature = "db")]
use postgres_types::{FromSql, ToSql};

pub mod id;

pub type Timestamp = chrono::DateTime<chrono::Utc>;

#[derive(Debug, Serialize, Deserialize)]
pub struct ClientData {
  pub scopes: Vec<ClientScope>,
  pub fields: Vec<ClientField>
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ClientScope {
  pub id: UniqueId,
  pub descriptor: ScopeDescriptor
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize, Clone)]
#[serde(tag = "kind", rename_all = "snake_case")]
pub enum ScopeDescriptor {
  Global,
  Tenant {tenant_id: Identifier},
  Service {tenant_id: Identifier, service_id: Identifier},
  ServiceGroup {tenant_id: Identifier, service_group_id: Identifier}
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ClientField {
  pub id: UniqueId,
  pub name: String,
  pub scope_id: UniqueId,
  pub salt: i32,
  pub values: Vec<ClientFieldValue>
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ClientFieldValue {
  pub id: UniqueId,
  pub payload: serde_json::Value,
  pub constraints: Vec<ClientFieldValueConstraint>
}

#[derive(Debug, Serialize, Deserialize, Copy, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "db", derive(ToSql, FromSql))]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
#[cfg_attr(feature = "db", postgres(name="constraint_operation"))]
pub enum ConstraintOperation {
  #[cfg_attr(feature = "db", postgres(name = "EQUALS"))]
  Equals,
  #[cfg_attr(feature = "db", postgres(name = "NOT_EQUALS"))]
  NotEquals,
  #[cfg_attr(feature = "db", postgres(name = "IN_ARRAY"))]
  InArray
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ClientFieldValueConstraint {
  pub id: UniqueId,
  pub context_field: String,
  pub operation: ConstraintOperation,
  pub constrained_to_value: serde_json::Value,
}

#[cfg(test)]
mod tests {

}
