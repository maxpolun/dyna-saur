use std::ops::Deref;

use actix::{Handler, Message, ResponseFuture};
use dyna_db::actor::RealDbActor;
use pagination::{PaginationParams, Paginated, GenericPaginatedApiError};

use crate::model::User;


pub struct IndexUsers {
  pub params: PaginationParams
}

impl Message for IndexUsers {
  type Result = Result<Paginated<Vec<User>>, GenericPaginatedApiError>;
}

impl Handler<IndexUsers> for RealDbActor {
    type Result = ResponseFuture<<IndexUsers as Message>::Result>;

    fn handle(&mut self, msg: IndexUsers, _ctx: &mut Self::Context) -> Self::Result {
      let pool = self.pool.clone();
      Box::pin(async move {
        let client = pool.get().await.unwrap();
        User::index(client.deref(), msg.params).await
      })
    }
}
