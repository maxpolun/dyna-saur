use actix::{Addr};
#[cfg(test)]
use actix::{actors::mocker::Mocker};
use actix_web::{HttpResponse, web::Data, Result};
use authorization::{permission_at_self_or_any_parent, Permission};
use dyna_db::actor::RealDbActor;
use dyna_saur_core::ScopeDescriptor;
use pagination::PaginationParams;
use problem_json::ProblemJsonExt;
use serde::Deserialize;
use serde_qs::actix::QsQuery;
use subject::Subject;

use crate::messages::IndexUsers;

#[cfg(not(test))]
pub type DbActor = RealDbActor;
#[cfg(test)]
pub type DbActor = Mocker<RealDbActor>;

pub type DbAddr = Addr<DbActor>;


#[derive(Debug, Deserialize)]
pub struct UserIndexParams {
  pub pagination:PaginationParams
}

pub async fn index_users(db: Data<DbAddr>, subject: Subject, params: QsQuery<UserIndexParams>) -> Result<HttpResponse> {
  permission_at_self_or_any_parent(db.as_ref(), ScopeDescriptor::Global, Permission::Write, &subject).await?;
  let users = db.send(IndexUsers {
    params: params.into_inner().pagination,
  }).await.err_problem_json()?.err_problem_json()?;

  Ok(HttpResponse::Ok().json(users))
}

pub async fn create_user() -> HttpResponse {
  todo!()
}

pub async fn get_user_role_assignments() -> HttpResponse {
  todo!()
}
