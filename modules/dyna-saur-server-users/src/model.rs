use actix_web::Result;
use argon2::{Argon2, PasswordHash, PasswordHasher, PasswordVerifier, password_hash::SaltString};
use derive_pgmap::DbMap;
use dyna_db::{DbMap, DbError};
use dyna_saur_core::{id::UniqueId, Timestamp};
use pagination::{PaginationParams, GenericPaginatedApiError, PaginatedQuery, Paginated};
use rand::rngs::OsRng;
use sqlquery::{SqlCols, sql};
use tokio_postgres::{GenericClient};
use serde::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize, DbMap)]
pub struct User {
  pub id: UniqueId,
  pub email: String,
  pub display_name: String,
  pub created_at: Timestamp,
  pub updated_at: Timestamp
}

#[derive(Debug, DbMap)]
pub struct LocalCredential {
  pub id: UniqueId,
  pub user_id: UniqueId,
  pub crypted_password: String,
  pub created_at: Timestamp,
  pub updated_at: Timestamp
}

#[derive(Debug, thiserror::Error)]
pub enum CreateUserError {
  #[error("Email must contain an '@'")]
  InvalidEmail,
  #[error("Display name must not be blank")]
  InvalidDisplayName,
  #[error("Error saving user to database: {}", .0)]
  DbError(#[from] tokio_postgres::Error)
}

#[derive(Debug, thiserror::Error)]
pub enum CreateLocalCredentialError {
  #[error("Password must be at least 8 characters long")]
  InvalidPassword,
  #[error("Password must match confirmation")]
  PasswordDoesNotMatchConfirmation,
  #[error("Error saving credential to database: {}", .0)]
  DbError(#[from] tokio_postgres::Error),
  #[error("Error Hashing password")]
  CryptError(#[from] argon2::password_hash::Error)
}

pub fn password_hasher() -> Argon2<'static> {
  Argon2::default()
}

impl User {
  const TABLE: &'static str = "users";
  const ALL_COLS: SqlCols<5> = SqlCols(["id", "email", "display_name", "created_at", "updated_at"]);
  const CREATE_COLS: SqlCols<3> = SqlCols(["id", "email", "display_name"]);
  const SORTABLE_COLUMNS: SqlCols<2> = SqlCols(["email", "display_name"]);

  pub async fn create(client: &impl GenericClient, email: &str, display_name: &str) -> Result<User, CreateUserError> {
    if !email.contains('@') {
      return Err(CreateUserError::InvalidEmail)
    }
    let display_name = display_name.trim();

    if display_name.is_empty() {
      return Err(CreateUserError::InvalidDisplayName)
    }
    let id = UniqueId::new().to_string();
    Ok(sql!("INSERT INTO {:table} ({:cols}) VALUES ({id}, {email}, {display_name}) RETURNING {:all_cols}",
      :table = Self::TABLE, :cols = Self::CREATE_COLS, :all_cols = Self::ALL_COLS,
      id, email, display_name
    ).query_one(client).await.map(User::db_map)?)
  }

  pub async fn create_local_credential(&self, client: &impl GenericClient, password: &str, password_confirmation: &str) -> Result<LocalCredential, CreateLocalCredentialError> {
    let (password, password_confirmation) = (password.trim(), password_confirmation.trim());
    if password.chars().count() < LocalCredential::MIN_PW_CHARS {
      return Err(CreateLocalCredentialError::InvalidPassword)
    }
    if password != password_confirmation {
      return Err(CreateLocalCredentialError::PasswordDoesNotMatchConfirmation)
    }
    let id = UniqueId::new();
    let user_id = self.id;
    let salt = SaltString::generate(&mut OsRng);
    let crypted_password = password_hasher().hash_password(password.as_bytes(), &salt)?.to_string();
    let row = sql!("INSERT INTO {:table} ({:insert_cols}) VALUES ({id}, {user_id}, {crypted_password}) RETURNING {:all_cols}",
      :table = LocalCredential::TABLE, :insert_cols = LocalCredential::INSERT_COLS, :all_cols = LocalCredential::ALL_COLS,
      id, user_id, crypted_password
    ).query_one(client).await?;
    Ok(LocalCredential::db_map(row))
  }

  pub async fn find_by_email(client: &impl GenericClient, email: &str) -> Result<Option<Self>, DbError> {
    Ok(sql!("SELECT {:cols} from {:table} WHERE email = {email}",
      :cols = Self::ALL_COLS, :table = Self::TABLE, email
    ).query_opt(client).await?.map(User::db_map))
  }

  pub async fn index(client: &impl GenericClient, params: PaginationParams) -> Result<Paginated<Vec<User>>, GenericPaginatedApiError> {
    let PaginatedQuery { fields, last_id } = params.clone().query()?;
    let (dir, cmp) = fields.sort_order();
    let col = fields.sort_col(&Self::SORTABLE_COLUMNS.0)?;
    let page_size = fields.page_size()?;
    let items = sql!("SELECT {:cols} FROM {:table}
      {?last_id} WHERE id {:cmp} {last_id} {/?last_id}
      ORDER BY {col} {:dir}
      LIMIT {page_size}",
      :cols = Self::ALL_COLS, :table = Self::TABLE, :cmp = cmp,
      ?last_id = last_id,
      col, :dir, page_size
    ).query(client).await?.into_iter().map(User::db_map).collect::<Vec<_>>();
     let cursor = items.last().map(|t| fields.into_cursor(&t.id.to_string()));
    Ok(Paginated::new(cursor, items))

  }

  pub async fn local_credential(&self, client: &impl GenericClient) -> Result<Option<LocalCredential>, DbError> {
    let user_id = self.id.to_string();
    sql!("SELECT {:cols} FROM {:table} WHERE user_id = {user_id}",
      :cols = LocalCredential::ALL_COLS, :table = LocalCredential::TABLE, user_id
    ).query_opt(client).await.map(|o| o.map(LocalCredential::db_map))
  }
}

impl LocalCredential {
  const MIN_PW_CHARS: usize = 8;
  const TABLE: &'static str = "local_user_credentials";
  const ALL_COLS: SqlCols<5> = SqlCols(["id", "user_id", "crypted_password", "created_at", "updated_at"]);
  const INSERT_COLS: SqlCols<3> = SqlCols(["id", "user_id", "crypted_password"]);

  pub fn validate_password(&self, password: &str) -> bool {
    if let Ok(hash) = PasswordHash::new(&self.crypted_password) {
      return password_hasher().verify_password(password.as_bytes(), &hash).is_ok()
    }
    false
  }
}

#[cfg(test)]
mod tests {
  use dyna_db::test_transaction;
use pagination::{PaginationFields, SortOrder};
use pretty_assertions::assert_eq;

  use super::*;

  #[actix_rt::test]
  async fn can_create_user() {
    let tx = test_transaction().await;

    User::create(&tx, "test@example.com", "test").await.unwrap();
  }

  #[actix_rt::test]
  async fn checks_for_email() {
    let tx = test_transaction().await;

    assert!(matches!(User::create(&tx, "notanemail", "test").await, Err(CreateUserError::InvalidEmail)));
  }

  #[actix_rt::test]
  async fn checks_for_display_name() {
    let tx = test_transaction().await;

    assert!(matches!(User::create(&tx, "test@example.com", "        ").await, Err(CreateUserError::InvalidDisplayName)));
  }

  #[actix_rt::test]
  async fn can_index() {
    let tx = test_transaction().await;

    for i in 1..1000 {
      User::create(&tx, &format!("test{:03}@example.com", i), &format!("test{}", i)).await.unwrap();
    }

    let users = User::index(&tx, PaginationParams::PaginationFields(PaginationFields {
      sort_by: "email".into(),
      sort_order: SortOrder::Ascending,
      page_size: 10
    })).await.unwrap();

    assert_eq!(users.items().len(), 10);
    assert_eq!(users.items()[5].email, "test006@example.com");

    let users2 = User::index(&tx, PaginationParams::Cursor(users.cursor().unwrap().to_string())).await.unwrap();

    assert_eq!(users2.items().len(), 10);
    assert_eq!(users2.items()[5].email, "test016@example.com");
  }
}
