use std::{collections::HashMap, sync::{Arc, Mutex}, time::Duration};

use dyna_saur_core::{id::UniqueId, ClientScope, ClientField, ClientData, ScopeDescriptor};
use log::{warn, debug, trace};
use url::Url;

#[derive(Debug, Clone)]
pub struct ClientConfig {
  pub base_url: Url,
  pub secret: String,
  pub polling_interval: Duration,
}

pub struct Client {
  pub config: ClientConfig,
  state: Option<ClientState>
}

#[derive(Debug)]
pub enum ClientGetConfigError {
  NoInitialFieldState,
  FieldNotFound,
  NoValue,
  InvalidConstraint
}

impl Client {
    pub fn new(config: ClientConfig) -> Arc<Mutex<Self>> {
      // TODO: benchmark rwlock vs mutex. Keeping it simple for now
      Arc::new(Mutex::new(Self { config, state: None}))
    }

    pub fn update_state(&mut self, new_data: ClientData) {
      self.state.replace(ClientState::new(new_data));
      trace!("updated dyna-saur state");
    }

    pub fn get_raw(&self, name: &str, ctx: &HashMap<String, serde_json::Value>) -> Result<serde_json::Value, ClientGetConfigError> {
      let state = self.state.as_ref().ok_or(ClientGetConfigError::NoInitialFieldState)?;
      let field = {
        if let Some(f) = state.service_fields_by_name.get(name) {
          f
        } else if let Some(f) = state.sg_fields_by_name.iter().find_map(|sg_hash| sg_hash.get(name)) {
          f
        } else if let Some(f) = state.tenant_fields_by_name.get(name) {
          f
        } else {
          return Err(ClientGetConfigError::FieldNotFound)
        }
      };

      let value = field.values.iter().find(|&v| {
        v.constraints.iter().all(|c| {
          let ctx_val = ctx.get(c.context_field.as_str());
          match c.operation {
            dyna_saur_core::ConstraintOperation::Equals => {
              match ctx_val {
                Some(ctx_val) => ctx_val == &c.constrained_to_value,
                None => c.constrained_to_value.is_null(),
              }
            },
            dyna_saur_core::ConstraintOperation::NotEquals => {
              match ctx_val {
                Some(ctx_val) => ctx_val != &c.constrained_to_value,
                None => !c.constrained_to_value.is_null(),
              }
            },
            dyna_saur_core::ConstraintOperation::InArray => {
              if let Some(arr) = c.constrained_to_value.as_array() {
                arr.iter().any(|arr_item| {
                  match ctx_val {
                    Some(ctx_val) => ctx_val == arr_item,
                    None => c.constrained_to_value.is_null(),
                  }
                })
              } else {
                false
              }
            },
          }
        })
      }).ok_or(ClientGetConfigError::NoValue)?;

      Ok(value.payload.clone())
    }
}

impl ClientConfig {
  pub fn url(&self) -> Url {
    let mut u = self.base_url.clone();
    u.path_segments_mut().unwrap().extend(&["client", "configs"]);
    u
  }
}

struct ClientState {
  _scopes: HashMap<UniqueId, ClientScope>,
  tenant_fields_by_name: HashMap<String, ClientField>,
  sg_fields_by_name: Vec<HashMap<String, ClientField>>,
  service_fields_by_name: HashMap<String, ClientField>
}

impl ClientState {
    fn new(data: ClientData) -> Self {
      let ClientData { scopes, fields } = data;
      let scopes = scopes.into_iter().map(|s| (s.id, s)).collect::<HashMap<UniqueId, ClientScope>>();
      let tenant_scope_id = *scopes.iter().find_map(|(id, s)| match &s.descriptor {
        ScopeDescriptor::Tenant{ tenant_id: _} => Some(id),
        _ => None
      }).expect("Bad data from dyna-saur api: expect one tenant");
      let service_scope_id = *scopes.iter().find_map(|(id, s)| match &s.descriptor {
        ScopeDescriptor::Service{ tenant_id: _, service_id: _} => Some(id),
        _ => None
      }).expect("Bad data from dyna-saur api: expect one service");
      let mut service_group_scopes = scopes.iter().filter_map(|(_id, scope)| match &scope.descriptor {
        ScopeDescriptor::ServiceGroup {tenant_id: _, service_group_id: _} => Some(scope.clone()),
        _ => None
      }).collect::<Vec<_>>();
      service_group_scopes.sort_unstable_by(|a, b| match (&a.descriptor, &b.descriptor) {
        (
          ScopeDescriptor::ServiceGroup { tenant_id: _, service_group_id: a_id },
          ScopeDescriptor::ServiceGroup { tenant_id: _, service_group_id: b_id }) => a_id.cmp(b_id),
        _ => unreachable!(),
      });

      Self {
        _scopes: scopes,
        tenant_fields_by_name: fields.iter().filter_map(|f| if f.scope_id == tenant_scope_id {
          Some((f.name.clone(), f.clone()))
        } else {
          None
        }).collect(),
        service_fields_by_name: fields.iter().filter_map(|f| if f.scope_id == service_scope_id {
          Some((f.name.clone(), f.clone()))
        } else {
          None
        }).collect(),
        sg_fields_by_name: service_group_scopes.into_iter().map(|sg_scope|
          fields.iter().filter_map(|f| if f.scope_id == sg_scope.id {
            Some((f.name.clone(), f.clone()))
          } else {
            None
          }).collect()).collect()
      }
    }
}

pub trait Backend {
  fn start(&mut self, client: Arc<Mutex<Client>>);
}

#[derive(Debug)]
pub struct AwcBackend {
  jh: Option<actix_rt::task::JoinHandle<()>>
}

impl AwcBackend {
    pub fn new() -> Self { Self { jh: None } }
}

impl Default for AwcBackend {
    fn default() -> Self {
        Self::new()
    }
}

impl Backend for AwcBackend {
    fn start(&mut self, client: Arc<Mutex<Client>>) {
        if self.jh.is_some() {
          warn!("AwcBackend::start has been called twice. This is unnecessary, but will be ignored");
          return
        }

        let handle = actix_rt::spawn(async move {
          let config = client.lock().unwrap().config.clone();
          let mut interval = actix_rt::time::interval(config.polling_interval);
          loop {
            interval.tick().await;
            let awc_client = awc::Client::default();
            let url = config.url().to_string();
            debug!("requesting dyna_saur field config from `{}`", url);
            let request = awc_client.get(url).bearer_auth(config.secret.clone()).send();
            match request.await {
                Ok(mut res) => {
                  if res.status().is_success() {
                    match res.json::<ClientData>().await {
                        Ok(data) => {
                          trace!("got new client data {:?}", data);
                          client.lock().unwrap().update_state(data)
                        },
                        Err(e) => warn!("unable to parse response from dyna-saur server: {}", e),
                    }
                  } else {
                    warn!("got {} response from dyna-saur server", res.status())
                  }
                },
                Err(e) => {
                  warn!("unable to connect to dyna-saur server: {}", e);
                },
            }
          }
        });

        self.jh = Some(handle);
    }
}

#[cfg(test)]
mod tests {

}
