# Dyna-saur

A dynamic configuration service

## architecture

Dyna-saur is a dynamic configurtion service -- it stores configuration data which can be consumed in other services without a redeploy or any other change in code.

### packages

* dyna-saur-core

Contains types and shared logic

* dyna-saur-server

The API backend server for the dyna-saur

* dyna-saur-client

The rust client for dyna-saur. Other clients can be either built from scratch or via binding the rust client.
