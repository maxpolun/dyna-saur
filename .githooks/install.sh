#!/usr/bin/env bash
set -euo pipefail

echo "installing git hooks"

if [[ $BASH_SOURCE = */* ]]; then
  current_dir=${BASH_SOURCE%/*}/
else
  current_dir=./
fi

function copy-hook {
  cp "${current_dir}/$1" "${current_dir}/../.git/hooks/$1"

  chmod u+x "${current_dir}/../.git/hooks/$1"
}

copy-hook "commit-msg"
