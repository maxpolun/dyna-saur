use std::{time::Duration, collections::HashMap, sync::Arc};

use dyna_saur_client::{ClientConfig, AwcBackend, Backend};
use url::Url;

#[actix_rt::main]
async fn main() {
  dotenv::dotenv().ok();
  env_logger::init();
  let base_url = std::env::var("DYNA_SAUR_URL").unwrap_or_else(|_| "http://localhost:3500".into());
  let secret = std::env::var("DYNA_SAUR_SECRET").expect("secret is required");
  let client = dyna_saur_client::Client::new(ClientConfig {
    base_url: Url::parse(&base_url).unwrap(), secret, polling_interval: Duration::from_secs(60)
  });
  let mut backend = AwcBackend::default();
  backend.start(Arc::clone(&client));

  let mut interval = actix_rt::time::interval(Duration::from_secs(30));

  let ctx = HashMap::new();

  loop {
    println!("client value is {:?}", client.lock().unwrap().get_raw("test_key", &ctx));
    interval.tick().await;
  }
}
