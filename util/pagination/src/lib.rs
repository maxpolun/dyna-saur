use std::{fmt::Display, str::FromStr};

use actix_web::http::StatusCode;
use dyna_db::{DbError, PgMappingError};
use problem_json::{ToProblemJson, ProblemJson, not_found, internal_server_error};
use serde::{Deserialize, Serialize};
use thiserror::Error;

pub const MAX_PAGE_SIZE: i64 = 1000;
#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(rename_all = "snake_case")]
pub enum PaginationParams {
  Cursor(String),
  PaginationFields(PaginationFields),
}

impl PaginationParams {
  pub fn query(self) -> Result<PaginatedQuery, GenericPaginatedApiError> {
    match self {
      PaginationParams::Cursor(c) => {
        let Cursor { fields, last_id } = Cursor::from_str(&c)?;
        Ok(PaginatedQuery {
          fields,
          last_id: Some(last_id),
        })
      }
      PaginationParams::PaginationFields(fields) => Ok(PaginatedQuery { fields, last_id: None }),
    }
  }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct PaginationFields {
  pub sort_by: String,
  pub sort_order: SortOrder,
  pub page_size: i64,
}

impl PaginationFields {
  pub fn sort_col(&self, cols: &[&'static str]) -> Result<&'static str, InvalidPaginationError> {
    let found = cols.iter().find(|&&c| self.sort_by == c);
    if let Some(c) = found {
      Ok(*c)
    } else {
      Err(InvalidPaginationError::InvalidColumn)
    }
  }

  pub fn sort_order(&self) -> (String, &'static str) {
    let order_str = self.sort_order.to_string();
    let cmp_str = match self.sort_order {
      SortOrder::Ascending => ">",
      SortOrder::Decending => "<",
    };
    (order_str, cmp_str)
  }

  pub fn page_size(&self) -> Result<i64, InvalidPaginationError> {
    if self.page_size > 1 && self.page_size <= MAX_PAGE_SIZE {
      Ok(self.page_size)
    } else {
      Err(InvalidPaginationError::PageSizeOutOfRange)
    }
  }

  pub fn into_cursor(self, new_last_id: &str) -> Cursor {
    Cursor {
      last_id: new_last_id.to_owned(),
      fields: self,
    }
  }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub enum SortOrder {
  #[serde(rename = "asc")]
  Ascending,
  #[serde(rename = "desc")]
  Decending,
}

impl Display for SortOrder {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    match self {
      SortOrder::Ascending => f.write_str("ASC"),
      SortOrder::Decending => f.write_str("DESC"),
    }
  }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Cursor {
  #[serde(rename = "l")]
  pub last_id: String,
  #[serde(flatten)]
  pub fields: PaginationFields,
}

#[derive(Debug, Error)]
pub enum CursorDecodeError {
  #[error("error parsing cursor json: {0}")]
  InvalidJson(#[from] serde_json::Error),
  #[error("error decoding base64: {0}")]
  InvalidB64(#[from] base64::DecodeError),
}

impl FromStr for Cursor {
  type Err = CursorDecodeError;

  fn from_str(s: &str) -> Result<Self, Self::Err> {
    let decoded = base64::decode(s)?;
    let s = String::from_utf8_lossy(&decoded);
    Ok(serde_json::from_str(&s)?)
  }
}

impl Display for Cursor {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    f.write_str(&base64::encode(serde_json::to_string(self).unwrap().as_str()))
  }
}

#[derive(Debug, Error, Clone)]
pub enum InvalidPaginationError {
  #[error("sort_by column not valid for this API")]
  InvalidColumn,
  #[error("Page size must be between 1 and 1000")]
  PageSizeOutOfRange,
  #[error("Invalid cursor, please pass a cursor returned from this API")]
  InvalidCursor,
  #[error("Cursor refers to data that has expired, make a new request to get a new cursor")]
  ExpiredCursor,
}

impl From<CursorDecodeError> for InvalidPaginationError {
  fn from(_e: CursorDecodeError) -> Self {
    Self::InvalidCursor
  }
}

impl ToProblemJson for InvalidPaginationError {
  fn to_problem_json(self) -> ProblemJson {
    let errpage = match self {
      InvalidPaginationError::InvalidColumn => "sortable-field",
      InvalidPaginationError::PageSizeOutOfRange => "page-size-range",
      InvalidPaginationError::InvalidCursor => "invalid-cursor",
      InvalidPaginationError::ExpiredCursor => "cursor-expired",
    };
    ProblemJson::new_custom(StatusCode::UNPROCESSABLE_ENTITY, errpage, self.to_string())
  }
}

#[derive(Debug, Error)]
pub enum GenericPaginatedApiError {
  #[error("bad pagination params {0}")]
  Pagination(#[from] InvalidPaginationError),
  #[error("missing parent resource")]
  MissingParent(String),
  #[error("database error {0}")]
  Db(#[from] DbError),
  #[error("error mapping data {0}")]
  Mapper(#[from] PgMappingError),
}

impl From<CursorDecodeError> for GenericPaginatedApiError {
  fn from(_e: CursorDecodeError) -> Self {
    Self::Pagination(InvalidPaginationError::InvalidCursor)
  }
}

impl ToProblemJson for GenericPaginatedApiError {
  fn to_problem_json(self) -> ProblemJson {
    match self {
      Self::Pagination(p) => p.to_problem_json(),
      Self::MissingParent(_parent) => not_found(),
      Self::Db(_) => internal_server_error(),
      Self::Mapper(_) => internal_server_error(),
    }
  }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Paginated<R> {
  cursor: Option<String>,
  items: R,
}

impl<R> Paginated<R> {
  pub fn new(cursor: Option<Cursor>, items: R) -> Self {
    Self {
      cursor: cursor.map(|c| c.to_string()),
      items,
    }
  }

  pub fn cursor(&self) -> Option<&str> {
    self.cursor.as_deref()
  }

  pub fn items(&self) -> &R {
    &self.items
  }
}

#[derive(Debug)]
pub struct PaginatedQuery {
  pub fields: PaginationFields,
  pub last_id: Option<String>,
}
