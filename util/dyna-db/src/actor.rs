use actix::{Actor, Context};

pub struct RealDbActor {
  pub pool: crate::Pool,
}

impl RealDbActor {
  pub fn new(pool: crate::Pool) -> Self {
    Self { pool }
  }
}

impl Actor for RealDbActor {
  type Context = Context<Self>;
}
