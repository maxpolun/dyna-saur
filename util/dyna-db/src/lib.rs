use std::{error::Error, fmt::Display, env};

use tokio_postgres::{Row, types::FromSql, Client, NoTls};

pub mod actor;

pub type DbError = tokio_postgres::error::Error;
pub type Pool = bb8::Pool<bb8_postgres::PostgresConnectionManager<NoTls>>;

#[derive(Debug, thiserror::Error)]
pub enum PgMappingError {
  #[error("pg error: {0}")]
  Pg(#[from] PgMappingErrorInner)
}

#[derive(Debug)]
pub struct PgMappingErrorInner {
  column: String,
  struct_name: String,
  pg_err: tokio_postgres::error::Error,
}

impl Display for PgMappingErrorInner {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    f.write_fmt(format_args!(
      "got error mapping column {}.{}: {}",
      self.struct_name, self.column, self.pg_err
    ))
  }
}

impl Error for PgMappingErrorInner {
  fn source(&self) -> Option<&(dyn Error + 'static)> {
    Some(&self.pg_err)
  }
}

#[derive(Debug)]
pub struct PgMapper {
  struct_name: &'static str,
  namespace: Option<&'static str>,
  row: Row,
}

impl PgMapper {
  pub fn new(struct_name: &'static str, row: Row) -> Self {
    Self { struct_name, row, namespace: None }
  }

  pub fn new_with_namespacenew(struct_name: &'static str, row: Row, namespace: &'static str) -> Self {
    Self { struct_name, row, namespace: Some(namespace) }
  }

  pub fn set_namespace(&mut self, ns: &'static str) {
    self.namespace = Some(ns);
  }

  pub fn map<T>(&self, column: &'static str) -> Result<T, PgMappingError>
  where
    for<'a> T: FromSql<'a>,
  {
     match self.namespace {
        Some(ns) => {
          let full_column = format!("{}.{}", ns, column);
          self.row.try_get(full_column.as_str())
        },
        None => self.row.try_get(column),
    }.map_err(|e| PgMappingError::Pg(PgMappingErrorInner {
      column: column.to_owned(),
      struct_name: self.struct_name.to_owned(),
      pg_err: e,
    }))
  }
}

pub trait DbMap: Sized {
  const TABLE_NAME: &'static str;
  fn try_db_mapper(mapper: PgMapper) -> Result<Self, PgMappingError>;

  fn default_db_mapper(r: Row) -> PgMapper {
    PgMapper::new(Self::TABLE_NAME, r)
  }

  fn try_db_map(row: Row) -> Result<Self, PgMappingError> {
    let mapper = Self::default_db_mapper(row);
    Self::try_db_mapper(mapper)
  }

  fn db_map(row: Row) -> Self {
    Self::try_db_map(row).expect("Unable to map database row to struct")
  }

  fn db_map_mapper(mapper: PgMapper) -> Self {
    Self::try_db_mapper(mapper).expect("Unable to map database row to struct")
  }
}

pub async fn establish_connection(constr: &str) -> Client {
  let (client, conn) = tokio_postgres::connect(constr, NoTls).await.unwrap();
  tokio::spawn(async move {
    if let Err(e) = conn.await {
      eprintln!("connection error: {}", e);
    }
  });
  client
}

pub async fn test_client() -> Client {
  dotenv::dotenv().ok();
  let template_db = env::var("TEST_DB_TEMPLATE").expect("TEST_DB_TEMPLATE must be set");
  establish_connection(&template_db).await
}

pub async fn test_transaction() -> Client {
  let c = test_client().await;
  c.execute("BEGIN", &[]).await.unwrap();
  c
}

pub fn db_error_constraint(err: &tokio_postgres::Error) -> Option<&str> {
  if let Some(db_err) = err.as_db_error() {
    return db_err.constraint();
  }
  None
}
