use std::collections::{HashMap, HashSet};

use once_cell::sync::OnceCell;
use postgres_types::ToSql;
use regex::Regex;
use tokio_postgres::{GenericClient, Row};

pub extern crate postgres_types;

#[derive(Debug)]
enum QuerySegment<'a> {
  Literal(&'a str),
  RawName(&'a str),
  StartOptional(&'a str),
  EndOptional(&'a str),
  ParameterizedName(&'a str),
}

#[derive(Debug)]
pub struct ParameterizedQueryBuilder<'a> {
  segs: Vec<QuerySegment<'a>>,
}

fn re() -> &'static Regex {
  static RE: OnceCell<Regex> = OnceCell::new();
  RE.get_or_init(|| Regex::new(r"\{\s*((:|\?|/\?)?[[:word:]]+)\s*\}").unwrap())
}

impl<'a> ParameterizedQueryBuilder<'a> {
  pub fn parse(raw_query: &'a str) -> Self {
    let mut segs = Vec::new();
    let mut last_index = 0;

    for capture in re().captures_iter(raw_query) {
      let m = capture.get(0).unwrap(); // 0th capture is the whole match
      let var_match = capture.get(1).unwrap();
      segs.push(QuerySegment::Literal(&raw_query[last_index..m.start()]));
      let var = raw_query[var_match.range()].trim();
      if let Some(stripped) = var.strip_prefix(':') {
        segs.push(QuerySegment::RawName(stripped))
      } else if let Some(stripped) = var.strip_prefix('?') {
        segs.push(QuerySegment::StartOptional(stripped))
      } else if let Some(stripped) = var.strip_prefix("/?") {
        segs.push(QuerySegment::EndOptional(stripped))
      } else {
        segs.push(QuerySegment::ParameterizedName(var))
      }
      last_index = m.end()
    }

    segs.push(QuerySegment::Literal(&raw_query[last_index..]));

    Self { segs }
  }

  pub fn build(
    self,
    raw_fields: HashMap<&str, &'a dyn ToString>,
    parameterized_fields: HashMap<&str, &'a (dyn ToSql + Sync)>,
    optional_fields: HashMap<&str, bool>
  ) -> ParameterizedQuery<'a> {
    let mut param_count = 1;
    let mut params = Vec::new();
    let mut query = String::new();
    let mut iter = self.segs.iter();
    let mut active_optionals = HashSet::new();
    while let Some(item) = iter.next() {
      match *item {
        QuerySegment::Literal(s) => query += s,
        QuerySegment::RawName(n) => {
          query += &raw_fields.get(n).unwrap_or_else(|| panic!("raw name not found: {}", n)).to_string()
        },
        QuerySegment::ParameterizedName(n) => {
          query += &format!("${}", param_count);
          param_count += 1;
          if !parameterized_fields.contains_key(n) {
            panic!("missing param {}", n)
          }
          params.push(parameterized_fields[n])
        },
        QuerySegment::EndOptional(name) => {
          if !active_optionals.remove(name) {
            // this name didn't exist panic on mismatch
            panic!("mismatched end optional query: {}", name)
          }
        },
        QuerySegment::StartOptional(name) => {
          // do nothing if the optional is true, remove until we find the matching otherwise
          if !optional_fields.get(name).unwrap_or_else(|| panic!("optional name not found: {}", name)) {
            let mut matched = false;
            'inner: for inner_item in iter.by_ref() {
              if let QuerySegment::EndOptional(end_name) = *inner_item {
                if end_name == name {
                  active_optionals.remove(name);
                  matched = true;
                  break 'inner
                }
              }
            }
            if !matched {
              panic!("unmatched optional param in query: {}", name);
            }
          } else {
            // track that we're going into the optional, so we can error on mismatches
            active_optionals.insert(name);
          }
        }
      }
    }

    if !active_optionals.is_empty() {
      panic!("unmatched optional params on query: {}", active_optionals.iter().fold("".to_string(), |mut a, b| {
        a += ", ";
        a += b;
        a
      }))
    }

    ParameterizedQuery { query, params }
  }
}

pub struct ParameterizedQuery<'a> {
  pub query: String,
  pub params: Vec<&'a (dyn ToSql + Sync)>,
}

impl<'a> ParameterizedQuery<'a> {
  pub async fn execute(&self, client: &impl GenericClient) -> Result<u64, tokio_postgres::Error> {
    client.execute(self.query.as_str(), self.params.as_slice()).await
  }

  pub async fn query_one(&self, client: &impl GenericClient) -> Result<Row, tokio_postgres::Error> {
    client.query_one(self.query.as_str(), self.params.as_slice()).await
  }

  pub async fn query_opt(&self, client: &impl GenericClient) -> Result<Option<Row>, tokio_postgres::Error> {
    client.query_opt(self.query.as_str(), self.params.as_slice()).await
  }

  pub async fn query(&self, client: &impl GenericClient) -> Result<Vec<Row>, tokio_postgres::Error> {
    client.query(self.query.as_str(), self.params.as_slice()).await
  }
}

#[derive(Debug)]
pub struct SqlCols<const N: usize>(pub [&'static str; N]);

impl<const N: usize> SqlCols<N> {
    pub fn qualified(&self, table_name: &'static str) -> QualifiedSqlCols<N> {
      QualifiedSqlCols {
        cols: self.0,
        table_name
      }
    }
}

impl<const N: usize> ToString for SqlCols<N> {
  fn to_string(&self) -> String {
    self
      .0
      .iter()
      .map(|&c| format!("\"{}\"", c.replace('\"', "\\\"")))
      .collect::<Vec<_>>()
      .join(", ")
  }
}

pub struct QualifiedSqlCols<const N: usize> {
  cols: [&'static str; N],
  table_name: &'static str
}

impl<const N: usize> ToString for QualifiedSqlCols<N> {
  fn to_string(&self) -> String {
    self
      .cols
      .iter()
      .map(|&c| format!("\"{}\".\"{}\"", self.table_name, c.replace('\"', "\\\"")))
      .collect::<Vec<_>>()
      .join(", ")
  }
}

#[macro_export]
macro_rules! sql {
  (@ [$raw:ident, $cooked:ident, $optional:ident] $(,)*) => {()};

  (@ [$raw:ident, $cooked:ident, $optional:ident] $sql_varname:ident = $sql_var:expr, $($rest:tt)*) => {{
    $cooked.insert(stringify!($sql_varname), &$sql_var);
    $crate::sql!(@ [$raw, $cooked, $optional] $($rest)*)
  }};

  (@ [$raw:ident, $cooked:ident, $optional:ident] $sql_varname:ident, $($rest:tt)*) => {{
    $cooked.insert(stringify!($sql_varname), &$sql_varname);
    $crate::sql!(@ [$raw, $cooked, $optional] $($rest)*)
  }};

  (@ [$raw:ident, $cooked:ident, $optional:ident] :$raw_varname:ident = $raw_var:expr, $($rest:tt)*) => {{
    $raw.insert(stringify!($raw_varname), &$raw_var);
    $crate::sql!(@ [$raw, $cooked, $optional] $($rest)*);
  }};

  (@ [$raw:ident, $cooked:ident, $optional:ident] :$raw_varname:ident, $($rest:tt)*) => {{
    $raw.insert(stringify!($raw_varname), &$raw_varname);
    $crate::sql!(@ [$raw, $cooked, $optional] $($rest)*);
  }};

  // optionals -- these both add the optional value, and add to the `cooked` params
  (@ [$raw:ident, $cooked:ident, $optional:ident] ?$opt_varname:ident = $opt_var:expr, $($rest:tt)*) => {{
    $optional.insert(stringify!($opt_varname), $opt_var.is_some());
    $cooked.insert(stringify!($opt_varname), &$opt_var);
    $crate::sql!(@ [$raw, $cooked, $optional] $($rest)*);
  }};

  (@ [$raw:ident, $cooked:ident, $optional:ident] ?$opt_varname:ident, $($rest:tt)*) => {{
    $optional.insert(stringify!($opt_varname), $opt_varname.is_some());
    $cooked.insert(stringify!($opt_varname), &$opt_varname);
    $crate::sql!(@ [$raw, $cooked, $optional] $($rest)*);
  }};

  // entry point -- parse string and make params hashes, then send to other matchers in order to add params to hashes
  ($query_str:expr, $($args:tt)+) => {
    {
      let parsed = $crate::ParameterizedQueryBuilder::parse($query_str);
      #[allow(unused_mut)]
      let mut raw_params = std::collections::HashMap::<&str, &dyn std::string::ToString>::new();
      #[allow(unused_mut)]
      let mut sql_params = std::collections::HashMap::<&str, &(dyn $crate::postgres_types::ToSql + Sync)>::new();
      #[allow(unused_mut)]
      let mut optional_params = std::collections::HashMap::<&str, bool>::new();

      $crate::sql!(@ [raw_params, sql_params, optional_params] $($args)+,);

      parsed.build(raw_params, sql_params, optional_params)
    }
  };
}

#[cfg(test)]
mod tests {
  use dyna_db::test_transaction;

use super::*;

  #[test]
  fn it_parses() {
    use QuerySegment::*;
    assert!(matches!(
      ParameterizedQueryBuilder::parse("SELECT * FROM { :table } WHERE col > {value }{ val2}")
        .segs
        .as_slice(),
      &[
        Literal("SELECT * FROM "),
        RawName("table"),
        Literal(" WHERE col > "),
        ParameterizedName("value"),
        Literal(""),
        ParameterizedName("val2"),
        Literal(""),
      ]
    ));
  }

  #[actix_rt::test]
  async fn can_make_a_query() {
    let tx = test_transaction().await;
    let table = "tenants";
    let id = "c";
    let cols = SqlCols(["id", "description"]);
    let q = sql!("SELECT {:cols} FROM {:table} WHERE id = {id}", :table = table, id = id, :cols = cols);

    tx.query_opt(q.query.as_str(), q.params.as_slice()).await.unwrap();
  }
}
