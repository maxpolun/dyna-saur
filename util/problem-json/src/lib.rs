use actix::MailboxError;
use actix_web::{
  http::{header, StatusCode},
  HttpResponse, ResponseError, body::MessageBody,
};
use serde::{Deserialize, Serialize};
use tracing::info;
use std::convert::TryInto;
use std::env;
use std::{error::Error, fmt::Display};
use url::Url;

#[derive(Debug, Serialize, Deserialize)]
pub struct ProblemJson {
  #[serde(skip)]
  status_code: StatusCode,
  #[serde(rename = "type")]
  type_url: Url,
  title: String,
  detail: Option<String>,
  instance: Option<String>,
}

pub fn hostname() -> String {
  env::var("HOSTNAME").unwrap_or_else(|_e| "localhost:3500".into())
}

impl ProblemJson {
  pub fn new(status_code: StatusCode, type_url: Url, title: String) -> Self {
    Self {
      status_code,
      type_url,
      title,
      detail: None,
      instance: None,
    }
  }

  pub fn new_custom(status_code: StatusCode, prob_str: &str, title: String) -> Self {
    let hostname = hostname();
    let url_str = format!("http://{}/probs/{}", hostname, prob_str);
    let url = Url::parse(&url_str).unwrap();
    Self::new(status_code, url, title)
  }

  pub fn set_detail<D: ToString>(mut self, detail: D) -> Self {
    self.detail = Some(detail.to_string());
    self
  }

  #[allow(dead_code)]
  pub fn set_instance<I: ToString>(mut self, instance: I) -> Self {
    self.instance = Some(instance.to_string());
    self
  }
}

impl Display for ProblemJson {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    f.write_str(&self.title)
  }
}

impl Error for ProblemJson {}

impl ResponseError for ProblemJson {
  fn status_code(&self) -> actix_web::http::StatusCode {
    self.status_code
  }

  fn error_response(&self) -> HttpResponse {
    let mut resp = HttpResponse::new(self.status_code());
    let serialized = serde_json::to_vec(self).unwrap();

    resp.headers_mut().insert(
      header::CONTENT_TYPE,
      header::HeaderValue::from_static("application/problem+json; charset=utf-8"),
    );
    resp.set_body(serialized.boxed())
  }
}

pub trait ToProblemJson {
  fn to_problem_json(self) -> ProblemJson;
}

impl ToProblemJson for MailboxError {
  fn to_problem_json(self) -> ProblemJson {
    internal_server_error()
  }
}

impl ToProblemJson for tokio_postgres::Error {
    fn to_problem_json(self) -> ProblemJson {
        internal_server_error()
    }
}

impl<T: ToProblemJson> From<T> for ProblemJson {
  fn from(item: T) -> Self {
    item.to_problem_json()
  }
}

pub trait ProblemJsonExt {
  type Output;
  fn err_problem_json(self) -> Self::Output;
}

impl<T, E: ToProblemJson + Display> ProblemJsonExt for Result<T, E> {
  type Output = Result<T, ProblemJson>;
  fn err_problem_json(self) -> Self::Output {
    self.map_err(|e| {
      info!("converting error {} to problem json", e);
      ToProblemJson::to_problem_json(e)
    })
  }
}

pub fn internal_server_error() -> ProblemJson {
  ProblemJson::new(
    StatusCode::INTERNAL_SERVER_ERROR,
    "about:blank".try_into().unwrap(),
    "Internal Server Error".into(),
  )
}

pub fn not_found() -> ProblemJson {
  ProblemJson::new(
    StatusCode::NOT_FOUND,
    "about:blank".try_into().unwrap(),
    "Not Found".into(),
  )
}

pub fn unprocessable_entity() -> ProblemJson {
  ProblemJson::new(
    StatusCode::UNPROCESSABLE_ENTITY,
    "about:blank".try_into().unwrap(),
    "Unprocessable Entity".into(),
  )
}

pub fn conflict() -> ProblemJson {
  ProblemJson::new(
    StatusCode::CONFLICT,
    "about:blank".try_into().unwrap(),
    "Conflict".into(),
  )
}

pub fn unauthorized() -> ProblemJson {
  ProblemJson::new(StatusCode::UNAUTHORIZED,"about:blank".try_into().unwrap(), "Unauthorized".into())
}

pub fn forbidden() -> ProblemJson {
  ProblemJson::new(StatusCode::FORBIDDEN,"about:blank".try_into().unwrap(), "Forbidden".into())
}
