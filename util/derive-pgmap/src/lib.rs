use proc_macro::{self, TokenStream};
use quote::quote;
use syn::{parse_macro_input, DeriveInput, Field};

#[proc_macro_derive(DbMap)]
pub fn derive(input: TokenStream) -> TokenStream {
    let DeriveInput { ident, data, .. } = parse_macro_input!(input);
    let fields = match data {
        syn::Data::Struct(ds) => match ds.fields {
            syn::Fields::Named(nf) => nf,
            _ => panic!("derive DBMap only supports named fields"),
        },
        _ => panic!("derive DbMap only supports structs")
    };
    let field_mappings = fields.named.iter().map(|Field{ident, ..}|{
      let id = ident.clone().unwrap();
      let id_str = syn::LitStr::new(&id.to_string(), id.span());
      quote! {
      #id: mapper.map(#id_str)?,
    }}).reduce(|mut a, b|{ a.extend(b); a});
    let table_name = syn::LitStr::new(&ident.to_string(), ident.span());
    let output = quote! {
        impl dyna_db::DbMap for #ident {
          const TABLE_NAME: &'static str = #table_name;
          fn try_db_mapper(mapper: dyna_db::PgMapper) -> Result<Self, dyna_db::PgMappingError> {
            Ok(Self {
              #field_mappings
            })
          }
        }
    };
    output.into()
}
